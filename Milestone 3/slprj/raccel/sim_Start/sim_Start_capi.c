#include "rtw_capi.h"
#ifdef HOST_CAPI_BUILD
#include "sim_Start_capi_host.h"
#define sizeof(s) ((size_t)(0xFFFF))
#undef rt_offsetof
#define rt_offsetof(s,el) ((uint16_T)(0xFFFF))
#define TARGET_CONST
#define TARGET_STRING(s) (s)    
#else
#include "builtin_typeid_types.h"
#include "sim_Start.h"
#include "sim_Start_capi.h"
#include "sim_Start_private.h"
#ifdef LIGHT_WEIGHT_CAPI
#define TARGET_CONST                  
#define TARGET_STRING(s)               (NULL)                    
#else
#define TARGET_CONST                   const
#define TARGET_STRING(s)               (s)
#endif
#endif
static const rtwCAPI_Signals rtBlockSignals [ ] = { { 0 , 1 , TARGET_STRING (
"sim_Start/Chart" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 1 , 1 ,
TARGET_STRING ( "sim_Start/Chart" ) , TARGET_STRING ( "" ) , 1 , 0 , 0 , 0 ,
0 } , { 2 , 1 , TARGET_STRING ( "sim_Start/Chart" ) , TARGET_STRING ( "" ) ,
3 , 0 , 0 , 0 , 0 } , { 3 , 0 , TARGET_STRING (
"sim_Start/Chart/is_active_c3_sim_Start" ) , TARGET_STRING (
"is_active_c3_sim_Start" ) , 0 , 1 , 0 , 0 , 0 } , { 4 , 0 , TARGET_STRING (
"sim_Start/Chart/is_c3_sim_Start" ) , TARGET_STRING ( "is_c3_sim_Start" ) , 0
, 2 , 0 , 0 , 0 } , { 5 , 2 , TARGET_STRING ( "sim_Start/Chart1" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 6 , 2 , TARGET_STRING (
"sim_Start/Chart1" ) , TARGET_STRING ( "" ) , 1 , 0 , 0 , 0 , 0 } , { 7 , 2 ,
TARGET_STRING ( "sim_Start/Chart1" ) , TARGET_STRING ( "" ) , 2 , 0 , 0 , 0 ,
0 } , { 8 , 2 , TARGET_STRING ( "sim_Start/Chart1" ) , TARGET_STRING ( "" ) ,
3 , 0 , 0 , 0 , 0 } , { 9 , 2 , TARGET_STRING ( "sim_Start/Chart1" ) ,
TARGET_STRING ( "" ) , 4 , 0 , 0 , 0 , 0 } , { 10 , 2 , TARGET_STRING (
"sim_Start/Chart1" ) , TARGET_STRING ( "" ) , 5 , 0 , 0 , 0 , 0 } , { 11 , 0
, TARGET_STRING ( "sim_Start/Chart1/is_active_c1_sim_Start" ) , TARGET_STRING
( "is_active_c1_sim_Start" ) , 0 , 1 , 0 , 0 , 0 } , { 12 , 0 , TARGET_STRING
( "sim_Start/Chart1/is_c1_sim_Start" ) , TARGET_STRING ( "is_c1_sim_Start" )
, 0 , 2 , 0 , 0 , 0 } , { 13 , 3 , TARGET_STRING ( "sim_Start/Chart2" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 14 , 3 , TARGET_STRING (
"sim_Start/Chart2" ) , TARGET_STRING ( "" ) , 1 , 0 , 0 , 0 , 0 } , { 15 , 3
, TARGET_STRING ( "sim_Start/Chart2" ) , TARGET_STRING ( "" ) , 2 , 0 , 0 , 0
, 0 } , { 16 , 3 , TARGET_STRING ( "sim_Start/Chart2" ) , TARGET_STRING ( ""
) , 3 , 0 , 0 , 0 , 0 } , { 17 , 0 , TARGET_STRING (
"sim_Start/Chart2/is_active_c6_sim_Start" ) , TARGET_STRING (
"is_active_c6_sim_Start" ) , 0 , 1 , 0 , 0 , 0 } , { 18 , 0 , TARGET_STRING (
"sim_Start/Chart2/is_c6_sim_Start" ) , TARGET_STRING ( "is_c6_sim_Start" ) ,
0 , 2 , 0 , 0 , 0 } , { 19 , 0 , TARGET_STRING ( "sim_Start/From Workspace3"
) , TARGET_STRING ( "" ) , 0 , 1 , 1 , 0 , 1 } , { 20 , 0 , TARGET_STRING (
"sim_Start/AND" ) , TARGET_STRING ( "" ) , 0 , 3 , 0 , 0 , 1 } , { 21 , 0 ,
TARGET_STRING ( "sim_Start/NOT" ) , TARGET_STRING ( "" ) , 0 , 3 , 0 , 0 , 0
} , { 22 , 0 , TARGET_STRING ( "sim_Start/NOT1" ) , TARGET_STRING ( "" ) , 0
, 3 , 0 , 0 , 0 } , { 23 , 0 , TARGET_STRING ( "sim_Start/NOT2" ) ,
TARGET_STRING ( "" ) , 0 , 3 , 0 , 0 , 0 } , { 24 , 0 , TARGET_STRING (
"sim_Start/NOT3" ) , TARGET_STRING ( "" ) , 0 , 3 , 0 , 0 , 0 } , { 25 , 0 ,
TARGET_STRING ( "sim_Start/Add" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0
} , { 26 , 0 , TARGET_STRING ( "sim_Start/Add1" ) , TARGET_STRING ( "" ) , 0
, 0 , 0 , 0 , 0 } , { 27 , 0 , TARGET_STRING ( "sim_Start/Add2" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 28 , 0 , TARGET_STRING (
"sim_Start/Add3" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 29 , 0 ,
TARGET_STRING ( "sim_Start/Add4" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0
} , { 30 , 0 , TARGET_STRING ( "sim_Start/Subtract" ) , TARGET_STRING ( "" )
, 0 , 0 , 0 , 0 , 0 } , { 31 , 0 , TARGET_STRING ( "sim_Start/Subtract1" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 32 , 0 , TARGET_STRING (
"sim_Start/Unit Delay1" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 33
, 0 , TARGET_STRING ( "sim_Start/Unit Delay2" ) , TARGET_STRING ( "" ) , 0 ,
0 , 0 , 0 , 0 } , { 34 , 13 , TARGET_STRING (
"sim_Start/Optimization Subsystem/MATLAB Function1" ) , TARGET_STRING ( "" )
, 0 , 0 , 0 , 0 , 0 } , { 35 , 0 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Add5" ) , TARGET_STRING ( "" ) , 0 , 0 , 0
, 0 , 0 } , { 36 , 0 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Switch2" ) , TARGET_STRING ( "" ) , 0 , 0 ,
0 , 0 , 0 } , { 37 , 0 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Switch3" ) , TARGET_STRING ( "" ) , 0 , 4 ,
0 , 0 , 0 } , { 38 , 0 , TARGET_STRING (
"sim_Start/Optimization Subsystem/String Constant" ) , TARGET_STRING ( "" ) ,
0 , 4 , 0 , 0 , 2 } , { 39 , 0 , TARGET_STRING (
"sim_Start/Optimization Subsystem/String to ASCII" ) , TARGET_STRING ( "" ) ,
0 , 1 , 1 , 0 , 0 } , { 40 , 0 , TARGET_STRING (
"sim_Start/Robot Simulator/Discrete-Time Integrator" ) , TARGET_STRING ( "" )
, 0 , 0 , 0 , 0 , 0 } , { 41 , 0 , TARGET_STRING (
"sim_Start/Robot Simulator/Discrete-Time Integrator1" ) , TARGET_STRING ( ""
) , 0 , 0 , 0 , 0 , 0 } , { 42 , 0 , TARGET_STRING (
"sim_Start/Robot Simulator/Gain1" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 ,
2 } , { 43 , 0 , TARGET_STRING ( "sim_Start/Robot Simulator/Math Function" )
, TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 44 , 0 , TARGET_STRING (
"sim_Start/Robot Simulator/Divide" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 ,
2 } , { 45 , 0 , TARGET_STRING ( "sim_Start/Robot Simulator/Product1" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 46 , 0 , TARGET_STRING (
"sim_Start/Robot Simulator/Product2" ) , TARGET_STRING ( "" ) , 0 , 0 , 0 , 0
, 0 } , { 47 , 0 , TARGET_STRING ( "sim_Start/Robot Simulator/Product3" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 48 , 0 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/Left Motor LUT" ) , TARGET_STRING (
"Left PWM" ) , 0 , 0 , 0 , 0 , 1 } , { 49 , 0 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/Right Motor LUT" ) , TARGET_STRING (
"Right PWM" ) , 0 , 0 , 0 , 0 , 1 } , { 50 , 25 , TARGET_STRING (
"sim_Start/Write to memory/MATLAB Function" ) , TARGET_STRING ( "" ) , 0 , 0
, 0 , 0 , 0 } , { 51 , 25 , TARGET_STRING (
"sim_Start/Write to memory/MATLAB Function" ) , TARGET_STRING ( "" ) , 1 , 4
, 0 , 0 , 0 } , { 52 , 0 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Motor/Internal" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 53 , 0 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Motor/Internal" ) ,
TARGET_STRING ( "" ) , 0 , 0 , 0 , 0 , 0 } , { 0 , 0 , ( NULL ) , ( NULL ) ,
0 , 0 , 0 , 0 , 0 } } ; static const rtwCAPI_BlockParameters
rtBlockParameters [ ] = { { 54 , TARGET_STRING (
"sim_Start/Data Store Memory" ) , TARGET_STRING ( "InitialValue" ) , 5 , 0 ,
0 } , { 55 , TARGET_STRING ( "sim_Start/Left Line Sensor" ) , TARGET_STRING (
"envVal" ) , 0 , 0 , 0 } , { 56 , TARGET_STRING (
"sim_Start/Left Line Sensor" ) , TARGET_STRING ( "lineVal" ) , 0 , 0 , 0 } ,
{ 57 , TARGET_STRING ( "sim_Start/Left Middle Line Sensor" ) , TARGET_STRING
( "envVal" ) , 0 , 0 , 0 } , { 58 , TARGET_STRING (
"sim_Start/Left Middle Line Sensor" ) , TARGET_STRING ( "lineVal" ) , 0 , 0 ,
0 } , { 59 , TARGET_STRING ( "sim_Start/Right Line Sensor" ) , TARGET_STRING
( "envVal" ) , 0 , 0 , 0 } , { 60 , TARGET_STRING (
"sim_Start/Right Line Sensor" ) , TARGET_STRING ( "lineVal" ) , 0 , 0 , 0 } ,
{ 61 , TARGET_STRING ( "sim_Start/Right Middle Line Sensor" ) , TARGET_STRING
( "envVal" ) , 0 , 0 , 0 } , { 62 , TARGET_STRING (
"sim_Start/Right Middle Line Sensor" ) , TARGET_STRING ( "lineVal" ) , 0 , 0
, 0 } , { 63 , TARGET_STRING ( "sim_Start/Robot Simulator" ) , TARGET_STRING
( "startX" ) , 0 , 0 , 0 } , { 64 , TARGET_STRING (
"sim_Start/Robot Simulator" ) , TARGET_STRING ( "startY" ) , 0 , 0 , 0 } , {
65 , TARGET_STRING ( "sim_Start/Robot Simulator" ) , TARGET_STRING (
"startTheta" ) , 0 , 0 , 0 } , { 66 , TARGET_STRING (
"sim_Start/From Workspace1" ) , TARGET_STRING ( "Time0" ) , 0 , 0 , 0 } , {
67 , TARGET_STRING ( "sim_Start/From Workspace1" ) , TARGET_STRING ( "Data0"
) , 0 , 0 , 0 } , { 68 , TARGET_STRING ( "sim_Start/From Workspace2" ) ,
TARGET_STRING ( "Time0" ) , 0 , 0 , 0 } , { 69 , TARGET_STRING (
"sim_Start/From Workspace2" ) , TARGET_STRING ( "Data0" ) , 0 , 0 , 0 } , {
70 , TARGET_STRING ( "sim_Start/From Workspace3" ) , TARGET_STRING ( "Time0"
) , 0 , 2 , 0 } , { 71 , TARGET_STRING ( "sim_Start/From Workspace3" ) ,
TARGET_STRING ( "Data0" ) , 1 , 3 , 0 } , { 72 , TARGET_STRING (
"sim_Start/Unit Delay1" ) , TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0
} , { 73 , TARGET_STRING ( "sim_Start/Unit Delay2" ) , TARGET_STRING (
"InitialCondition" ) , 0 , 0 , 0 } , { 74 , TARGET_STRING (
"sim_Start/Left Line Sensor/Constant" ) , TARGET_STRING ( "Value" ) , 0 , 0 ,
0 } , { 75 , TARGET_STRING ( "sim_Start/Left Line Sensor/Constant1" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 76 , TARGET_STRING (
"sim_Start/Left Line Sensor/Switch" ) , TARGET_STRING ( "Threshold" ) , 6 , 0
, 0 } , { 77 , TARGET_STRING ( "sim_Start/Left Middle Line Sensor/Constant" )
, TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 78 , TARGET_STRING (
"sim_Start/Left Middle Line Sensor/Constant1" ) , TARGET_STRING ( "Value" ) ,
0 , 0 , 0 } , { 79 , TARGET_STRING (
"sim_Start/Left Middle Line Sensor/Switch" ) , TARGET_STRING ( "Threshold" )
, 6 , 0 , 0 } , { 80 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Constant" ) , TARGET_STRING ( "Value" ) , 0
, 0 , 0 } , { 81 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Constant1" ) , TARGET_STRING ( "Value" ) ,
0 , 0 , 0 } , { 82 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Constant2" ) , TARGET_STRING ( "Value" ) ,
0 , 0 , 0 } , { 83 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Switch2" ) , TARGET_STRING ( "Threshold" )
, 0 , 0 , 0 } , { 84 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Switch3" ) , TARGET_STRING ( "Threshold" )
, 0 , 0 , 0 } , { 85 , TARGET_STRING (
"sim_Start/Optimization Subsystem/Delay" ) , TARGET_STRING (
"InitialCondition" ) , 0 , 0 , 0 } , { 86 , TARGET_STRING (
"sim_Start/Optimization Subsystem/String Constant" ) , TARGET_STRING (
"String" ) , 5 , 0 , 0 } , { 87 , TARGET_STRING (
"sim_Start/Right Line Sensor/Constant" ) , TARGET_STRING ( "Value" ) , 0 , 0
, 0 } , { 88 , TARGET_STRING ( "sim_Start/Right Line Sensor/Constant1" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 89 , TARGET_STRING (
"sim_Start/Right Line Sensor/Switch" ) , TARGET_STRING ( "Threshold" ) , 6 ,
0 , 0 } , { 90 , TARGET_STRING (
"sim_Start/Right Middle Line Sensor/Constant" ) , TARGET_STRING ( "Value" ) ,
0 , 0 , 0 } , { 91 , TARGET_STRING (
"sim_Start/Right Middle Line Sensor/Constant1" ) , TARGET_STRING ( "Value" )
, 0 , 0 , 0 } , { 92 , TARGET_STRING (
"sim_Start/Right Middle Line Sensor/Switch" ) , TARGET_STRING ( "Threshold" )
, 6 , 0 , 0 } , { 93 , TARGET_STRING ( "sim_Start/Robot Simulator/Circle" ) ,
TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 94 , TARGET_STRING (
"sim_Start/Robot Simulator/Discrete-Time Integrator" ) , TARGET_STRING (
"gainval" ) , 0 , 0 , 0 } , { 95 , TARGET_STRING (
"sim_Start/Robot Simulator/Discrete-Time Integrator1" ) , TARGET_STRING (
"gainval" ) , 0 , 0 , 0 } , { 96 , TARGET_STRING (
"sim_Start/Robot Simulator/Discrete-Time Integrator2" ) , TARGET_STRING (
"gainval" ) , 0 , 0 , 0 } , { 97 , TARGET_STRING (
"sim_Start/Robot Simulator/Gain1" ) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 }
, { 98 , TARGET_STRING ( "sim_Start/Robot Simulator/Soft Real Time" ) ,
TARGET_STRING ( "P1" ) , 0 , 0 , 0 } , { 99 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor" ) , TARGET_STRING (
"sensorType" ) , 0 , 0 , 0 } , { 100 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor" ) , TARGET_STRING (
"sensorType" ) , 0 , 0 , 0 } , { 101 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/To wlwr" ) , TARGET_STRING ( "wheelR" ) ,
0 , 0 , 0 } , { 102 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/From Workspace" ) , TARGET_STRING (
"Time0" ) , 0 , 0 , 0 } , { 103 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/From Workspace" ) , TARGET_STRING (
"Data0" ) , 0 , 0 , 0 } , { 104 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/Switch" ) , TARGET_STRING ( "Threshold" )
, 0 , 0 , 0 } , { 105 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/Switch1" ) , TARGET_STRING ( "Threshold" )
, 0 , 0 , 0 } , { 106 , TARGET_STRING (
"sim_Start/Write to memory/Unit Delay" ) , TARGET_STRING ( "InitialCondition"
) , 5 , 0 , 0 } , { 107 , TARGET_STRING (
"sim_Start/Write to memory/Unit Delay1" ) , TARGET_STRING (
"InitialCondition" ) , 0 , 0 , 0 } , { 108 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Discrete-Time Integrator" ) ,
TARGET_STRING ( "gainval" ) , 0 , 0 , 0 } , { 109 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Discrete-Time Integrator" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 110 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Switch" ) , TARGET_STRING (
"Threshold" ) , 0 , 0 , 0 } , { 111 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Discrete-Time Integrator" ) ,
TARGET_STRING ( "gainval" ) , 0 , 0 , 0 } , { 112 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Discrete-Time Integrator" ) ,
TARGET_STRING ( "InitialCondition" ) , 0 , 0 , 0 } , { 113 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Switch" ) , TARGET_STRING (
"Threshold" ) , 0 , 0 , 0 } , { 114 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/To wlwr/change parameters" ) ,
TARGET_STRING ( "Gain" ) , 0 , 4 , 0 } , { 115 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Motor/Internal" ) ,
TARGET_STRING ( "A" ) , 0 , 5 , 0 } , { 116 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Motor/Internal" ) ,
TARGET_STRING ( "B" ) , 0 , 6 , 0 } , { 117 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Motor/Internal" ) ,
TARGET_STRING ( "C" ) , 0 , 6 , 0 } , { 118 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/LeftMotor/Radians to Degrees/Gain" ) ,
TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 119 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Motor/Internal" ) ,
TARGET_STRING ( "A" ) , 0 , 5 , 0 } , { 120 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Motor/Internal" ) ,
TARGET_STRING ( "B" ) , 0 , 6 , 0 } , { 121 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Motor/Internal" ) ,
TARGET_STRING ( "C" ) , 0 , 6 , 0 } , { 122 , TARGET_STRING (
"sim_Start/Wheel Control subsystem/RightMotor/Radians to Degrees/Gain" ) ,
TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 123 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 124 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias1"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 125 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Constant"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 126 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 127 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain1"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 128 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 129 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 130 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 131 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 132 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "Table" ) , 6 , 7 , 0 } , { 133 , TARGET_STRING (
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "DiagnosticForOutOfRangeInput" ) , 1 , 0 , 0 } , { 134 ,
TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 135 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias1"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 136 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Constant"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 137 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 138 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain1"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 139 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 140 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 141 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 142 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 143 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "Table" ) , 6 , 7 , 0 } , { 144 , TARGET_STRING (
 "sim_Start/Left Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "DiagnosticForOutOfRangeInput" ) , 1 , 0 , 0 } , { 145 ,
TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 146 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias1"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 147 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Constant"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 148 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 149 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain1"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 150 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 151 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 152 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 153 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 154 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "Table" ) , 6 , 7 , 0 } , { 155 , TARGET_STRING (
 "sim_Start/Right Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "DiagnosticForOutOfRangeInput" ) , 1 , 0 , 0 } , { 156 ,
TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 157 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Bias1"
) , TARGET_STRING ( "Bias" ) , 0 , 0 , 0 } , { 158 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Constant"
) , TARGET_STRING ( "Value" ) , 0 , 0 , 0 } , { 159 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 160 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Gain1"
) , TARGET_STRING ( "Gain" ) , 0 , 0 , 0 } , { 161 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 162 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation2"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 163 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "UpperLimit" ) , 0 , 0 , 0 } , { 164 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Saturation3"
) , TARGET_STRING ( "LowerLimit" ) , 0 , 0 , 0 } , { 165 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "Table" ) , 6 , 7 , 0 } , { 166 , TARGET_STRING (
 "sim_Start/Right Middle Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) , TARGET_STRING ( "DiagnosticForOutOfRangeInput" ) , 1 , 0 , 0 } , { 0 , (
NULL ) , ( NULL ) , 0 , 0 , 0 } } ; static int_T rt_LoggedStateIdxList [ ] =
{ - 1 } ; static const rtwCAPI_Signals rtRootInputs [ ] = { { 0 , 0 , ( NULL
) , ( NULL ) , 0 , 0 , 0 , 0 , 0 } } ; static const rtwCAPI_Signals
rtRootOutputs [ ] = { { 0 , 0 , ( NULL ) , ( NULL ) , 0 , 0 , 0 , 0 , 0 } } ;
static const rtwCAPI_ModelParameters rtModelParameters [ ] = { { 167 ,
TARGET_STRING ( "InputPWM" ) , 0 , 8 , 0 } , { 168 , TARGET_STRING (
"WheelSpeed" ) , 0 , 8 , 0 } , { 169 , TARGET_STRING ( "axleLength" ) , 0 , 0
, 0 } , { 170 , TARGET_STRING ( "lineLength" ) , 0 , 0 , 0 } , { 171 ,
TARGET_STRING ( "wheelR" ) , 0 , 0 , 0 } , { 0 , ( NULL ) , 0 , 0 , 0 } } ;
#ifndef HOST_CAPI_BUILD
static void * rtDataAddrMap [ ] = { & rtB . hldfmfuah3 , & rtB . ii32dgziov ,
& rtB . la1brdabwh , & rtDW . nyhsr4t3fp , & rtDW . mqobma15yv , & rtB .
jjkwpfenp4 , & rtB . mo2yss1m1k , & rtB . i5w0dsemue , & rtB . mtdmnunuoi , &
rtB . mjogabnmjg , & rtB . ls0wcsfeyn , & rtDW . c1g1b2pcb5 , & rtDW .
co2n0fejda , & rtB . dxmhlj5cu2 , & rtB . jl4a230o5q , & rtB . pnwjxb31po , &
rtB . oqf5mpl1fz , & rtDW . kfkq4jq4py , & rtDW . m1jml1zdzn , & rtB .
jialwgyso4 [ 0 ] , & rtB . mql3fd1ucd , & rtB . krhfkykqos , & rtB .
amaivwoi3x , & rtB . faxpoeey0v , & rtB . br5qezvsfu , & rtB . ezqnv40hor , &
rtB . dvpimpdrs3 , & rtB . dkze5lu4ma , & rtB . n4zowml5qq , & rtB .
ltxnmikrnx , & rtB . nm1wroc1sb , & rtB . ktb4r2yzkb , & rtB . dzwy32uw0n , &
rtB . o1ijyw24e0 , & rtB . chxyftuvao , & rtB . gnp2aeui1f , & rtB .
p01c3bphbw , & rtB . izgo3pnyfb , & rtB . o0ftqgjmrw , & rtB . g2v4ytatik [ 0
] , & rtB . h012lh2sdm , & rtB . kawa0ssxug , & rtB . orypiaqyjn , & rtB .
hng4zofqfq , & rtB . ppg1emiqtt , & rtB . e41kschlmd , & rtB . jnbrvrbl4c , &
rtB . j4n5p1agp1 , & rtB . n3rebawkox , & rtB . eryzgsl04j , & rtB .
hskzxi0fod , & rtB . bkd0pdtznt , & rtB . cf5djepmnt , & rtB . hqdziciins , &
rtP . DataStoreMemory_InitialValue , & rtP . LeftLineSensor_envVal , & rtP .
LeftLineSensor_lineVal , & rtP . LeftMiddleLineSensor_envVal , & rtP .
LeftMiddleLineSensor_lineVal , & rtP . RightLineSensor_envVal , & rtP .
RightLineSensor_lineVal , & rtP . RightMiddleLineSensor_envVal , & rtP .
RightMiddleLineSensor_lineVal , & rtP . RobotSimulator_startX , & rtP .
RobotSimulator_startY , & rtP . RobotSimulator_startTheta , & rtP .
FromWorkspace1_Time0 , & rtP . FromWorkspace1_Data0 , & rtP .
FromWorkspace2_Time0 , & rtP . FromWorkspace2_Data0 , & rtP .
FromWorkspace3_Time0 [ 0 ] , & rtP . FromWorkspace3_Data0 [ 0 ] , & rtP .
UnitDelay1_InitialCondition_jmsdimwcgj , & rtP . UnitDelay2_InitialCondition
, & rtP . Constant_Value_hlhm5ajdso , & rtP . Constant1_Value_plr2cy0rrk , &
rtP . Switch_Threshold_ijwjdx5hx3 , & rtP . Constant_Value_olk5gkppvb , & rtP
. Constant1_Value_oyulwwquga , & rtP . Switch_Threshold_a2s0c4f3g1 , & rtP .
Constant_Value , & rtP . Constant1_Value , & rtP . Constant2_Value , & rtP .
Switch2_Threshold , & rtP . Switch3_Threshold , & rtP .
Delay_InitialCondition , & rtP . StringConstant_String , & rtP .
Constant_Value_nizol5xyrr , & rtP . Constant1_Value_larkvagb33 , & rtP .
Switch_Threshold_owqwz4ijh5 , & rtP . Constant_Value_gf0zpo0joi , & rtP .
Constant1_Value_ih0jkfo3kb , & rtP . Switch_Threshold_aplx2fzo4d , & rtP .
Circle_Value , & rtP . DiscreteTimeIntegrator_gainval , & rtP .
DiscreteTimeIntegrator1_gainval , & rtP . DiscreteTimeIntegrator2_gainval , &
rtP . Gain1_Gain , & rtP . SoftRealTime_P1 , & rtP . LeftMotor_sensorType , &
rtP . RightMotor_sensorType , & rtP . Towlwr_wheelR , & rtP .
FromWorkspace_Time0 , & rtP . FromWorkspace_Data0 , & rtP .
Switch_Threshold_iry4v5n5zm , & rtP . Switch1_Threshold , & rtP .
UnitDelay_InitialCondition , & rtP . UnitDelay1_InitialCondition , & rtP .
DiscreteTimeIntegrator_gainval_afikqgzpe0 , & rtP . DiscreteTimeIntegrator_IC
, & rtP . Switch_Threshold , & rtP .
DiscreteTimeIntegrator_gainval_ptlrlhszmg , & rtP .
DiscreteTimeIntegrator_IC_e3x2ks5ohu , & rtP . Switch_Threshold_bmwdvzwwf2 ,
& rtP . changeparameters_Gain [ 0 ] , & rtP . Internal_A [ 0 ] , & rtP .
Internal_B [ 0 ] , & rtP . Internal_C [ 0 ] , & rtP . Gain_Gain , & rtP .
Internal_A_avp5ztxxzl [ 0 ] , & rtP . Internal_B_ebkdikdi50 [ 0 ] , & rtP .
Internal_C_hiyczu5j42 [ 0 ] , & rtP . Gain_Gain_o4hmcpr5ca , & rtP .
eo3swzvasqb . dd10qqbw3x . Bias_Bias , & rtP . eo3swzvasqb . dd10qqbw3x .
Bias1_Bias , & rtP . eo3swzvasqb . dd10qqbw3x . Constant_Value , & rtP .
eo3swzvasqb . dd10qqbw3x . Gain_Gain , & rtP . eo3swzvasqb . dd10qqbw3x .
Gain1_Gain , & rtP . eo3swzvasqb . dd10qqbw3x . Saturation2_UpperSat , & rtP
. eo3swzvasqb . dd10qqbw3x . Saturation2_LowerSat , & rtP . eo3swzvasqb .
dd10qqbw3x . Saturation3_UpperSat , & rtP . eo3swzvasqb . dd10qqbw3x .
Saturation3_LowerSat , & rtP . eo3swzvasqb . dd10qqbw3x .
DirectLookupTablenD_table [ 0 ] , & rtP . eo3swzvasqb . dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput , & rtP . cor5gsbd40 .
dd10qqbw3x . Bias_Bias , & rtP . cor5gsbd40 . dd10qqbw3x . Bias1_Bias , & rtP
. cor5gsbd40 . dd10qqbw3x . Constant_Value , & rtP . cor5gsbd40 . dd10qqbw3x
. Gain_Gain , & rtP . cor5gsbd40 . dd10qqbw3x . Gain1_Gain , & rtP .
cor5gsbd40 . dd10qqbw3x . Saturation2_UpperSat , & rtP . cor5gsbd40 .
dd10qqbw3x . Saturation2_LowerSat , & rtP . cor5gsbd40 . dd10qqbw3x .
Saturation3_UpperSat , & rtP . cor5gsbd40 . dd10qqbw3x . Saturation3_LowerSat
, & rtP . cor5gsbd40 . dd10qqbw3x . DirectLookupTablenD_table [ 0 ] , & rtP .
cor5gsbd40 . dd10qqbw3x . DirectLookupTablenD_DiagnosticForOutOfRangeInput ,
& rtP . btjpsv3ywi . dd10qqbw3x . Bias_Bias , & rtP . btjpsv3ywi . dd10qqbw3x
. Bias1_Bias , & rtP . btjpsv3ywi . dd10qqbw3x . Constant_Value , & rtP .
btjpsv3ywi . dd10qqbw3x . Gain_Gain , & rtP . btjpsv3ywi . dd10qqbw3x .
Gain1_Gain , & rtP . btjpsv3ywi . dd10qqbw3x . Saturation2_UpperSat , & rtP .
btjpsv3ywi . dd10qqbw3x . Saturation2_LowerSat , & rtP . btjpsv3ywi .
dd10qqbw3x . Saturation3_UpperSat , & rtP . btjpsv3ywi . dd10qqbw3x .
Saturation3_LowerSat , & rtP . btjpsv3ywi . dd10qqbw3x .
DirectLookupTablenD_table [ 0 ] , & rtP . btjpsv3ywi . dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput , & rtP . jyzsgxunlp .
dd10qqbw3x . Bias_Bias , & rtP . jyzsgxunlp . dd10qqbw3x . Bias1_Bias , & rtP
. jyzsgxunlp . dd10qqbw3x . Constant_Value , & rtP . jyzsgxunlp . dd10qqbw3x
. Gain_Gain , & rtP . jyzsgxunlp . dd10qqbw3x . Gain1_Gain , & rtP .
jyzsgxunlp . dd10qqbw3x . Saturation2_UpperSat , & rtP . jyzsgxunlp .
dd10qqbw3x . Saturation2_LowerSat , & rtP . jyzsgxunlp . dd10qqbw3x .
Saturation3_UpperSat , & rtP . jyzsgxunlp . dd10qqbw3x . Saturation3_LowerSat
, & rtP . jyzsgxunlp . dd10qqbw3x . DirectLookupTablenD_table [ 0 ] , & rtP .
jyzsgxunlp . dd10qqbw3x . DirectLookupTablenD_DiagnosticForOutOfRangeInput ,
& rtP . InputPWM [ 0 ] , & rtP . WheelSpeed [ 0 ] , & rtP . axleLength , &
rtP . lineLength , & rtP . wheelR , } ; static int32_T * rtVarDimsAddrMap [ ]
= { ( NULL ) } ;
#endif
static TARGET_CONST rtwCAPI_DataTypeMap rtDataTypeMap [ ] = { { "double" ,
"real_T" , 0 , 0 , sizeof ( real_T ) , SS_DOUBLE , 0 , 0 , 0 } , {
"unsigned char" , "uint8_T" , 0 , 0 , sizeof ( uint8_T ) , SS_UINT8 , 0 , 0 ,
0 } , { "unsigned int" , "uint32_T" , 0 , 0 , sizeof ( uint32_T ) , SS_UINT32
, 0 , 0 , 0 } , { "unsigned char" , "boolean_T" , 0 , 0 , sizeof ( boolean_T
) , SS_BOOLEAN , 0 , 0 , 0 } , { "char" , "char_T" , 0 , 0 , sizeof ( char_T
) , SS_UINT8 , 0 , 0 , 0 } , { "numeric" , "pointer_T" , 0 , 0 , sizeof (
pointer_T ) , SS_POINTER , 0 , 0 , 0 } , { "float" , "real32_T" , 0 , 0 ,
sizeof ( real32_T ) , SS_SINGLE , 0 , 0 , 0 } } ;
#ifdef HOST_CAPI_BUILD
#undef sizeof
#endif
static TARGET_CONST rtwCAPI_ElementMap rtElementMap [ ] = { { ( NULL ) , 0 ,
0 , 0 , 0 } , } ; static const rtwCAPI_DimensionMap rtDimensionMap [ ] = { {
rtwCAPI_SCALAR , 0 , 2 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 2 , 2 , 0 } , {
rtwCAPI_VECTOR , 4 , 2 , 0 } , { rtwCAPI_VECTOR , 6 , 2 , 0 } , {
rtwCAPI_MATRIX_COL_MAJOR , 8 , 2 , 0 } , { rtwCAPI_VECTOR , 10 , 2 , 0 } , {
rtwCAPI_VECTOR , 12 , 2 , 0 } , { rtwCAPI_MATRIX_COL_MAJOR , 14 , 2 , 0 } , {
rtwCAPI_VECTOR , 16 , 2 , 0 } } ; static const uint_T rtDimensionArray [ ] =
{ 1 , 1 , 1 , 31 , 81 , 1 , 2511 , 1 , 2 , 2 , 9 , 1 , 3 , 1 , 7838 , 8165 ,
1 , 173 } ; static const real_T rtcapiStoredFloats [ ] = { 0.01 , 0.0 } ;
static const rtwCAPI_FixPtMap rtFixPtMap [ ] = { { ( NULL ) , ( NULL ) ,
rtwCAPI_FIX_RESERVED , 0 , 0 , 0 } , } ; static const rtwCAPI_SampleTimeMap
rtSampleTimeMap [ ] = { { ( const void * ) & rtcapiStoredFloats [ 0 ] , (
const void * ) & rtcapiStoredFloats [ 1 ] , 1 , 0 } , { ( const void * ) &
rtcapiStoredFloats [ 1 ] , ( const void * ) & rtcapiStoredFloats [ 1 ] , 0 ,
0 } , { ( NULL ) , ( NULL ) , 2 , 0 } } ; static
rtwCAPI_ModelMappingStaticInfo mmiStatic = { { rtBlockSignals , 54 ,
rtRootInputs , 0 , rtRootOutputs , 0 } , { rtBlockParameters , 113 ,
rtModelParameters , 5 } , { ( NULL ) , 0 } , { rtDataTypeMap , rtDimensionMap
, rtFixPtMap , rtElementMap , rtSampleTimeMap , rtDimensionArray } , "float"
, { 271311271U , 287428677U , 139058845U , 2583372187U } , ( NULL ) , 0 , 0 ,
rt_LoggedStateIdxList } ; const rtwCAPI_ModelMappingStaticInfo *
sim_Start_GetCAPIStaticMap ( void ) { return & mmiStatic ; }
#ifndef HOST_CAPI_BUILD
void sim_Start_InitializeDataMapInfo ( void ) { rtwCAPI_SetVersion ( ( *
rt_dataMapInfoPtr ) . mmi , 1 ) ; rtwCAPI_SetStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , & mmiStatic ) ; rtwCAPI_SetLoggingStaticMap ( ( *
rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ; rtwCAPI_SetDataAddressMap ( ( *
rt_dataMapInfoPtr ) . mmi , rtDataAddrMap ) ; rtwCAPI_SetVarDimsAddressMap (
( * rt_dataMapInfoPtr ) . mmi , rtVarDimsAddrMap ) ;
rtwCAPI_SetInstanceLoggingInfo ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArray ( ( * rt_dataMapInfoPtr ) . mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( ( * rt_dataMapInfoPtr ) . mmi , 0 ) ; }
#else
#ifdef __cplusplus
extern "C" {
#endif
void sim_Start_host_InitializeDataMapInfo ( sim_Start_host_DataMapInfo_T *
dataMap , const char * path ) { rtwCAPI_SetVersion ( dataMap -> mmi , 1 ) ;
rtwCAPI_SetStaticMap ( dataMap -> mmi , & mmiStatic ) ;
rtwCAPI_SetDataAddressMap ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetVarDimsAddressMap ( dataMap -> mmi , NULL ) ; rtwCAPI_SetPath (
dataMap -> mmi , path ) ; rtwCAPI_SetFullPath ( dataMap -> mmi , NULL ) ;
rtwCAPI_SetChildMMIArray ( dataMap -> mmi , ( NULL ) ) ;
rtwCAPI_SetChildMMIArrayLen ( dataMap -> mmi , 0 ) ; }
#ifdef __cplusplus
}
#endif
#endif
