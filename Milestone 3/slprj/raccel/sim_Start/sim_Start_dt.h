#include "ext_types.h"
static DataTypeInfo rtDataTypeInfoTable [ ] = { { "real_T" , 0 , 8 } , {
"real32_T" , 1 , 4 } , { "int8_T" , 2 , 1 } , { "uint8_T" , 3 , 1 } , {
"int16_T" , 4 , 2 } , { "uint16_T" , 5 , 2 } , { "int32_T" , 6 , 4 } , {
"uint32_T" , 7 , 4 } , { "boolean_T" , 8 , 1 } , { "fcn_call_T" , 9 , 0 } , {
"int_T" , 10 , 4 } , { "pointer_T" , 11 , 8 } , { "action_T" , 12 , 8 } , {
"timer_uint32_pair_T" , 13 , 8 } , { "string" , 14 , 8 } , { "string" , 15 ,
8 } } ; static uint_T rtDataTypeSizes [ ] = { sizeof ( real_T ) , sizeof (
real32_T ) , sizeof ( int8_T ) , sizeof ( uint8_T ) , sizeof ( int16_T ) ,
sizeof ( uint16_T ) , sizeof ( int32_T ) , sizeof ( uint32_T ) , sizeof (
boolean_T ) , sizeof ( fcn_call_T ) , sizeof ( int_T ) , sizeof ( void * ) ,
sizeof ( action_T ) , 2 * sizeof ( uint32_T ) , 8 , sizeof ( char_T ) } ;
static const char_T * rtDataTypeNames [ ] = { "real_T" , "real32_T" ,
"int8_T" , "uint8_T" , "int16_T" , "uint16_T" , "int32_T" , "uint32_T" ,
"boolean_T" , "fcn_call_T" , "int_T" , "pointer_T" , "action_T" ,
"timer_uint32_pair_T" , "string" , "string" } ; static DataTypeTransition
rtBTransitions [ ] = { { ( char_T * ) ( & rtB . izgo3pnyfb ) , 15 , 0 , 1 } ,
{ ( char_T * ) ( & rtB . o0ftqgjmrw ) , 15 , 0 , 1 } , { ( char_T * ) ( & rtB
. bkd0pdtznt ) , 15 , 0 , 1 } , { ( char_T * ) ( & rtB . dzwy32uw0n ) , 0 , 0
, 38 } , { ( char_T * ) ( & rtB . g2v4ytatik [ 0 ] ) , 3 , 0 , 62 } , { (
char_T * ) ( & rtB . faxpoeey0v ) , 8 , 0 , 5 } , { ( char_T * ) ( & rtDW .
pd03hpujxp ) , 0 , 0 , 1514 } , { ( char_T * ) ( & rtDW . icjcuyoesf ) , 15 ,
0 , 1 } , { ( char_T * ) ( & rtDW . a42pr4cd24 ) , 0 , 0 , 1 } , { ( char_T *
) ( & rtDW . ex0ljdtjz3 ) , 15 , 0 , 1 } , { ( char_T * ) ( & rtDW .
mwwhbb5fno ) , 0 , 0 , 1 } , { ( char_T * ) ( & rtDW . k3xrmmg34t . TimePtr )
, 11 , 0 , 4 } , { ( char_T * ) ( & rtDW . grgj1nbj2f ) , 6 , 0 , 3 } , { (
char_T * ) ( & rtDW . m1jml1zdzn ) , 7 , 0 , 3 } , { ( char_T * ) ( & rtDW .
fsmjxejob3 . PrevIndex ) , 10 , 0 , 4 } , { ( char_T * ) ( & rtDW .
kfkq4jq4py ) , 3 , 0 , 6 } , { ( char_T * ) ( & rtDW . jyzsgxunlp [ 0 ] .
dd10qqbw3x . b43cz3on2r ) , 7 , 0 , 1 } , { ( char_T * ) ( & rtDW .
btjpsv3ywi [ 0 ] . dd10qqbw3x . b43cz3on2r ) , 7 , 0 , 1 } , { ( char_T * ) (
& rtDW . cor5gsbd40 [ 0 ] . dd10qqbw3x . b43cz3on2r ) , 7 , 0 , 1 } , { (
char_T * ) ( & rtDW . eo3swzvasqb [ 0 ] . dd10qqbw3x . b43cz3on2r ) , 7 , 0 ,
1 } } ; static DataTypeTransitionTable rtBTransTable = { 20U , rtBTransitions
} ; static DataTypeTransition rtPTransitions [ ] = { { ( char_T * ) ( & rtP .
InputPWM [ 0 ] ) , 0 , 0 , 519 } , { ( char_T * ) ( & rtP .
UnitDelay_InitialCondition ) , 11 , 0 , 3 } , { ( char_T * ) ( & rtP .
Switch_Threshold_a2s0c4f3g1 ) , 1 , 0 , 4 } , { ( char_T * ) ( & rtP .
FromWorkspace3_Data0 [ 0 ] ) , 3 , 0 , 2511 } , { ( char_T * ) ( & rtP .
jyzsgxunlp . dd10qqbw3x . Gain1_Gain ) , 0 , 0 , 9 } , { ( char_T * ) ( & rtP
. jyzsgxunlp . dd10qqbw3x . DirectLookupTablenD_table [ 0 ] ) , 1 , 0 ,
63997270 } , { ( char_T * ) ( & rtP . jyzsgxunlp . dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput ) , 3 , 0 , 1 } , { ( char_T
* ) ( & rtP . btjpsv3ywi . dd10qqbw3x . Gain1_Gain ) , 0 , 0 , 9 } , { (
char_T * ) ( & rtP . btjpsv3ywi . dd10qqbw3x . DirectLookupTablenD_table [ 0
] ) , 1 , 0 , 63997270 } , { ( char_T * ) ( & rtP . btjpsv3ywi . dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput ) , 3 , 0 , 1 } , { ( char_T
* ) ( & rtP . cor5gsbd40 . dd10qqbw3x . Gain1_Gain ) , 0 , 0 , 9 } , { (
char_T * ) ( & rtP . cor5gsbd40 . dd10qqbw3x . DirectLookupTablenD_table [ 0
] ) , 1 , 0 , 63997270 } , { ( char_T * ) ( & rtP . cor5gsbd40 . dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput ) , 3 , 0 , 1 } , { ( char_T
* ) ( & rtP . eo3swzvasqb . dd10qqbw3x . Gain1_Gain ) , 0 , 0 , 9 } , { (
char_T * ) ( & rtP . eo3swzvasqb . dd10qqbw3x . DirectLookupTablenD_table [ 0
] ) , 1 , 0 , 63997270 } , { ( char_T * ) ( & rtP . eo3swzvasqb . dd10qqbw3x
. DirectLookupTablenD_DiagnosticForOutOfRangeInput ) , 3 , 0 , 1 } } ; static
DataTypeTransitionTable rtPTransTable = { 16U , rtPTransitions } ;
