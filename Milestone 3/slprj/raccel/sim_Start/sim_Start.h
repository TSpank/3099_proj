#ifndef RTW_HEADER_sim_Start_h_
#define RTW_HEADER_sim_Start_h_
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "rtw_modelmap_simtarget.h"
#ifndef sim_Start_COMMON_INCLUDES_
#define sim_Start_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "sigstream_rtw.h"
#include "simtarget/slSimTgtSigstreamRTW.h"
#include "simtarget/slSimTgtSlioCoreRTW.h"
#include "simtarget/slSimTgtSlioClientsRTW.h"
#include "simtarget/slSimTgtSlioSdiRTW.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "raccel.h"
#include "slsv_diagnostic_codegen_c_api.h"
#include "rt_logging_simtarget.h"
#include "dt_info.h"
#include "ext_work.h"
#endif
#include "sim_Start_types.h"
#include "multiword_types.h"
#include "mwmathutil.h"
#include "mwstringutil.h"
#include "rt_defines.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#define MODEL_NAME sim_Start
#define NSAMPLE_TIMES (3) 
#define NINPUTS (0)       
#define NOUTPUTS (0)     
#define NBLOCKIO (49) 
#define NUM_ZC_EVENTS (0) 
#ifndef NCSTATES
#define NCSTATES (0)   
#elif NCSTATES != 0
#error Invalid specification of NCSTATES defined in compiler command
#endif
#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm) (*rt_dataMapInfoPtr)
#endif
#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val) (rt_dataMapInfoPtr = &val)
#endif
#ifndef IN_RACCEL_MAIN
#endif
typedef struct { uint32_T b43cz3on2r ; } kn45tvceut ; typedef struct {
kn45tvceut dd10qqbw3x ; } bk4xk0dq22 ; typedef struct { void * izgo3pnyfb ;
void * o0ftqgjmrw ; void * bkd0pdtznt ; real_T dzwy32uw0n ; real_T h012lh2sdm
; real_T kawa0ssxug ; real_T hng4zofqfq ; real_T gnp2aeui1f ; real_T
p01c3bphbw ; real_T cf5djepmnt ; real_T hqdziciins ; real_T e41kschlmd ;
real_T jnbrvrbl4c ; real_T j4n5p1agp1 ; real_T o1ijyw24e0 ; real_T ezqnv40hor
; real_T dvpimpdrs3 ; real_T dkze5lu4ma ; real_T n4zowml5qq ; real_T
ltxnmikrnx ; real_T nm1wroc1sb ; real_T ktb4r2yzkb ; real_T n3rebawkox ;
real_T eryzgsl04j ; real_T ppg1emiqtt ; real_T orypiaqyjn ; real_T hskzxi0fod
; real_T chxyftuvao ; real_T dxmhlj5cu2 ; real_T jl4a230o5q ; real_T
pnwjxb31po ; real_T oqf5mpl1fz ; real_T jjkwpfenp4 ; real_T mo2yss1m1k ;
real_T i5w0dsemue ; real_T mtdmnunuoi ; real_T mjogabnmjg ; real_T ls0wcsfeyn
; real_T hldfmfuah3 ; real_T ii32dgziov ; real_T la1brdabwh ; uint8_T
g2v4ytatik [ 31 ] ; uint8_T jialwgyso4 [ 31 ] ; boolean_T faxpoeey0v ;
boolean_T br5qezvsfu ; boolean_T mql3fd1ucd ; boolean_T krhfkykqos ;
boolean_T amaivwoi3x ; } B ; typedef struct { real_T pd03hpujxp ; real_T
lruetkegpd ; real_T jlsempkvov ; real_T flos1pd3x0 ; real_T ileqgfdrx1 ;
real_T caiyox4oj2 [ 1500 ] ; real_T iafj1kgffe ; real_T gyqsentnna [ 3 ] ;
real_T j2fbjqvjcp ; real_T psgym2jyk0 [ 3 ] ; real_T p2qqabsh1z ; void *
icjcuyoesf ; real_T a42pr4cd24 ; void * ex0ljdtjz3 ; real_T mwwhbb5fno ;
struct { void * TimePtr ; void * DataPtr ; void * RSimInfoPtr ; } k3xrmmg34t
; struct { void * TimePtr ; void * DataPtr ; void * RSimInfoPtr ; }
pg5gatyniv ; struct { void * TimePtr ; void * DataPtr ; void * RSimInfoPtr ;
} jayrspqml1 ; struct { void * TimePtr ; void * DataPtr ; void * RSimInfoPtr
; } opv4zomgtb ; int32_T grgj1nbj2f ; int32_T fen0hw5zdb ; int32_T gbtehizmoh
; uint32_T m1jml1zdzn ; uint32_T co2n0fejda ; uint32_T mqobma15yv ; struct {
int_T PrevIndex ; } fsmjxejob3 ; struct { int_T PrevIndex ; } bn4k2qv04s ;
struct { int_T PrevIndex ; } j244bobwtj ; struct { int_T PrevIndex ; }
aatqqysjgr ; uint8_T kfkq4jq4py ; uint8_T bgn14fgjm3 ; uint8_T c1g1b2pcb5 ;
uint8_T njbocqae2t ; uint8_T nyhsr4t3fp ; uint8_T j04nfygwcf ; bk4xk0dq22
jyzsgxunlp [ 1 ] ; bk4xk0dq22 btjpsv3ywi [ 1 ] ; bk4xk0dq22 cor5gsbd40 [ 1 ]
; bk4xk0dq22 eo3swzvasqb [ 1 ] ; } DW ; typedef struct {
rtwCAPI_ModelMappingInfo mmi ; } DataMapInfo ; struct dicmktxnir_ { real_T
Gain1_Gain ; real_T Bias_Bias ; real_T Gain_Gain ; real_T Bias1_Bias ; real_T
Saturation2_UpperSat ; real_T Saturation2_LowerSat ; real_T
Saturation3_UpperSat ; real_T Saturation3_LowerSat ; real_T Constant_Value ;
real32_T DirectLookupTablenD_table [ 63997270 ] ; uint8_T
DirectLookupTablenD_DiagnosticForOutOfRangeInput ; } ; struct hbdtjxouhm_ {
dicmktxnir dd10qqbw3x ; } ; struct P_ { real_T InputPWM [ 173 ] ; real_T
WheelSpeed [ 173 ] ; real_T axleLength ; real_T lineLength ; real_T wheelR ;
real_T LeftLineSensor_envVal ; real_T LeftMiddleLineSensor_envVal ; real_T
RightLineSensor_envVal ; real_T RightMiddleLineSensor_envVal ; real_T
LeftLineSensor_lineVal ; real_T LeftMiddleLineSensor_lineVal ; real_T
RightLineSensor_lineVal ; real_T RightMiddleLineSensor_lineVal ; real_T
LeftMotor_sensorType ; real_T RightMotor_sensorType ; real_T
RobotSimulator_startTheta ; real_T RobotSimulator_startX ; real_T
RobotSimulator_startY ; real_T Towlwr_wheelR ; real_T Gain_Gain ; real_T
Gain_Gain_o4hmcpr5ca ; real_T UnitDelay1_InitialCondition ; real_T
UnitDelay1_InitialCondition_jmsdimwcgj ; real_T
DiscreteTimeIntegrator_gainval ; real_T DiscreteTimeIntegrator1_gainval ;
real_T DiscreteTimeIntegrator2_gainval ; real_T Delay_InitialCondition ;
real_T Switch2_Threshold ; real_T Switch3_Threshold ; real_T
DiscreteTimeIntegrator_gainval_afikqgzpe0 ; real_T DiscreteTimeIntegrator_IC
; real_T Internal_A [ 9 ] ; real_T Internal_B [ 3 ] ; real_T Internal_C [ 3 ]
; real_T Switch_Threshold ; real_T DiscreteTimeIntegrator_gainval_ptlrlhszmg
; real_T DiscreteTimeIntegrator_IC_e3x2ks5ohu ; real_T Internal_A_avp5ztxxzl
[ 9 ] ; real_T Internal_B_ebkdikdi50 [ 3 ] ; real_T Internal_C_hiyczu5j42 [ 3
] ; real_T Switch_Threshold_bmwdvzwwf2 ; real_T SoftRealTime_P1_Size [ 2 ] ;
real_T SoftRealTime_P1 ; real_T FromWorkspace2_Time0 ; real_T
FromWorkspace2_Data0 ; real_T FromWorkspace3_Time0 [ 81 ] ; real_T
FromWorkspace1_Time0 ; real_T FromWorkspace1_Data0 ; real_T
UnitDelay2_InitialCondition ; real_T FromWorkspace_Time0 ; real_T
FromWorkspace_Data0 ; real_T Switch1_Threshold ; real_T
Switch_Threshold_iry4v5n5zm ; real_T changeparameters_Gain [ 4 ] ; real_T
Constant_Value ; real_T Constant1_Value ; real_T Constant2_Value ; real_T
Circle_Value ; real_T Gain1_Gain ; real_T Constant_Value_hlhm5ajdso ; real_T
Constant1_Value_plr2cy0rrk ; real_T Constant_Value_olk5gkppvb ; real_T
Constant1_Value_oyulwwquga ; real_T Constant_Value_nizol5xyrr ; real_T
Constant1_Value_larkvagb33 ; real_T Constant_Value_gf0zpo0joi ; real_T
Constant1_Value_ih0jkfo3kb ; void * UnitDelay_InitialCondition ; void *
DataStoreMemory_InitialValue ; void * StringConstant_String ; real32_T
Switch_Threshold_a2s0c4f3g1 ; real32_T Switch_Threshold_aplx2fzo4d ; real32_T
Switch_Threshold_owqwz4ijh5 ; real32_T Switch_Threshold_ijwjdx5hx3 ; uint8_T
FromWorkspace3_Data0 [ 2511 ] ; hbdtjxouhm jyzsgxunlp ; hbdtjxouhm btjpsv3ywi
; hbdtjxouhm cor5gsbd40 ; hbdtjxouhm eo3swzvasqb ; } ; extern const char *
RT_MEMORY_ALLOCATION_ERROR ; extern B rtB ; extern DW rtDW ; extern P rtP ;
extern mxArray * mr_sim_Start_GetDWork ( ) ; extern void
mr_sim_Start_SetDWork ( const mxArray * ssDW ) ; extern mxArray *
mr_sim_Start_GetSimStateDisallowedBlocks ( ) ; extern const
rtwCAPI_ModelMappingStaticInfo * sim_Start_GetCAPIStaticMap ( void ) ; extern
SimStruct * const rtS ; extern const int_T gblNumToFiles ; extern const int_T
gblNumFrFiles ; extern const int_T gblNumFrWksBlocks ; extern rtInportTUtable
* gblInportTUtables ; extern const char * gblInportFileName ; extern const
int_T gblNumRootInportBlks ; extern const int_T gblNumModelInputs ; extern
const int_T gblInportDataTypeIdx [ ] ; extern const int_T gblInportDims [ ] ;
extern const int_T gblInportComplex [ ] ; extern const int_T
gblInportInterpoFlag [ ] ; extern const int_T gblInportContinuous [ ] ;
extern const int_T gblParameterTuningTid ; extern DataMapInfo *
rt_dataMapInfoPtr ; extern rtwCAPI_ModelMappingInfo * rt_modelMapInfoPtr ;
void MdlOutputs ( int_T tid ) ; void MdlOutputsParameterSampleTime ( int_T
tid ) ; void MdlUpdate ( int_T tid ) ; void MdlTerminate ( void ) ; void
MdlInitializeSizes ( void ) ; void MdlInitializeSampleTimes ( void ) ;
SimStruct * raccel_register_model ( ssExecutionInfo * executionInfo ) ;
#endif
