#ifndef RTW_HEADER_sim_Start_private_h_
#define RTW_HEADER_sim_Start_private_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "sim_Start.h"
#if !defined(rt_VALIDATE_MEMORY)
#define rt_VALIDATE_MEMORY(S, ptr)   if(!(ptr)) {\
  ssSetErrorStatus(rtS, RT_MEMORY_ALLOCATION_ERROR);\
  }
#endif
#if !defined(rt_FREE)
#if !defined(_WIN32)
#define rt_FREE(ptr)   if((ptr) != (NULL)) {\
  free((ptr));\
  (ptr) = (NULL);\
  }
#else
#define rt_FREE(ptr)   if((ptr) != (NULL)) {\
  free((void *)(ptr));\
  (ptr) = (NULL);\
  }
#endif
#endif
#ifndef rtInterpolate
#define rtInterpolate(v1,v2,f1,f2)   (((v1)==(v2))?((double)(v1)):  (((f1)*((double)(v1)))+((f2)*((double)(v2)))))
#endif
#ifndef rtRound
#define rtRound(v) ( ((v) >= 0) ?   muDoubleScalarFloor((v) + 0.5) :   muDoubleScalarCeil((v) - 0.5) )
#endif
extern uint32_T plook_u32d_binckan ( real_T u , const real_T bp [ ] ,
uint32_T maxIndex ) ; extern uint32_T binsearch_u32d ( real_T u , const
real_T bp [ ] , uint32_T startIndex , uint32_T maxIndex ) ; extern void
sfun_time ( SimStruct * rts ) ; extern void eg2eg1jqqe ( int32_T NumIters ,
bk4xk0dq22 localDW [ 1 ] ) ; extern void eo3swzvasq ( int32_T NumIters ,
SimStruct * rtS_m , real_T dvv1hyxjz0 , real_T f2q3bqwtam , real_T phn0bn0g41
, const real_T * e5hilnioyq , const real_T * amcbqloso2 , real32_T *
jjqq4ay2qq , bk4xk0dq22 localDW [ 1 ] , hbdtjxouhm * localP ) ; extern void
dfbtnmeqyz ( int32_T NumIters , SimStruct * rtS_g , bk4xk0dq22 localDW [ 1 ]
) ;
#if defined(MULTITASKING)
#error Model (sim_Start) was built in \SingleTasking solver mode, however the MULTITASKING define is \present. If you have multitasking (e.g. -DMT or -DMULTITASKING) \defined on the Code Generation page of Simulation parameter dialog, please \remove it and on the Solver page, select solver mode \MultiTasking. If the Simulation parameter dialog is configured \correctly, please verify that your template makefile is \configured correctly.
#endif
#endif
