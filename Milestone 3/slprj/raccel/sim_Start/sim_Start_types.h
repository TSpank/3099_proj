#ifndef RTW_HEADER_sim_Start_types_h_
#define RTW_HEADER_sim_Start_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#ifndef struct_emxArray_char_T
#define struct_emxArray_char_T
struct emxArray_char_T { char_T * data ; int32_T * size ; int32_T
allocatedSize ; int32_T numDimensions ; boolean_T canFreeData ; } ;
#endif
#ifndef typedef_hv2uegvrr5
#define typedef_hv2uegvrr5
typedef struct emxArray_char_T hv2uegvrr5 ;
#endif
typedef struct dicmktxnir_ dicmktxnir ; typedef struct hbdtjxouhm_ hbdtjxouhm
; typedef struct P_ P ;
#endif
