  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 16;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (rtP)
    ;%
      section.nData     = 67;
      section.data(67)  = dumData; %prealloc
      
	  ;% rtP.InputPWM
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.WheelSpeed
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 173;
	
	  ;% rtP.axleLength
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 346;
	
	  ;% rtP.lineLength
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 347;
	
	  ;% rtP.wheelR
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 348;
	
	  ;% rtP.LeftLineSensor_envVal
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 349;
	
	  ;% rtP.LeftMiddleLineSensor_envVal
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 350;
	
	  ;% rtP.RightLineSensor_envVal
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 351;
	
	  ;% rtP.RightMiddleLineSensor_envVal
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 352;
	
	  ;% rtP.LeftLineSensor_lineVal
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 353;
	
	  ;% rtP.LeftMiddleLineSensor_lineVal
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 354;
	
	  ;% rtP.RightLineSensor_lineVal
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 355;
	
	  ;% rtP.RightMiddleLineSensor_lineVal
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 356;
	
	  ;% rtP.LeftMotor_sensorType
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 357;
	
	  ;% rtP.RightMotor_sensorType
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 358;
	
	  ;% rtP.RobotSimulator_startTheta
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 359;
	
	  ;% rtP.RobotSimulator_startX
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 360;
	
	  ;% rtP.RobotSimulator_startY
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 361;
	
	  ;% rtP.Towlwr_wheelR
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 362;
	
	  ;% rtP.Gain_Gain
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 363;
	
	  ;% rtP.Gain_Gain_o4hmcpr5ca
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 364;
	
	  ;% rtP.UnitDelay1_InitialCondition
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 365;
	
	  ;% rtP.UnitDelay1_InitialCondition_jmsdimwcgj
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 366;
	
	  ;% rtP.DiscreteTimeIntegrator_gainval
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 367;
	
	  ;% rtP.DiscreteTimeIntegrator1_gainval
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 368;
	
	  ;% rtP.DiscreteTimeIntegrator2_gainval
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 369;
	
	  ;% rtP.Delay_InitialCondition
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 370;
	
	  ;% rtP.Switch2_Threshold
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 371;
	
	  ;% rtP.Switch3_Threshold
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 372;
	
	  ;% rtP.DiscreteTimeIntegrator_gainval_afikqgzpe0
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 373;
	
	  ;% rtP.DiscreteTimeIntegrator_IC
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 374;
	
	  ;% rtP.Internal_A
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 375;
	
	  ;% rtP.Internal_B
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 384;
	
	  ;% rtP.Internal_C
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 387;
	
	  ;% rtP.Switch_Threshold
	  section.data(35).logicalSrcIdx = 36;
	  section.data(35).dtTransOffset = 390;
	
	  ;% rtP.DiscreteTimeIntegrator_gainval_ptlrlhszmg
	  section.data(36).logicalSrcIdx = 37;
	  section.data(36).dtTransOffset = 391;
	
	  ;% rtP.DiscreteTimeIntegrator_IC_e3x2ks5ohu
	  section.data(37).logicalSrcIdx = 38;
	  section.data(37).dtTransOffset = 392;
	
	  ;% rtP.Internal_A_avp5ztxxzl
	  section.data(38).logicalSrcIdx = 39;
	  section.data(38).dtTransOffset = 393;
	
	  ;% rtP.Internal_B_ebkdikdi50
	  section.data(39).logicalSrcIdx = 40;
	  section.data(39).dtTransOffset = 402;
	
	  ;% rtP.Internal_C_hiyczu5j42
	  section.data(40).logicalSrcIdx = 41;
	  section.data(40).dtTransOffset = 405;
	
	  ;% rtP.Switch_Threshold_bmwdvzwwf2
	  section.data(41).logicalSrcIdx = 44;
	  section.data(41).dtTransOffset = 408;
	
	  ;% rtP.SoftRealTime_P1_Size
	  section.data(42).logicalSrcIdx = 45;
	  section.data(42).dtTransOffset = 409;
	
	  ;% rtP.SoftRealTime_P1
	  section.data(43).logicalSrcIdx = 46;
	  section.data(43).dtTransOffset = 411;
	
	  ;% rtP.FromWorkspace2_Time0
	  section.data(44).logicalSrcIdx = 47;
	  section.data(44).dtTransOffset = 412;
	
	  ;% rtP.FromWorkspace2_Data0
	  section.data(45).logicalSrcIdx = 48;
	  section.data(45).dtTransOffset = 413;
	
	  ;% rtP.FromWorkspace3_Time0
	  section.data(46).logicalSrcIdx = 49;
	  section.data(46).dtTransOffset = 414;
	
	  ;% rtP.FromWorkspace1_Time0
	  section.data(47).logicalSrcIdx = 50;
	  section.data(47).dtTransOffset = 495;
	
	  ;% rtP.FromWorkspace1_Data0
	  section.data(48).logicalSrcIdx = 51;
	  section.data(48).dtTransOffset = 496;
	
	  ;% rtP.UnitDelay2_InitialCondition
	  section.data(49).logicalSrcIdx = 52;
	  section.data(49).dtTransOffset = 497;
	
	  ;% rtP.FromWorkspace_Time0
	  section.data(50).logicalSrcIdx = 53;
	  section.data(50).dtTransOffset = 498;
	
	  ;% rtP.FromWorkspace_Data0
	  section.data(51).logicalSrcIdx = 54;
	  section.data(51).dtTransOffset = 499;
	
	  ;% rtP.Switch1_Threshold
	  section.data(52).logicalSrcIdx = 55;
	  section.data(52).dtTransOffset = 500;
	
	  ;% rtP.Switch_Threshold_iry4v5n5zm
	  section.data(53).logicalSrcIdx = 56;
	  section.data(53).dtTransOffset = 501;
	
	  ;% rtP.changeparameters_Gain
	  section.data(54).logicalSrcIdx = 57;
	  section.data(54).dtTransOffset = 502;
	
	  ;% rtP.Constant_Value
	  section.data(55).logicalSrcIdx = 58;
	  section.data(55).dtTransOffset = 506;
	
	  ;% rtP.Constant1_Value
	  section.data(56).logicalSrcIdx = 59;
	  section.data(56).dtTransOffset = 507;
	
	  ;% rtP.Constant2_Value
	  section.data(57).logicalSrcIdx = 60;
	  section.data(57).dtTransOffset = 508;
	
	  ;% rtP.Circle_Value
	  section.data(58).logicalSrcIdx = 61;
	  section.data(58).dtTransOffset = 509;
	
	  ;% rtP.Gain1_Gain
	  section.data(59).logicalSrcIdx = 62;
	  section.data(59).dtTransOffset = 510;
	
	  ;% rtP.Constant_Value_hlhm5ajdso
	  section.data(60).logicalSrcIdx = 63;
	  section.data(60).dtTransOffset = 511;
	
	  ;% rtP.Constant1_Value_plr2cy0rrk
	  section.data(61).logicalSrcIdx = 64;
	  section.data(61).dtTransOffset = 512;
	
	  ;% rtP.Constant_Value_olk5gkppvb
	  section.data(62).logicalSrcIdx = 65;
	  section.data(62).dtTransOffset = 513;
	
	  ;% rtP.Constant1_Value_oyulwwquga
	  section.data(63).logicalSrcIdx = 66;
	  section.data(63).dtTransOffset = 514;
	
	  ;% rtP.Constant_Value_nizol5xyrr
	  section.data(64).logicalSrcIdx = 67;
	  section.data(64).dtTransOffset = 515;
	
	  ;% rtP.Constant1_Value_larkvagb33
	  section.data(65).logicalSrcIdx = 68;
	  section.data(65).dtTransOffset = 516;
	
	  ;% rtP.Constant_Value_gf0zpo0joi
	  section.data(66).logicalSrcIdx = 69;
	  section.data(66).dtTransOffset = 517;
	
	  ;% rtP.Constant1_Value_ih0jkfo3kb
	  section.data(67).logicalSrcIdx = 70;
	  section.data(67).dtTransOffset = 518;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% rtP.UnitDelay_InitialCondition
	  section.data(1).logicalSrcIdx = 71;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.DataStoreMemory_InitialValue
	  section.data(2).logicalSrcIdx = 72;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtP.StringConstant_String
	  section.data(3).logicalSrcIdx = 73;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% rtP.Switch_Threshold_a2s0c4f3g1
	  section.data(1).logicalSrcIdx = 74;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.Switch_Threshold_aplx2fzo4d
	  section.data(2).logicalSrcIdx = 75;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtP.Switch_Threshold_owqwz4ijh5
	  section.data(3).logicalSrcIdx = 76;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtP.Switch_Threshold_ijwjdx5hx3
	  section.data(4).logicalSrcIdx = 77;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.FromWorkspace3_Data0
	  section.data(1).logicalSrcIdx = 78;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Gain1_Gain
	  section.data(1).logicalSrcIdx = 79;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Bias_Bias
	  section.data(2).logicalSrcIdx = 80;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Gain_Gain
	  section.data(3).logicalSrcIdx = 81;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Bias1_Bias
	  section.data(4).logicalSrcIdx = 82;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Saturation2_UpperSat
	  section.data(5).logicalSrcIdx = 83;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Saturation2_LowerSat
	  section.data(6).logicalSrcIdx = 84;
	  section.data(6).dtTransOffset = 5;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Saturation3_UpperSat
	  section.data(7).logicalSrcIdx = 85;
	  section.data(7).dtTransOffset = 6;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Saturation3_LowerSat
	  section.data(8).logicalSrcIdx = 86;
	  section.data(8).dtTransOffset = 7;
	
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.Constant_Value
	  section.data(9).logicalSrcIdx = 87;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(5) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.DirectLookupTablenD_table
	  section.data(1).logicalSrcIdx = 88;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(6) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.jyzsgxunlp.dd10qqbw3x.DirectLookupTablenD_DiagnosticForOutOfRangeInput
	  section.data(1).logicalSrcIdx = 89;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(7) = section;
      clear section
      
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Gain1_Gain
	  section.data(1).logicalSrcIdx = 90;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Bias_Bias
	  section.data(2).logicalSrcIdx = 91;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Gain_Gain
	  section.data(3).logicalSrcIdx = 92;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Bias1_Bias
	  section.data(4).logicalSrcIdx = 93;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Saturation2_UpperSat
	  section.data(5).logicalSrcIdx = 94;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Saturation2_LowerSat
	  section.data(6).logicalSrcIdx = 95;
	  section.data(6).dtTransOffset = 5;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Saturation3_UpperSat
	  section.data(7).logicalSrcIdx = 96;
	  section.data(7).dtTransOffset = 6;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Saturation3_LowerSat
	  section.data(8).logicalSrcIdx = 97;
	  section.data(8).dtTransOffset = 7;
	
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.Constant_Value
	  section.data(9).logicalSrcIdx = 98;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(8) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.DirectLookupTablenD_table
	  section.data(1).logicalSrcIdx = 99;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(9) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.btjpsv3ywi.dd10qqbw3x.DirectLookupTablenD_DiagnosticForOutOfRangeInput
	  section.data(1).logicalSrcIdx = 100;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(10) = section;
      clear section
      
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Gain1_Gain
	  section.data(1).logicalSrcIdx = 101;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Bias_Bias
	  section.data(2).logicalSrcIdx = 102;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Gain_Gain
	  section.data(3).logicalSrcIdx = 103;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Bias1_Bias
	  section.data(4).logicalSrcIdx = 104;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Saturation2_UpperSat
	  section.data(5).logicalSrcIdx = 105;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Saturation2_LowerSat
	  section.data(6).logicalSrcIdx = 106;
	  section.data(6).dtTransOffset = 5;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Saturation3_UpperSat
	  section.data(7).logicalSrcIdx = 107;
	  section.data(7).dtTransOffset = 6;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Saturation3_LowerSat
	  section.data(8).logicalSrcIdx = 108;
	  section.data(8).dtTransOffset = 7;
	
	  ;% rtP.cor5gsbd40.dd10qqbw3x.Constant_Value
	  section.data(9).logicalSrcIdx = 109;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.cor5gsbd40.dd10qqbw3x.DirectLookupTablenD_table
	  section.data(1).logicalSrcIdx = 110;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.cor5gsbd40.dd10qqbw3x.DirectLookupTablenD_DiagnosticForOutOfRangeInput
	  section.data(1).logicalSrcIdx = 111;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(13) = section;
      clear section
      
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Gain1_Gain
	  section.data(1).logicalSrcIdx = 112;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Bias_Bias
	  section.data(2).logicalSrcIdx = 113;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Gain_Gain
	  section.data(3).logicalSrcIdx = 114;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Bias1_Bias
	  section.data(4).logicalSrcIdx = 115;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Saturation2_UpperSat
	  section.data(5).logicalSrcIdx = 116;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Saturation2_LowerSat
	  section.data(6).logicalSrcIdx = 117;
	  section.data(6).dtTransOffset = 5;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Saturation3_UpperSat
	  section.data(7).logicalSrcIdx = 118;
	  section.data(7).dtTransOffset = 6;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Saturation3_LowerSat
	  section.data(8).logicalSrcIdx = 119;
	  section.data(8).dtTransOffset = 7;
	
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.Constant_Value
	  section.data(9).logicalSrcIdx = 120;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(14) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.DirectLookupTablenD_table
	  section.data(1).logicalSrcIdx = 121;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(15) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtP.eo3swzvasqb.dd10qqbw3x.DirectLookupTablenD_DiagnosticForOutOfRangeInput
	  section.data(1).logicalSrcIdx = 122;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(16) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 6;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (rtB)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtB.izgo3pnyfb
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtB.o0ftqgjmrw
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtB.bkd0pdtznt
	  section.data(1).logicalSrcIdx = 2;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(3) = section;
      clear section
      
      section.nData     = 38;
      section.data(38)  = dumData; %prealloc
      
	  ;% rtB.dzwy32uw0n
	  section.data(1).logicalSrcIdx = 3;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtB.h012lh2sdm
	  section.data(2).logicalSrcIdx = 4;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtB.kawa0ssxug
	  section.data(3).logicalSrcIdx = 5;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtB.hng4zofqfq
	  section.data(4).logicalSrcIdx = 6;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtB.gnp2aeui1f
	  section.data(5).logicalSrcIdx = 7;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtB.p01c3bphbw
	  section.data(6).logicalSrcIdx = 8;
	  section.data(6).dtTransOffset = 5;
	
	  ;% rtB.cf5djepmnt
	  section.data(7).logicalSrcIdx = 9;
	  section.data(7).dtTransOffset = 6;
	
	  ;% rtB.hqdziciins
	  section.data(8).logicalSrcIdx = 10;
	  section.data(8).dtTransOffset = 7;
	
	  ;% rtB.e41kschlmd
	  section.data(9).logicalSrcIdx = 11;
	  section.data(9).dtTransOffset = 8;
	
	  ;% rtB.jnbrvrbl4c
	  section.data(10).logicalSrcIdx = 12;
	  section.data(10).dtTransOffset = 9;
	
	  ;% rtB.j4n5p1agp1
	  section.data(11).logicalSrcIdx = 13;
	  section.data(11).dtTransOffset = 10;
	
	  ;% rtB.o1ijyw24e0
	  section.data(12).logicalSrcIdx = 14;
	  section.data(12).dtTransOffset = 11;
	
	  ;% rtB.ezqnv40hor
	  section.data(13).logicalSrcIdx = 15;
	  section.data(13).dtTransOffset = 12;
	
	  ;% rtB.dvpimpdrs3
	  section.data(14).logicalSrcIdx = 16;
	  section.data(14).dtTransOffset = 13;
	
	  ;% rtB.dkze5lu4ma
	  section.data(15).logicalSrcIdx = 17;
	  section.data(15).dtTransOffset = 14;
	
	  ;% rtB.n4zowml5qq
	  section.data(16).logicalSrcIdx = 18;
	  section.data(16).dtTransOffset = 15;
	
	  ;% rtB.ltxnmikrnx
	  section.data(17).logicalSrcIdx = 19;
	  section.data(17).dtTransOffset = 16;
	
	  ;% rtB.nm1wroc1sb
	  section.data(18).logicalSrcIdx = 20;
	  section.data(18).dtTransOffset = 17;
	
	  ;% rtB.ktb4r2yzkb
	  section.data(19).logicalSrcIdx = 21;
	  section.data(19).dtTransOffset = 18;
	
	  ;% rtB.n3rebawkox
	  section.data(20).logicalSrcIdx = 22;
	  section.data(20).dtTransOffset = 19;
	
	  ;% rtB.eryzgsl04j
	  section.data(21).logicalSrcIdx = 23;
	  section.data(21).dtTransOffset = 20;
	
	  ;% rtB.ppg1emiqtt
	  section.data(22).logicalSrcIdx = 24;
	  section.data(22).dtTransOffset = 21;
	
	  ;% rtB.orypiaqyjn
	  section.data(23).logicalSrcIdx = 25;
	  section.data(23).dtTransOffset = 22;
	
	  ;% rtB.hskzxi0fod
	  section.data(24).logicalSrcIdx = 26;
	  section.data(24).dtTransOffset = 23;
	
	  ;% rtB.chxyftuvao
	  section.data(25).logicalSrcIdx = 27;
	  section.data(25).dtTransOffset = 24;
	
	  ;% rtB.dxmhlj5cu2
	  section.data(26).logicalSrcIdx = 28;
	  section.data(26).dtTransOffset = 25;
	
	  ;% rtB.jl4a230o5q
	  section.data(27).logicalSrcIdx = 29;
	  section.data(27).dtTransOffset = 26;
	
	  ;% rtB.pnwjxb31po
	  section.data(28).logicalSrcIdx = 30;
	  section.data(28).dtTransOffset = 27;
	
	  ;% rtB.oqf5mpl1fz
	  section.data(29).logicalSrcIdx = 31;
	  section.data(29).dtTransOffset = 28;
	
	  ;% rtB.jjkwpfenp4
	  section.data(30).logicalSrcIdx = 32;
	  section.data(30).dtTransOffset = 29;
	
	  ;% rtB.mo2yss1m1k
	  section.data(31).logicalSrcIdx = 33;
	  section.data(31).dtTransOffset = 30;
	
	  ;% rtB.i5w0dsemue
	  section.data(32).logicalSrcIdx = 34;
	  section.data(32).dtTransOffset = 31;
	
	  ;% rtB.mtdmnunuoi
	  section.data(33).logicalSrcIdx = 35;
	  section.data(33).dtTransOffset = 32;
	
	  ;% rtB.mjogabnmjg
	  section.data(34).logicalSrcIdx = 36;
	  section.data(34).dtTransOffset = 33;
	
	  ;% rtB.ls0wcsfeyn
	  section.data(35).logicalSrcIdx = 37;
	  section.data(35).dtTransOffset = 34;
	
	  ;% rtB.hldfmfuah3
	  section.data(36).logicalSrcIdx = 38;
	  section.data(36).dtTransOffset = 35;
	
	  ;% rtB.ii32dgziov
	  section.data(37).logicalSrcIdx = 39;
	  section.data(37).dtTransOffset = 36;
	
	  ;% rtB.la1brdabwh
	  section.data(38).logicalSrcIdx = 41;
	  section.data(38).dtTransOffset = 37;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(4) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% rtB.g2v4ytatik
	  section.data(1).logicalSrcIdx = 42;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtB.jialwgyso4
	  section.data(2).logicalSrcIdx = 43;
	  section.data(2).dtTransOffset = 31;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(5) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% rtB.faxpoeey0v
	  section.data(1).logicalSrcIdx = 44;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtB.br5qezvsfu
	  section.data(2).logicalSrcIdx = 45;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtB.mql3fd1ucd
	  section.data(3).logicalSrcIdx = 46;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtB.krhfkykqos
	  section.data(4).logicalSrcIdx = 47;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtB.amaivwoi3x
	  section.data(5).logicalSrcIdx = 48;
	  section.data(5).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(6) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 14;
    sectIdxOffset = 6;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (rtDW)
    ;%
      section.nData     = 11;
      section.data(11)  = dumData; %prealloc
      
	  ;% rtDW.pd03hpujxp
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtDW.lruetkegpd
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtDW.jlsempkvov
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtDW.flos1pd3x0
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtDW.ileqgfdrx1
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtDW.caiyox4oj2
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% rtDW.iafj1kgffe
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 1505;
	
	  ;% rtDW.gyqsentnna
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 1506;
	
	  ;% rtDW.j2fbjqvjcp
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 1509;
	
	  ;% rtDW.psgym2jyk0
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 1510;
	
	  ;% rtDW.p2qqabsh1z
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 1513;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.icjcuyoesf
	  section.data(1).logicalSrcIdx = 11;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.a42pr4cd24
	  section.data(1).logicalSrcIdx = 12;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.ex0ljdtjz3
	  section.data(1).logicalSrcIdx = 13;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.mwwhbb5fno
	  section.data(1).logicalSrcIdx = 14;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% rtDW.k3xrmmg34t.TimePtr
	  section.data(1).logicalSrcIdx = 15;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtDW.pg5gatyniv.TimePtr
	  section.data(2).logicalSrcIdx = 16;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtDW.jayrspqml1.TimePtr
	  section.data(3).logicalSrcIdx = 17;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtDW.opv4zomgtb.TimePtr
	  section.data(4).logicalSrcIdx = 18;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% rtDW.grgj1nbj2f
	  section.data(1).logicalSrcIdx = 19;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtDW.fen0hw5zdb
	  section.data(2).logicalSrcIdx = 20;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtDW.gbtehizmoh
	  section.data(3).logicalSrcIdx = 21;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(7) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% rtDW.m1jml1zdzn
	  section.data(1).logicalSrcIdx = 22;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtDW.co2n0fejda
	  section.data(2).logicalSrcIdx = 23;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtDW.mqobma15yv
	  section.data(3).logicalSrcIdx = 24;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(8) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% rtDW.fsmjxejob3.PrevIndex
	  section.data(1).logicalSrcIdx = 25;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtDW.bn4k2qv04s.PrevIndex
	  section.data(2).logicalSrcIdx = 26;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtDW.j244bobwtj.PrevIndex
	  section.data(3).logicalSrcIdx = 27;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtDW.aatqqysjgr.PrevIndex
	  section.data(4).logicalSrcIdx = 28;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(9) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% rtDW.kfkq4jq4py
	  section.data(1).logicalSrcIdx = 29;
	  section.data(1).dtTransOffset = 0;
	
	  ;% rtDW.bgn14fgjm3
	  section.data(2).logicalSrcIdx = 30;
	  section.data(2).dtTransOffset = 1;
	
	  ;% rtDW.c1g1b2pcb5
	  section.data(3).logicalSrcIdx = 31;
	  section.data(3).dtTransOffset = 2;
	
	  ;% rtDW.njbocqae2t
	  section.data(4).logicalSrcIdx = 32;
	  section.data(4).dtTransOffset = 3;
	
	  ;% rtDW.nyhsr4t3fp
	  section.data(5).logicalSrcIdx = 33;
	  section.data(5).dtTransOffset = 4;
	
	  ;% rtDW.j04nfygwcf
	  section.data(6).logicalSrcIdx = 34;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(10) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.jyzsgxunlp[0].dd10qqbw3x.b43cz3on2r
	  section.data(1).logicalSrcIdx = 35;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.btjpsv3ywi[0].dd10qqbw3x.b43cz3on2r
	  section.data(1).logicalSrcIdx = 36;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.cor5gsbd40[0].dd10qqbw3x.b43cz3on2r
	  section.data(1).logicalSrcIdx = 37;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(13) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% rtDW.eo3swzvasqb[0].dd10qqbw3x.b43cz3on2r
	  section.data(1).logicalSrcIdx = 38;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(14) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 271311271;
  targMap.checksum1 = 287428677;
  targMap.checksum2 = 139058845;
  targMap.checksum3 = 2583372187;

