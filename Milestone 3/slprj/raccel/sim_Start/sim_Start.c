#include "rt_logging_mmi.h"
#include "sim_Start_capi.h"
#include <math.h>
#include "sim_Start.h"
#include "sim_Start_private.h"
#include "sim_Start_dt.h"
#include "sfcn_loader_c_api.h"
extern void * CreateDiagnosticAsVoidPtr_wrapper ( const char * id , int nargs
, ... ) ; RTWExtModeInfo * gblRTWExtModeInfo = NULL ; void
raccelForceExtModeShutdown ( boolean_T extModeStartPktReceived ) { if ( !
extModeStartPktReceived ) { boolean_T stopRequested = false ;
rtExtModeWaitForStartPkt ( gblRTWExtModeInfo , 2 , & stopRequested ) ; }
rtExtModeShutdown ( 2 ) ; }
#include "slsv_diagnostic_codegen_c_api.h"
#include "slsa_sim_engine.h"
const int_T gblNumToFiles = 0 ; const int_T gblNumFrFiles = 0 ; const int_T
gblNumFrWksBlocks = 4 ;
#ifdef RSIM_WITH_SOLVER_MULTITASKING
boolean_T gbl_raccel_isMultitasking = 1 ;
#else
boolean_T gbl_raccel_isMultitasking = 0 ;
#endif
boolean_T gbl_raccel_tid01eq = 1 ; int_T gbl_raccel_NumST = 3 ; const char_T
* gbl_raccel_Version = "10.3 (R2021a) 14-Nov-2020" ; void
raccel_setup_MMIStateLog ( SimStruct * S ) {
#ifdef UseMMIDataLogging
rt_FillStateSigInfoFromMMI ( ssGetRTWLogInfo ( S ) , & ssGetErrorStatus ( S )
) ;
#else
UNUSED_PARAMETER ( S ) ;
#endif
} static DataMapInfo rt_dataMapInfo ; DataMapInfo * rt_dataMapInfoPtr = &
rt_dataMapInfo ; rtwCAPI_ModelMappingInfo * rt_modelMapInfoPtr = & (
rt_dataMapInfo . mmi ) ; const int_T gblNumRootInportBlks = 0 ; const int_T
gblNumModelInputs = 0 ; extern const char * gblInportFileName ; extern
rtInportTUtable * gblInportTUtables ; const int_T gblInportDataTypeIdx [ ] =
{ - 1 } ; const int_T gblInportDims [ ] = { - 1 } ; const int_T
gblInportComplex [ ] = { - 1 } ; const int_T gblInportInterpoFlag [ ] = { - 1
} ; const int_T gblInportContinuous [ ] = { - 1 } ; int_T enableFcnCallFlag [
] = { 1 , 1 , 1 } ; const char * raccelLoadInputsAndAperiodicHitTimes (
SimStruct * S , const char * inportFileName , int * matFileFormat ) { return
rt_RAccelReadInportsMatFile ( S , inportFileName , matFileFormat ) ; }
#include "simstruc.h"
#include "fixedpoint.h"
#include "slsa_sim_engine.h"
#include "simtarget/slSimTgtSLExecSimBridge.h"
#define b054agindn (5U)
#define cqttlgnk1c (1U)
#define d0pes32jmr (4U)
#define ddgtoumbar (8U)
#define ewdqahmcux (11U)
#define fp2devqxsy (18U)
#define fphfoyh1fa (15U)
#define hps0ylwak0 (-1)
#define ihinm10c3b (12U)
#define ikr1dxkfwn (9U)
#define j1pnblzda5 (7U)
#define m3hjzoz043 (14U)
#define mbnkdlgcja (6U)
#define n34wplmkl4 (3U)
#define nvtrslvbe5 (2U)
#define nymocdvpmn (13U)
#define o3u1pgvs0u (16U)
#define oq4m20xxx1 (10U)
#define payc5e5ryc (17U)
#define agou4iuzhb (27U)
#define akz14vksph (5U)
#define atdrzrw5eu (10U)
#define bqiwxt5htw (23U)
#define bt3at1vcd3 (1U)
#define btej00r3bi (18U)
#define cj1fybdhr4 (14U)
#define ckjnuczefy (21U)
#define dpv45dh4v0 (7U)
#define dqhmmrgkr0 (30U)
#define ep4d23adrm (8U)
#define fxmqe1tj5l (20U)
#define g2xlvoqnxr (29U)
#define gis0nzutnc (6U)
#define gk5y14lpon (26U)
#define h2qkac2pap (34U)
#define i5smfx3fdn (33U)
#define irhhvjkqic (16U)
#define iua1sbu0j3 (28U)
#define jm04rqucdj (17U)
#define jwn2n42iuk (31U)
#define kw41sjmrpq (3U)
#define m0zspltx2s (32U)
#define maxri0m3or (24U)
#define msiuhxxah5 (11U)
#define ncvh5u3vyd (2U)
#define njy1ivq5gt (13U)
#define nli3jwkuqj (25U)
#define nmpsodxnf1 (12U)
#define nrqrzdpjwp (9U)
#define nuhjvypsnz (19U)
#define orvtclpxx4 (22U)
#define ovb3zhrf5j (15U)
#define bxfbrmulsm (12U)
#define cqizbzg1ld (9U)
#define crdnmesixf (6U)
#define dywneg1ovl (4U)
#define flmi0v3t0n (8U)
#define fnusxpnwqe (17U)
#define g1nz51j2op (15U)
#define i0c2gg1zyu (14U)
#define jfua5qkldn (7U)
#define jg3n5uukzi (10U)
#define jzjomt55gg (3U)
#define kpnsrpe45j (16U)
#define mf0n0qtzom (5U)
#define nhjyaksagi (11U)
#define p5vg45rs3h (13U)
B rtB ; DW rtDW ; static SimStruct model_S ; SimStruct * const rtS = &
model_S ; static void inbf3ttenx ( hv2uegvrr5 * * pEmxArray , int32_T
numDimensions ) ; static void dkx1govs0g ( void ) ; static void m1k31lb0rc (
void ) ; static void hip4glo4zv ( void ) ; static void fzjvjaawpn (
hv2uegvrr5 * emxArray , int32_T oldNumel ) ; static void j42rpwppwg (
hv2uegvrr5 * * pEmxArray ) ; static void fc04r2nndv ( void ) ; static void
nghjzs0ayn ( const hv2uegvrr5 * varargin_1_Value , hv2uegvrr5 * y_Value ) ;
static void nghjzs0ayn1 ( const hv2uegvrr5 * varargin_1_Value , hv2uegvrr5 *
y_Value ) ; static void nghjzs0ayn1g ( const hv2uegvrr5 * varargin_1_Value ,
hv2uegvrr5 * y_Value ) ; static void nghjzs0ayn1gl ( const hv2uegvrr5 *
varargin_1_Value , hv2uegvrr5 * y_Value ) ; static void nghjzs0ayn1glc (
const hv2uegvrr5 * varargin_1_Value , hv2uegvrr5 * y_Value ) ; uint32_T
plook_u32d_binckan ( real_T u , const real_T bp [ ] , uint32_T maxIndex ) {
uint32_T bpIndex ; if ( u <= bp [ 0U ] ) { bpIndex = 0U ; } else if ( u < bp
[ maxIndex ] ) { bpIndex = binsearch_u32d ( u , bp , maxIndex >> 1U ,
maxIndex ) ; if ( ( bpIndex < maxIndex ) && ( bp [ bpIndex + 1U ] - u <= u -
bp [ bpIndex ] ) ) { bpIndex ++ ; } } else { bpIndex = maxIndex ; } return
bpIndex ; } uint32_T binsearch_u32d ( real_T u , const real_T bp [ ] ,
uint32_T startIndex , uint32_T maxIndex ) { uint32_T bpIdx ; uint32_T bpIndex
; uint32_T iRght ; bpIdx = startIndex ; bpIndex = 0U ; iRght = maxIndex ;
while ( iRght - bpIndex > 1U ) { if ( u < bp [ bpIdx ] ) { iRght = bpIdx ; }
else { bpIndex = bpIdx ; } bpIdx = ( iRght + bpIndex ) >> 1U ; } return
bpIndex ; } void eg2eg1jqqe ( int32_T NumIters , bk4xk0dq22 localDW [ 1 ] ) {
int32_T l2ubquqqj2 ; for ( l2ubquqqj2 = 0 ; l2ubquqqj2 < NumIters ;
l2ubquqqj2 ++ ) { localDW [ l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r = 0U ; } }
void eo3swzvasq ( int32_T NumIters , SimStruct * rtS_m , real_T dvv1hyxjz0 ,
real_T f2q3bqwtam , real_T phn0bn0g41 , const real_T * e5hilnioyq , const
real_T * amcbqloso2 , real32_T * jjqq4ay2qq , bk4xk0dq22 localDW [ 1 ] ,
hbdtjxouhm * localP ) { int32_T l2ubquqqj2 ; real_T tmp_p [ 9 ] ; real_T
transPt [ 3 ] ; real_T djvdsf2p5q ; real_T kgyqh2swi5 ; real_T tmp ; int32_T
i ; void * diag ; for ( l2ubquqqj2 = 0 ; l2ubquqqj2 < NumIters ; l2ubquqqj2
++ ) { tmp_p [ 0 ] = muDoubleScalarCos ( phn0bn0g41 ) ; tmp_p [ 3 ] = -
muDoubleScalarSin ( phn0bn0g41 ) ; tmp_p [ 6 ] = 0.0 ; tmp_p [ 1 ] =
muDoubleScalarSin ( phn0bn0g41 ) ; tmp_p [ 4 ] = muDoubleScalarCos (
phn0bn0g41 ) ; tmp_p [ 7 ] = 0.0 ; tmp_p [ 2 ] = 0.0 ; tmp_p [ 5 ] = 0.0 ;
tmp_p [ 8 ] = 1.0 ; for ( i = 0 ; i < 3 ; i ++ ) { transPt [ i ] = tmp_p [ i
+ 3 ] * amcbqloso2 [ l2ubquqqj2 ] + tmp_p [ i ] * e5hilnioyq [ l2ubquqqj2 ] ;
} kgyqh2swi5 = muDoubleScalarRound ( localP -> dd10qqbw3x . Constant_Value -
( f2q3bqwtam + transPt [ 1 ] ) * localP -> dd10qqbw3x . Gain1_Gain ) + localP
-> dd10qqbw3x . Bias_Bias ; if ( kgyqh2swi5 > localP -> dd10qqbw3x .
Saturation2_UpperSat ) { kgyqh2swi5 = localP -> dd10qqbw3x .
Saturation2_UpperSat ; } else if ( kgyqh2swi5 < localP -> dd10qqbw3x .
Saturation2_LowerSat ) { kgyqh2swi5 = localP -> dd10qqbw3x .
Saturation2_LowerSat ; } djvdsf2p5q = muDoubleScalarRound ( ( dvv1hyxjz0 +
transPt [ 0 ] ) * localP -> dd10qqbw3x . Gain_Gain ) + localP -> dd10qqbw3x .
Bias1_Bias ; if ( djvdsf2p5q > localP -> dd10qqbw3x . Saturation3_UpperSat )
{ djvdsf2p5q = localP -> dd10qqbw3x . Saturation3_UpperSat ; } else if (
djvdsf2p5q < localP -> dd10qqbw3x . Saturation3_LowerSat ) { djvdsf2p5q =
localP -> dd10qqbw3x . Saturation3_LowerSat ; } tmp = muDoubleScalarFloor (
kgyqh2swi5 ) ; if ( ( localP -> dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput != 0 ) && ( ( ! ( tmp >= 0.0
) ) || ( ! ( tmp <= 7837.0 ) ) ) ) { if ( localP -> dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput == 1 ) { if ( localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r == 0U ) { diag =
CreateDiagnosticAsVoidPtr (
"Simulink:blocks:DirectLookupNdBlockIndexOutOfRangeWarnMsg" , 1 , 5 ,
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) ; rt_ssReportDiagnosticAsWarning ( rtS_m , diag ) ; } if ( localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r < MAX_uint32_T ) { localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r ++ ; } } else { diag =
CreateDiagnosticAsVoidPtr (
"Simulink:blocks:DirectLookupNdBlockIndexOutOfRangeErrorMsg" , 1 , 5 ,
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) ; rt_ssSet_slErrMsg ( rtS_m , diag ) ; } } tmp = muDoubleScalarFloor (
djvdsf2p5q ) ; if ( ( localP -> dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput != 0 ) && ( ( ! ( tmp >= 0.0
) ) || ( ! ( tmp <= 8164.0 ) ) ) ) { if ( localP -> dd10qqbw3x .
DirectLookupTablenD_DiagnosticForOutOfRangeInput == 1 ) { if ( localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r == 0U ) { diag =
CreateDiagnosticAsVoidPtr (
"Simulink:blocks:DirectLookupNdBlockIndexOutOfRangeWarnMsg" , 1 , 5 ,
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) ; rt_ssReportDiagnosticAsWarning ( rtS_m , diag ) ; } if ( localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r < MAX_uint32_T ) { localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r ++ ; } } else { diag =
CreateDiagnosticAsVoidPtr (
"Simulink:blocks:DirectLookupNdBlockIndexOutOfRangeErrorMsg" , 1 , 5 ,
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
) ; rt_ssSet_slErrMsg ( rtS_m , diag ) ; } } if ( muDoubleScalarIsNaN (
djvdsf2p5q ) ) { djvdsf2p5q = 0.0 ; } if ( muDoubleScalarIsNaN ( kgyqh2swi5 )
) { kgyqh2swi5 = 0.0 ; } if ( djvdsf2p5q > 8164.0 ) { djvdsf2p5q = 8164.0 ; }
else if ( djvdsf2p5q < 0.0 ) { djvdsf2p5q = 0.0 ; } if ( kgyqh2swi5 > 7837.0
) { kgyqh2swi5 = 7837.0 ; } else if ( kgyqh2swi5 < 0.0 ) { kgyqh2swi5 = 0.0 ;
} jjqq4ay2qq [ l2ubquqqj2 ] = localP -> dd10qqbw3x .
DirectLookupTablenD_table [ ( int32_T ) djvdsf2p5q * 7838 + ( int32_T )
kgyqh2swi5 ] ; } } void dfbtnmeqyz ( int32_T NumIters , SimStruct * rtS_g ,
bk4xk0dq22 localDW [ 1 ] ) { int32_T l2ubquqqj2 ; void * diag ; for (
l2ubquqqj2 = 0 ; l2ubquqqj2 < NumIters ; l2ubquqqj2 ++ ) { if ( localDW [
l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r != 0U ) { diag =
CreateDiagnosticAsVoidPtr (
"Simulink:blocks:DirectLookupNdBlockNumOfIndexOutOfRangeWarnMsg" , 2 , 5 ,
 "sim_Start/Left Line Sensor/Look up Line Value/For Each Subsystem/Look up Line Value/Direct Lookup Table (n-D)"
, 1 , localDW [ l2ubquqqj2 ] . dd10qqbw3x . b43cz3on2r ) ;
rt_ssReportDiagnosticAsWarning ( rtS_g , diag ) ; } } } static void
inbf3ttenx ( hv2uegvrr5 * * pEmxArray , int32_T numDimensions ) { hv2uegvrr5
* emxArray ; int32_T i ; * pEmxArray = ( hv2uegvrr5 * ) malloc ( sizeof (
hv2uegvrr5 ) ) ; emxArray = * pEmxArray ; emxArray -> data = ( char_T * )
NULL ; emxArray -> numDimensions = numDimensions ; emxArray -> size = (
int32_T * ) malloc ( sizeof ( int32_T ) * numDimensions ) ; emxArray ->
allocatedSize = 0 ; emxArray -> canFreeData = true ; for ( i = 0 ; i <
numDimensions ; i ++ ) { emxArray -> size [ i ] = 0 ; } } static void
dkx1govs0g ( void ) { if ( rtB . br5qezvsfu && rtB . faxpoeey0v ) { rtDW .
co2n0fejda = ncvh5u3vyd ; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ;
rtB . mtdmnunuoi = 0.0 ; rtB . mo2yss1m1k = 0.0 ; } } static void m1k31lb0rc
( void ) { if ( rtB . br5qezvsfu && rtB . faxpoeey0v ) { rtDW . co2n0fejda =
bt3at1vcd3 ; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ; rtB .
mtdmnunuoi = 0.0 ; rtB . mo2yss1m1k = 0.0 ; } } static void hip4glo4zv ( void
) { if ( rtB . dzwy32uw0n == 4.0 ) { rtB . mjogabnmjg = 1.0 ; rtB .
jjkwpfenp4 = 0.0 ; rtB . mo2yss1m1k = 0.0 ; rtDW . co2n0fejda = akz14vksph ;
rtDW . njbocqae2t = 0U ; } else if ( rtB . dzwy32uw0n == 1.0 ) { rtB .
mjogabnmjg = 3.0 ; rtB . jjkwpfenp4 = 0.0 ; rtB . mo2yss1m1k = 0.0 ; rtDW .
co2n0fejda = nrqrzdpjwp ; rtDW . njbocqae2t = 0U ; } else if ( rtB .
dzwy32uw0n == 8.0 ) { rtB . mjogabnmjg = 1.0 ; rtB . jjkwpfenp4 = 0.0 ; rtB .
mo2yss1m1k = 0.0 ; rtDW . co2n0fejda = gis0nzutnc ; rtDW . njbocqae2t = 0U ;
} else if ( rtB . dzwy32uw0n == 7.0 ) { rtB . mjogabnmjg = 2.0 ; rtB .
jjkwpfenp4 = 0.0 ; rtB . mo2yss1m1k = 0.0 ; rtDW . co2n0fejda = dpv45dh4v0 ;
rtDW . njbocqae2t = 0U ; } else if ( rtB . dzwy32uw0n == 2.0 ) { rtDW .
co2n0fejda = nuhjvypsnz ; rtB . mo2yss1m1k = 0.0 ; rtB . jjkwpfenp4 = 0.0 ;
rtB . mjogabnmjg = 5.0 ; rtB . ls0wcsfeyn = 1.0 ; } else if ( rtB .
dzwy32uw0n == 3.0 ) { rtB . mjogabnmjg = 0.0 ; rtB . jjkwpfenp4 = 0.0 ; rtB .
mo2yss1m1k = 0.0 ; rtDW . co2n0fejda = ep4d23adrm ; rtDW . njbocqae2t = 0U ;
} else if ( rtB . dzwy32uw0n == 6.0 ) { rtB . mjogabnmjg = 1.0 ; rtB .
jjkwpfenp4 = 0.0 ; rtB . mo2yss1m1k = 0.0 ; rtDW . co2n0fejda = msiuhxxah5 ;
rtDW . njbocqae2t = 0U ; } else if ( rtB . dzwy32uw0n == 5.0 ) { rtB .
mjogabnmjg = 1.0 ; rtB . jjkwpfenp4 = 0.0 ; rtB . mo2yss1m1k = 0.0 ; rtDW .
co2n0fejda = atdrzrw5eu ; rtDW . njbocqae2t = 0U ; } else { rtB . mjogabnmjg
= 5.0 ; rtB . jjkwpfenp4 = 0.03 ; rtB . mtdmnunuoi = 0.0 ; rtB . i5w0dsemue =
0.0 ; } } static void fzjvjaawpn ( hv2uegvrr5 * emxArray , int32_T oldNumel )
{ int32_T i ; int32_T newNumel ; void * newData ; if ( oldNumel < 0 ) {
oldNumel = 0 ; } newNumel = 1 ; for ( i = 0 ; i < emxArray -> numDimensions ;
i ++ ) { newNumel *= emxArray -> size [ i ] ; } if ( newNumel > emxArray ->
allocatedSize ) { i = emxArray -> allocatedSize ; if ( i < 16 ) { i = 16 ; }
while ( i < newNumel ) { if ( i > 1073741823 ) { i = MAX_int32_T ; } else { i
<<= 1 ; } } newData = calloc ( ( uint32_T ) i , sizeof ( char_T ) ) ; if (
emxArray -> data != NULL ) { memcpy ( newData , emxArray -> data , sizeof (
char_T ) * oldNumel ) ; if ( emxArray -> canFreeData ) { free ( emxArray ->
data ) ; } } emxArray -> data = ( char_T * ) newData ; emxArray ->
allocatedSize = i ; emxArray -> canFreeData = true ; } } static void
j42rpwppwg ( hv2uegvrr5 * * pEmxArray ) { if ( * pEmxArray != ( hv2uegvrr5 *
) NULL ) { if ( ( ( * pEmxArray ) -> data != ( char_T * ) NULL ) && ( *
pEmxArray ) -> canFreeData ) { free ( ( * pEmxArray ) -> data ) ; } free ( (
* pEmxArray ) -> size ) ; free ( * pEmxArray ) ; * pEmxArray = ( hv2uegvrr5 *
) NULL ; } } static void fc04r2nndv ( void ) { if ( ( ! rtB . krhfkykqos ) &&
rtB . amaivwoi3x ) { rtDW . mqobma15yv = ikr1dxkfwn ; rtDW . j04nfygwcf = 0U
; rtB . hldfmfuah3 = 0.0 ; } else if ( rtB . br5qezvsfu && ( ! rtB .
faxpoeey0v ) && ( rtB . ezqnv40hor == 0.0 ) ) { rtDW . mqobma15yv =
ihinm10c3b ; rtB . la1brdabwh = - 0.6 ; } else if ( rtB . krhfkykqos && ( !
rtB . amaivwoi3x ) ) { rtDW . mqobma15yv = m3hjzoz043 ; rtDW . j04nfygwcf =
0U ; rtB . hldfmfuah3 = 0.0 ; } else if ( ( ! rtB . br5qezvsfu ) && ( ! rtB .
faxpoeey0v ) && ( ! rtB . krhfkykqos ) && ( ! rtB . amaivwoi3x ) && ( rtB .
ezqnv40hor == 0.0 ) ) { rtDW . mqobma15yv = d0pes32jmr ; rtDW . j04nfygwcf =
0U ; rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 1.0 ; } else if ( ( ! rtB .
br5qezvsfu ) && rtB . faxpoeey0v && ( rtB . ezqnv40hor == 0.0 ) ) { rtDW .
mqobma15yv = ewdqahmcux ; rtB . la1brdabwh = 0.6 ; } else if ( rtB .
br5qezvsfu && rtB . faxpoeey0v && rtB . krhfkykqos && rtB . amaivwoi3x ) {
rtDW . mqobma15yv = cqttlgnk1c ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 =
0.0 ; } else { rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 0.0 ; } } static
void nghjzs0ayn ( const hv2uegvrr5 * varargin_1_Value , hv2uegvrr5 * y_Value
) { hv2uegvrr5 * c ; hv2uegvrr5 * yc ; int32_T b_copyfrom ; int32_T b_in_idx
; int32_T b_patt_idx ; int32_T b_tmp_in_idx ; int32_T copyfrom ; int32_T
in_idx ; int32_T out_idx ; int32_T out_len ; int32_T str_len ; int32_T tmp ;
void * b_y ; static const char_T f [ 3 ] = { 'L' , 'B' , 'S' } ; inbf3ttenx (
& yc , 2 ) ; inbf3ttenx ( & c , 2 ) ; tmp = suStringStackSize ( ) ; str_len =
varargin_1_Value -> size [ 1 ] ; if ( varargin_1_Value -> size [ 1 ] == 0 ) {
yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = 0 ; } else { in_idx = 0 ; copyfrom
= 1 ; out_idx = 0 ; b_in_idx = 0 ; b_copyfrom = 1 ; out_len = 0 ; while (
b_in_idx < str_len ) { b_in_idx ++ ; b_patt_idx = 1 ; b_tmp_in_idx = b_in_idx
; while ( ( b_patt_idx <= 3 ) && ( b_tmp_in_idx <= str_len ) && (
varargin_1_Value -> data [ b_tmp_in_idx - 1 ] == f [ b_patt_idx - 1 ] ) ) {
b_tmp_in_idx ++ ; b_patt_idx ++ ; } if ( b_patt_idx > 3 ) { out_len ++ ;
b_copyfrom = b_tmp_in_idx ; } else if ( b_in_idx >= b_copyfrom ) { out_len ++
; } } b_in_idx = yc -> size [ 0 ] * yc -> size [ 1 ] ; yc -> size [ 0 ] = 1 ;
yc -> size [ 1 ] = out_len ; fzjvjaawpn ( yc , b_in_idx ) ; while ( in_idx <
str_len ) { in_idx ++ ; b_in_idx = 1 ; b_copyfrom = in_idx ; while ( (
b_in_idx <= 3 ) && ( b_copyfrom <= str_len ) && ( varargin_1_Value -> data [
b_copyfrom - 1 ] == f [ b_in_idx - 1 ] ) ) { b_copyfrom ++ ; b_in_idx ++ ; }
if ( b_in_idx > 3 ) { yc -> data [ out_idx ] = 'R' ; out_idx ++ ; copyfrom =
b_copyfrom ; } else if ( in_idx >= copyfrom ) { yc -> data [ out_idx ] =
varargin_1_Value -> data [ in_idx - 1 ] ; out_idx ++ ; } } } b_y =
suAddStackString ( "" ) ; b_in_idx = yc -> size [ 1 ] ; suAsciiToStr ( b_y ,
( uint8_T * ) & yc -> data [ 0 ] , b_in_idx ) ; str_len = suStrlen ( b_y ) ;
b_in_idx = c -> size [ 0 ] * c -> size [ 1 ] ; c -> size [ 0 ] = 1 ; c ->
size [ 1 ] = str_len ; fzjvjaawpn ( c , b_in_idx ) ; suStrToAscii ( ( uint8_T
* ) & c -> data [ 0 ] , str_len , b_y ) ; b_in_idx = y_Value -> size [ 0 ] *
y_Value -> size [ 1 ] ; y_Value -> size [ 0 ] = 1 ; y_Value -> size [ 1 ] = c
-> size [ 1 ] ; fzjvjaawpn ( y_Value , b_in_idx ) ; in_idx = c -> size [ 1 ]
- 1 ; for ( str_len = 0 ; str_len <= in_idx ; str_len ++ ) { y_Value -> data
[ str_len ] = c -> data [ str_len ] ; } suSetStringStackSize ( tmp ) ;
j42rpwppwg ( & c ) ; j42rpwppwg ( & yc ) ; } static void nghjzs0ayn1 ( const
hv2uegvrr5 * varargin_1_Value , hv2uegvrr5 * y_Value ) { hv2uegvrr5 * c ;
hv2uegvrr5 * yc ; int32_T b_copyfrom ; int32_T b_in_idx ; int32_T b_patt_idx
; int32_T b_tmp_in_idx ; int32_T copyfrom ; int32_T in_idx ; int32_T out_idx
; int32_T out_len ; int32_T str_len ; int32_T tmp ; void * b_y ; static const
char_T f [ 3 ] = { 'R' , 'B' , 'L' } ; inbf3ttenx ( & yc , 2 ) ; inbf3ttenx (
& c , 2 ) ; tmp = suStringStackSize ( ) ; str_len = varargin_1_Value -> size
[ 1 ] ; if ( varargin_1_Value -> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ;
yc -> size [ 1 ] = 0 ; } else { in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ;
b_in_idx = 0 ; b_copyfrom = 1 ; out_len = 0 ; while ( b_in_idx < str_len ) {
b_in_idx ++ ; b_patt_idx = 1 ; b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx
<= 3 ) && ( b_tmp_in_idx <= str_len ) && ( varargin_1_Value -> data [
b_tmp_in_idx - 1 ] == f [ b_patt_idx - 1 ] ) ) { b_tmp_in_idx ++ ; b_patt_idx
++ ; } if ( b_patt_idx > 3 ) { out_len ++ ; b_copyfrom = b_tmp_in_idx ; }
else if ( b_in_idx >= b_copyfrom ) { out_len ++ ; } } b_in_idx = yc -> size [
0 ] * yc -> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = out_len ;
fzjvjaawpn ( yc , b_in_idx ) ; while ( in_idx < str_len ) { in_idx ++ ;
b_in_idx = 1 ; b_copyfrom = in_idx ; while ( ( b_in_idx <= 3 ) && (
b_copyfrom <= str_len ) && ( varargin_1_Value -> data [ b_copyfrom - 1 ] == f
[ b_in_idx - 1 ] ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if ( b_in_idx > 3 ) {
yc -> data [ out_idx ] = 'B' ; out_idx ++ ; copyfrom = b_copyfrom ; } else if
( in_idx >= copyfrom ) { yc -> data [ out_idx ] = varargin_1_Value -> data [
in_idx - 1 ] ; out_idx ++ ; } } } b_y = suAddStackString ( "" ) ; b_in_idx =
yc -> size [ 1 ] ; suAsciiToStr ( b_y , ( uint8_T * ) & yc -> data [ 0 ] ,
b_in_idx ) ; str_len = suStrlen ( b_y ) ; b_in_idx = c -> size [ 0 ] * c ->
size [ 1 ] ; c -> size [ 0 ] = 1 ; c -> size [ 1 ] = str_len ; fzjvjaawpn ( c
, b_in_idx ) ; suStrToAscii ( ( uint8_T * ) & c -> data [ 0 ] , str_len , b_y
) ; b_in_idx = y_Value -> size [ 0 ] * y_Value -> size [ 1 ] ; y_Value ->
size [ 0 ] = 1 ; y_Value -> size [ 1 ] = c -> size [ 1 ] ; fzjvjaawpn (
y_Value , b_in_idx ) ; in_idx = c -> size [ 1 ] - 1 ; for ( str_len = 0 ;
str_len <= in_idx ; str_len ++ ) { y_Value -> data [ str_len ] = c -> data [
str_len ] ; } suSetStringStackSize ( tmp ) ; j42rpwppwg ( & c ) ; j42rpwppwg
( & yc ) ; } static void nghjzs0ayn1g ( const hv2uegvrr5 * varargin_1_Value ,
hv2uegvrr5 * y_Value ) { hv2uegvrr5 * c ; hv2uegvrr5 * yc ; int32_T
b_copyfrom ; int32_T b_in_idx ; int32_T b_patt_idx ; int32_T b_tmp_in_idx ;
int32_T copyfrom ; int32_T in_idx ; int32_T out_idx ; int32_T out_len ;
int32_T str_len ; int32_T tmp ; void * b_y ; static const char_T f [ 3 ] = {
'S' , 'B' , 'L' } ; inbf3ttenx ( & yc , 2 ) ; inbf3ttenx ( & c , 2 ) ; tmp =
suStringStackSize ( ) ; str_len = varargin_1_Value -> size [ 1 ] ; if (
varargin_1_Value -> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ; yc -> size [ 1
] = 0 ; } else { in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ; b_in_idx = 0 ;
b_copyfrom = 1 ; out_len = 0 ; while ( b_in_idx < str_len ) { b_in_idx ++ ;
b_patt_idx = 1 ; b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx <= 3 ) && (
b_tmp_in_idx <= str_len ) && ( varargin_1_Value -> data [ b_tmp_in_idx - 1 ]
== f [ b_patt_idx - 1 ] ) ) { b_tmp_in_idx ++ ; b_patt_idx ++ ; } if (
b_patt_idx > 3 ) { out_len ++ ; b_copyfrom = b_tmp_in_idx ; } else if (
b_in_idx >= b_copyfrom ) { out_len ++ ; } } b_in_idx = yc -> size [ 0 ] * yc
-> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = out_len ;
fzjvjaawpn ( yc , b_in_idx ) ; while ( in_idx < str_len ) { in_idx ++ ;
b_in_idx = 1 ; b_copyfrom = in_idx ; while ( ( b_in_idx <= 3 ) && (
b_copyfrom <= str_len ) && ( varargin_1_Value -> data [ b_copyfrom - 1 ] == f
[ b_in_idx - 1 ] ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if ( b_in_idx > 3 ) {
yc -> data [ out_idx ] = 'R' ; out_idx ++ ; copyfrom = b_copyfrom ; } else if
( in_idx >= copyfrom ) { yc -> data [ out_idx ] = varargin_1_Value -> data [
in_idx - 1 ] ; out_idx ++ ; } } } b_y = suAddStackString ( "" ) ; b_in_idx =
yc -> size [ 1 ] ; suAsciiToStr ( b_y , ( uint8_T * ) & yc -> data [ 0 ] ,
b_in_idx ) ; str_len = suStrlen ( b_y ) ; b_in_idx = c -> size [ 0 ] * c ->
size [ 1 ] ; c -> size [ 0 ] = 1 ; c -> size [ 1 ] = str_len ; fzjvjaawpn ( c
, b_in_idx ) ; suStrToAscii ( ( uint8_T * ) & c -> data [ 0 ] , str_len , b_y
) ; b_in_idx = y_Value -> size [ 0 ] * y_Value -> size [ 1 ] ; y_Value ->
size [ 0 ] = 1 ; y_Value -> size [ 1 ] = c -> size [ 1 ] ; fzjvjaawpn (
y_Value , b_in_idx ) ; in_idx = c -> size [ 1 ] - 1 ; for ( str_len = 0 ;
str_len <= in_idx ; str_len ++ ) { y_Value -> data [ str_len ] = c -> data [
str_len ] ; } suSetStringStackSize ( tmp ) ; j42rpwppwg ( & c ) ; j42rpwppwg
( & yc ) ; } static void nghjzs0ayn1gl ( const hv2uegvrr5 * varargin_1_Value
, hv2uegvrr5 * y_Value ) { hv2uegvrr5 * c ; hv2uegvrr5 * yc ; int32_T
b_copyfrom ; int32_T b_in_idx ; int32_T b_patt_idx ; int32_T b_tmp_in_idx ;
int32_T copyfrom ; int32_T in_idx ; int32_T out_idx ; int32_T out_len ;
int32_T str_len ; int32_T tmp ; void * b_y ; static const char_T f [ 3 ] = {
'S' , 'B' , 'S' } ; inbf3ttenx ( & yc , 2 ) ; inbf3ttenx ( & c , 2 ) ; tmp =
suStringStackSize ( ) ; str_len = varargin_1_Value -> size [ 1 ] ; if (
varargin_1_Value -> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ; yc -> size [ 1
] = 0 ; } else { in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ; b_in_idx = 0 ;
b_copyfrom = 1 ; out_len = 0 ; while ( b_in_idx < str_len ) { b_in_idx ++ ;
b_patt_idx = 1 ; b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx <= 3 ) && (
b_tmp_in_idx <= str_len ) && ( varargin_1_Value -> data [ b_tmp_in_idx - 1 ]
== f [ b_patt_idx - 1 ] ) ) { b_tmp_in_idx ++ ; b_patt_idx ++ ; } if (
b_patt_idx > 3 ) { out_len ++ ; b_copyfrom = b_tmp_in_idx ; } else if (
b_in_idx >= b_copyfrom ) { out_len ++ ; } } b_in_idx = yc -> size [ 0 ] * yc
-> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = out_len ;
fzjvjaawpn ( yc , b_in_idx ) ; while ( in_idx < str_len ) { in_idx ++ ;
b_in_idx = 1 ; b_copyfrom = in_idx ; while ( ( b_in_idx <= 3 ) && (
b_copyfrom <= str_len ) && ( varargin_1_Value -> data [ b_copyfrom - 1 ] == f
[ b_in_idx - 1 ] ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if ( b_in_idx > 3 ) {
yc -> data [ out_idx ] = 'B' ; out_idx ++ ; copyfrom = b_copyfrom ; } else if
( in_idx >= copyfrom ) { yc -> data [ out_idx ] = varargin_1_Value -> data [
in_idx - 1 ] ; out_idx ++ ; } } } b_y = suAddStackString ( "" ) ; b_in_idx =
yc -> size [ 1 ] ; suAsciiToStr ( b_y , ( uint8_T * ) & yc -> data [ 0 ] ,
b_in_idx ) ; str_len = suStrlen ( b_y ) ; b_in_idx = c -> size [ 0 ] * c ->
size [ 1 ] ; c -> size [ 0 ] = 1 ; c -> size [ 1 ] = str_len ; fzjvjaawpn ( c
, b_in_idx ) ; suStrToAscii ( ( uint8_T * ) & c -> data [ 0 ] , str_len , b_y
) ; b_in_idx = y_Value -> size [ 0 ] * y_Value -> size [ 1 ] ; y_Value ->
size [ 0 ] = 1 ; y_Value -> size [ 1 ] = c -> size [ 1 ] ; fzjvjaawpn (
y_Value , b_in_idx ) ; in_idx = c -> size [ 1 ] - 1 ; for ( str_len = 0 ;
str_len <= in_idx ; str_len ++ ) { y_Value -> data [ str_len ] = c -> data [
str_len ] ; } suSetStringStackSize ( tmp ) ; j42rpwppwg ( & c ) ; j42rpwppwg
( & yc ) ; } static void nghjzs0ayn1glc ( const hv2uegvrr5 * varargin_1_Value
, hv2uegvrr5 * y_Value ) { hv2uegvrr5 * c ; hv2uegvrr5 * yc ; int32_T
b_copyfrom ; int32_T b_in_idx ; int32_T b_patt_idx ; int32_T b_tmp_in_idx ;
int32_T copyfrom ; int32_T in_idx ; int32_T out_idx ; int32_T out_len ;
int32_T str_len ; int32_T tmp ; void * b_y ; static const char_T f [ 3 ] = {
'L' , 'B' , 'L' } ; inbf3ttenx ( & yc , 2 ) ; inbf3ttenx ( & c , 2 ) ; tmp =
suStringStackSize ( ) ; str_len = varargin_1_Value -> size [ 1 ] ; if (
varargin_1_Value -> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ; yc -> size [ 1
] = 0 ; } else { in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ; b_in_idx = 0 ;
b_copyfrom = 1 ; out_len = 0 ; while ( b_in_idx < str_len ) { b_in_idx ++ ;
b_patt_idx = 1 ; b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx <= 3 ) && (
b_tmp_in_idx <= str_len ) && ( varargin_1_Value -> data [ b_tmp_in_idx - 1 ]
== f [ b_patt_idx - 1 ] ) ) { b_tmp_in_idx ++ ; b_patt_idx ++ ; } if (
b_patt_idx > 3 ) { out_len ++ ; b_copyfrom = b_tmp_in_idx ; } else if (
b_in_idx >= b_copyfrom ) { out_len ++ ; } } b_in_idx = yc -> size [ 0 ] * yc
-> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = out_len ;
fzjvjaawpn ( yc , b_in_idx ) ; while ( in_idx < str_len ) { in_idx ++ ;
b_in_idx = 1 ; b_copyfrom = in_idx ; while ( ( b_in_idx <= 3 ) && (
b_copyfrom <= str_len ) && ( varargin_1_Value -> data [ b_copyfrom - 1 ] == f
[ b_in_idx - 1 ] ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if ( b_in_idx > 3 ) {
yc -> data [ out_idx ] = 'S' ; out_idx ++ ; copyfrom = b_copyfrom ; } else if
( in_idx >= copyfrom ) { yc -> data [ out_idx ] = varargin_1_Value -> data [
in_idx - 1 ] ; out_idx ++ ; } } } b_y = suAddStackString ( "" ) ; b_in_idx =
yc -> size [ 1 ] ; suAsciiToStr ( b_y , ( uint8_T * ) & yc -> data [ 0 ] ,
b_in_idx ) ; str_len = suStrlen ( b_y ) ; b_in_idx = c -> size [ 0 ] * c ->
size [ 1 ] ; c -> size [ 0 ] = 1 ; c -> size [ 1 ] = str_len ; fzjvjaawpn ( c
, b_in_idx ) ; suStrToAscii ( ( uint8_T * ) & c -> data [ 0 ] , str_len , b_y
) ; b_in_idx = y_Value -> size [ 0 ] * y_Value -> size [ 1 ] ; y_Value ->
size [ 0 ] = 1 ; y_Value -> size [ 1 ] = c -> size [ 1 ] ; fzjvjaawpn (
y_Value , b_in_idx ) ; in_idx = c -> size [ 1 ] - 1 ; for ( str_len = 0 ;
str_len <= in_idx ; str_len ++ ) { y_Value -> data [ str_len ] = c -> data [
str_len ] ; } suSetStringStackSize ( tmp ) ; j42rpwppwg ( & c ) ; j42rpwppwg
( & yc ) ; } void MdlInitialize ( void ) { int32_T i ; suCopyString ( rtDW .
icjcuyoesf , rtP . UnitDelay_InitialCondition ) ; rtDW . pd03hpujxp = rtP .
UnitDelay1_InitialCondition ; rtDW . lruetkegpd = rtP .
UnitDelay1_InitialCondition_jmsdimwcgj ; rtDW . jlsempkvov = rtP .
RobotSimulator_startX ; rtDW . flos1pd3x0 = rtP . RobotSimulator_startY ;
rtDW . ileqgfdrx1 = rtP . RobotSimulator_startTheta * 0.017453292519943295 ;
for ( i = 0 ; i < 1500 ; i ++ ) { rtDW . caiyox4oj2 [ i ] = rtP .
Delay_InitialCondition ; } rtDW . iafj1kgffe = rtP .
DiscreteTimeIntegrator_IC ; rtDW . gyqsentnna [ 0 ] = 0.0 ; rtDW . gyqsentnna
[ 1 ] = 0.0 ; rtDW . gyqsentnna [ 2 ] = 0.0 ; rtDW . j2fbjqvjcp = rtP .
DiscreteTimeIntegrator_IC_e3x2ks5ohu ; rtDW . psgym2jyk0 [ 0 ] = 0.0 ; rtDW .
psgym2jyk0 [ 1 ] = 0.0 ; rtDW . psgym2jyk0 [ 2 ] = 0.0 ; rtDW . p2qqabsh1z =
rtP . UnitDelay2_InitialCondition ; rtDW . fen0hw5zdb = hps0ylwak0 ; rtDW .
njbocqae2t = 0U ; rtDW . c1g1b2pcb5 = 0U ; rtDW . co2n0fejda = 0U ; rtB .
jjkwpfenp4 = 0.0 ; rtB . mo2yss1m1k = 0.0 ; rtB . i5w0dsemue = 0.0 ; rtB .
mtdmnunuoi = 0.0 ; rtB . mjogabnmjg = 0.0 ; rtB . ls0wcsfeyn = 0.0 ; rtDW .
grgj1nbj2f = hps0ylwak0 ; rtDW . bgn14fgjm3 = 0U ; rtDW . kfkq4jq4py = 0U ;
rtDW . m1jml1zdzn = 0U ; rtDW . a42pr4cd24 = 0.0 ; rtB . dxmhlj5cu2 = 0.0 ;
rtB . jl4a230o5q = 0.0 ; rtB . pnwjxb31po = 0.0 ; rtB . oqf5mpl1fz = 0.0 ;
rtDW . gbtehizmoh = hps0ylwak0 ; rtDW . j04nfygwcf = 0U ; rtDW . nyhsr4t3fp =
0U ; rtDW . mqobma15yv = 0U ; rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 0.0
; rtB . la1brdabwh = 0.0 ; } void MdlStart ( void ) { { bool
externalInputIsInDatasetFormat = false ; void * pISigstreamManager =
rt_GetISigstreamManager ( rtS ) ;
rtwISigstreamManagerGetInputIsInDatasetFormat ( pISigstreamManager , &
externalInputIsInDatasetFormat ) ; if ( externalInputIsInDatasetFormat ) { }
} eg2eg1jqqe ( 1 , rtDW . eo3swzvasqb ) ; eg2eg1jqqe ( 1 , rtDW . cor5gsbd40
) ; eg2eg1jqqe ( 1 , rtDW . btjpsv3ywi ) ; eg2eg1jqqe ( 1 , rtDW . jyzsgxunlp
) ; { SimStruct * rts = ssGetSFunction ( rtS , 0 ) ; { static const char *
blockSIDForSFcnLoader = "sim_Start:4:401" ; sfcnLoader_setCurrentSFcnBlockSID
( blockSIDForSFcnLoader ) ; void ( * sfcnMethodPtr ) ( SimStruct * ) =
ssGetmdlStart ( rts ) ; sfcnLoader_separateComplexHandler ( rts ,
sfcnMethodPtr ) ; } if ( ssGetErrorStatus ( rts ) != ( NULL ) ) return ; } {
FWksInfo * fromwksInfo ; if ( ( fromwksInfo = ( FWksInfo * ) calloc ( 1 ,
sizeof ( FWksInfo ) ) ) == ( NULL ) ) { ssSetErrorStatus ( rtS ,
"from workspace STRING(Name) memory allocation error" ) ; } else {
fromwksInfo -> origWorkspaceVarName = "out.maze_state" ; fromwksInfo ->
origDataTypeId = 0 ; fromwksInfo -> origIsComplex = 0 ; fromwksInfo ->
origWidth = 1 ; fromwksInfo -> origElSize = sizeof ( real_T ) ; fromwksInfo
-> data = ( void * ) & rtP . FromWorkspace2_Data0 ; fromwksInfo ->
nDataPoints = 1 ; fromwksInfo -> time = ( double * ) & rtP .
FromWorkspace2_Time0 ; rtDW . k3xrmmg34t . TimePtr = fromwksInfo -> time ;
rtDW . k3xrmmg34t . DataPtr = fromwksInfo -> data ; rtDW . k3xrmmg34t .
RSimInfoPtr = fromwksInfo ; } rtDW . fsmjxejob3 . PrevIndex = 0 ; } {
FWksInfo * fromwksInfo ; if ( ( fromwksInfo = ( FWksInfo * ) calloc ( 1 ,
sizeof ( FWksInfo ) ) ) == ( NULL ) ) { ssSetErrorStatus ( rtS ,
"from workspace STRING(Name) memory allocation error" ) ; } else {
fromwksInfo -> origWorkspaceVarName = "out.path_taken" ; fromwksInfo ->
origDataTypeId = 3 ; fromwksInfo -> origIsComplex = 0 ; fromwksInfo ->
origWidth = 31 ; fromwksInfo -> origElSize = sizeof ( uint8_T ) ; fromwksInfo
-> data = ( void * ) rtP . FromWorkspace3_Data0 ; fromwksInfo -> nDataPoints
= 81 ; fromwksInfo -> time = ( double * ) rtP . FromWorkspace3_Time0 ; rtDW .
pg5gatyniv . TimePtr = fromwksInfo -> time ; rtDW . pg5gatyniv . DataPtr =
fromwksInfo -> data ; rtDW . pg5gatyniv . RSimInfoPtr = fromwksInfo ; } rtDW
. bn4k2qv04s . PrevIndex = 0 ; } { FWksInfo * fromwksInfo ; if ( (
fromwksInfo = ( FWksInfo * ) calloc ( 1 , sizeof ( FWksInfo ) ) ) == ( NULL )
) { ssSetErrorStatus ( rtS ,
"from workspace STRING(Name) memory allocation error" ) ; } else {
fromwksInfo -> origWorkspaceVarName = "out.count" ; fromwksInfo ->
origDataTypeId = 0 ; fromwksInfo -> origIsComplex = 0 ; fromwksInfo ->
origWidth = 1 ; fromwksInfo -> origElSize = sizeof ( real_T ) ; fromwksInfo
-> data = ( void * ) & rtP . FromWorkspace1_Data0 ; fromwksInfo ->
nDataPoints = 1 ; fromwksInfo -> time = ( double * ) & rtP .
FromWorkspace1_Time0 ; rtDW . jayrspqml1 . TimePtr = fromwksInfo -> time ;
rtDW . jayrspqml1 . DataPtr = fromwksInfo -> data ; rtDW . jayrspqml1 .
RSimInfoPtr = fromwksInfo ; } rtDW . j244bobwtj . PrevIndex = 0 ; } {
FWksInfo * fromwksInfo ; if ( ( fromwksInfo = ( FWksInfo * ) calloc ( 1 ,
sizeof ( FWksInfo ) ) ) == ( NULL ) ) { ssSetErrorStatus ( rtS ,
"from workspace STRING(Name) memory allocation error" ) ; } else {
fromwksInfo -> origWorkspaceVarName = "out.maze_state" ; fromwksInfo ->
origDataTypeId = 0 ; fromwksInfo -> origIsComplex = 0 ; fromwksInfo ->
origWidth = 1 ; fromwksInfo -> origElSize = sizeof ( real_T ) ; fromwksInfo
-> data = ( void * ) & rtP . FromWorkspace_Data0 ; fromwksInfo -> nDataPoints
= 1 ; fromwksInfo -> time = ( double * ) & rtP . FromWorkspace_Time0 ; rtDW .
opv4zomgtb . TimePtr = fromwksInfo -> time ; rtDW . opv4zomgtb . DataPtr =
fromwksInfo -> data ; rtDW . opv4zomgtb . RSimInfoPtr = fromwksInfo ; } rtDW
. aatqqysjgr . PrevIndex = 0 ; } suCopyString ( rtDW . ex0ljdtjz3 , rtP .
DataStoreMemory_InitialValue ) ; MdlInitialize ( ) ; } void MdlOutputs (
int_T tid ) { real_T m1ultwsa1j ; real_T iaqcyeykxh ; real_T k3ehhwacx5 ;
real32_T jxtqlafwss ; real32_T h5onfuqhtm ; real32_T hw4pn4nfst ; real32_T
kcxjzwo4sq ; hv2uegvrr5 * b_temp_Value ; hv2uegvrr5 * d ; hv2uegvrr5 * g ;
hv2uegvrr5 * i ; hv2uegvrr5 * obj1_Value ; hv2uegvrr5 * val ; hv2uegvrr5 * yc
; real_T aok40h4fye ; real_T deu1hqt5kj ; real_T l012ujkai0 ; int32_T
b_copyfrom ; int32_T b_in_idx ; int32_T b_patt_idx ; int32_T b_tmp_in_idx ;
int32_T copyfrom ; int32_T counter ; int32_T in_idx ; int32_T out_idx ;
int32_T out_len ; int32_T s ; int32_T str_len ; int32_T tmp_p ; uint32_T len
; uint8_T tmp [ 31 ] ; boolean_T equal ; SimStruct * S ; void * b_y ; void *
c_y ; void * d_y ; void * diag ; void * kt0wt4f5ht ; void * kxcjzg5yu4 ; void
* y ; void * y_p ; static const char_T k [ 3 ] = { 'L' , 'B' , 'R' } ;
hv2uegvrr5 * tmp_e ; hv2uegvrr5 * tmp_g ; hv2uegvrr5 * tmp_i ; hv2uegvrr5 *
tmp_m ; inbf3ttenx ( & obj1_Value , 2 ) ; inbf3ttenx ( & yc , 2 ) ;
inbf3ttenx ( & d , 2 ) ; inbf3ttenx ( & g , 2 ) ; inbf3ttenx ( & i , 2 ) ;
tmp_p = suStringStackSize ( ) ; kxcjzg5yu4 = suAddStackString ( NULL ) ;
suCopyString ( kxcjzg5yu4 , rtDW . icjcuyoesf ) ; l012ujkai0 = rtDW .
pd03hpujxp ; rtB . dzwy32uw0n = rtDW . lruetkegpd ; rtB . h012lh2sdm = rtDW .
jlsempkvov ; rtB . kawa0ssxug = rtDW . flos1pd3x0 ; aok40h4fye = rtDW .
ileqgfdrx1 ; rtB . hng4zofqfq = muDoubleScalarMod ( rtDW . ileqgfdrx1 , rtP .
Circle_Value ) ; eo3swzvasq ( 1 , rtS , rtB . h012lh2sdm , rtB . kawa0ssxug ,
rtB . hng4zofqfq , & rtP . Constant_Value_olk5gkppvb , & rtP .
Constant1_Value_oyulwwquga , & hw4pn4nfst , rtDW . cor5gsbd40 , & rtP .
cor5gsbd40 ) ; if ( hw4pn4nfst > rtP . Switch_Threshold_a2s0c4f3g1 ) { rtB .
faxpoeey0v = ! ( rtP . LeftMiddleLineSensor_envVal != 0.0 ) ; } else { rtB .
faxpoeey0v = ! ( rtP . LeftMiddleLineSensor_lineVal != 0.0 ) ; } eo3swzvasq (
1 , rtS , rtB . h012lh2sdm , rtB . kawa0ssxug , rtB . hng4zofqfq , & rtP .
Constant_Value_gf0zpo0joi , & rtP . Constant1_Value_ih0jkfo3kb , & jxtqlafwss
, rtDW . jyzsgxunlp , & rtP . jyzsgxunlp ) ; if ( jxtqlafwss > rtP .
Switch_Threshold_aplx2fzo4d ) { rtB . br5qezvsfu = ! ( rtP .
RightMiddleLineSensor_envVal != 0.0 ) ; } else { rtB . br5qezvsfu = ! ( rtP .
RightMiddleLineSensor_lineVal != 0.0 ) ; } if ( rtDW . njbocqae2t < 255U ) {
rtDW . njbocqae2t ++ ; } rtDW . fen0hw5zdb = hps0ylwak0 ; if ( rtDW .
c1g1b2pcb5 == 0U ) { rtDW . c1g1b2pcb5 = 1U ; rtDW . co2n0fejda = jwn2n42iuk
; rtDW . njbocqae2t = 0U ; rtB . mjogabnmjg = 5.0 ; rtB . mo2yss1m1k = 0.6 ;
} else { switch ( rtDW . co2n0fejda ) { case bt3at1vcd3 : if ( rtDW .
njbocqae2t >= 150U ) { rtDW . co2n0fejda = m0zspltx2s ; rtB . mjogabnmjg =
5.0 ; rtB . jjkwpfenp4 = 0.03 ; rtB . mtdmnunuoi = 0.0 ; rtB . i5w0dsemue =
0.0 ; } break ; case ncvh5u3vyd : if ( rtDW . njbocqae2t >= 25U ) { rtDW .
co2n0fejda = m0zspltx2s ; rtB . mjogabnmjg = 5.0 ; rtB . jjkwpfenp4 = 0.03 ;
rtB . mtdmnunuoi = 0.0 ; rtB . i5w0dsemue = 0.0 ; } break ; case kw41sjmrpq :
if ( rtDW . njbocqae2t >= 35U ) { rtDW . co2n0fejda = btej00r3bi ; rtB .
i5w0dsemue = rtB . dzwy32uw0n ; } break ; case d0pes32jmr : if ( rtDW .
njbocqae2t >= 23U ) { rtDW . co2n0fejda = jm04rqucdj ; rtB . i5w0dsemue = rtB
. dzwy32uw0n ; } break ; case akz14vksph : if ( rtDW . njbocqae2t >= 1U ) {
rtDW . co2n0fejda = maxri0m3or ; rtDW . njbocqae2t = 0U ; rtB . mjogabnmjg =
5.0 ; rtB . jjkwpfenp4 = 0.03 ; } break ; case gis0nzutnc : if ( rtDW .
njbocqae2t >= 1U ) { rtDW . co2n0fejda = nli3jwkuqj ; rtDW . njbocqae2t = 0U
; rtB . jjkwpfenp4 = 0.03 ; rtB . mjogabnmjg = 5.0 ; } break ; case
dpv45dh4v0 : if ( rtDW . njbocqae2t >= 1U ) { rtDW . co2n0fejda = gk5y14lpon
; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ; rtB . mjogabnmjg = 5.0 ;
} break ; case ep4d23adrm : if ( rtDW . njbocqae2t >= 1U ) { rtDW .
co2n0fejda = iua1sbu0j3 ; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ;
rtB . mjogabnmjg = 5.0 ; } break ; case nrqrzdpjwp : if ( rtDW . njbocqae2t
>= 1U ) { rtDW . co2n0fejda = dqhmmrgkr0 ; rtDW . njbocqae2t = 0U ; rtB .
jjkwpfenp4 = 0.05 ; rtB . mjogabnmjg = 5.0 ; } break ; case atdrzrw5eu : if (
rtDW . njbocqae2t >= 1U ) { rtDW . co2n0fejda = g2xlvoqnxr ; rtDW .
njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ; rtB . mjogabnmjg = 5.0 ; } break
; case msiuhxxah5 : if ( rtDW . njbocqae2t >= 1U ) { rtDW . co2n0fejda =
agou4iuzhb ; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ; rtB .
mjogabnmjg = 5.0 ; } break ; case nmpsodxnf1 : if ( rtB . br5qezvsfu && rtB .
faxpoeey0v ) { rtDW . co2n0fejda = bt3at1vcd3 ; rtDW . njbocqae2t = 0U ; rtB
. jjkwpfenp4 = 0.03 ; rtB . mtdmnunuoi = 0.0 ; rtB . mo2yss1m1k = 0.0 ; }
break ; case njy1ivq5gt : if ( rtB . br5qezvsfu && rtB . faxpoeey0v ) { rtDW
. co2n0fejda = bt3at1vcd3 ; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03
; rtB . mtdmnunuoi = 0.0 ; rtB . mo2yss1m1k = 0.0 ; } break ; case cj1fybdhr4
: if ( rtB . br5qezvsfu && rtB . faxpoeey0v ) { rtDW . co2n0fejda =
bt3at1vcd3 ; rtDW . njbocqae2t = 0U ; rtB . jjkwpfenp4 = 0.03 ; rtB .
mtdmnunuoi = 0.0 ; rtB . mo2yss1m1k = 0.0 ; } break ; case ovb3zhrf5j :
dkx1govs0g ( ) ; break ; case irhhvjkqic : dkx1govs0g ( ) ; break ; case
jm04rqucdj : dkx1govs0g ( ) ; break ; case btej00r3bi : m1k31lb0rc ( ) ;
break ; case nuhjvypsnz : rtB . ls0wcsfeyn = 1.0 ; break ; case fxmqe1tj5l :
if ( rtDW . njbocqae2t >= 25U ) { rtDW . co2n0fejda = nmpsodxnf1 ; rtB .
i5w0dsemue = rtB . dzwy32uw0n ; } break ; case ckjnuczefy : if ( rtDW .
njbocqae2t >= 25U ) { rtDW . co2n0fejda = njy1ivq5gt ; rtB . i5w0dsemue = rtB
. dzwy32uw0n ; } break ; case orvtclpxx4 : if ( rtDW . njbocqae2t >= 25U ) {
rtDW . co2n0fejda = irhhvjkqic ; rtB . i5w0dsemue = rtB . dzwy32uw0n ; }
break ; case bqiwxt5htw : if ( rtDW . njbocqae2t >= 25U ) { rtDW . co2n0fejda
= cj1fybdhr4 ; rtB . i5w0dsemue = rtB . dzwy32uw0n ; } break ; case
maxri0m3or : if ( rtDW . njbocqae2t >= 50U ) { rtDW . co2n0fejda = fxmqe1tj5l
; rtDW . njbocqae2t = 0U ; rtB . mtdmnunuoi = 1.0 ; rtB . mo2yss1m1k = 0.6 ;
rtB . jjkwpfenp4 = 0.0 ; } break ; case nli3jwkuqj : if ( rtDW . njbocqae2t
>= 50U ) { rtDW . co2n0fejda = ckjnuczefy ; rtDW . njbocqae2t = 0U ; rtB .
mo2yss1m1k = 0.6 ; rtB . jjkwpfenp4 = 0.0 ; } break ; case gk5y14lpon : if (
rtDW . njbocqae2t >= 50U ) { rtDW . co2n0fejda = bqiwxt5htw ; rtDW .
njbocqae2t = 0U ; rtB . mo2yss1m1k = - 0.6 ; rtB . jjkwpfenp4 = 0.0 ; } break
; case agou4iuzhb : if ( rtDW . njbocqae2t >= 50U ) { rtDW . co2n0fejda =
i5smfx3fdn ; rtDW . njbocqae2t = 0U ; rtB . mo2yss1m1k = 0.6 ; rtB .
jjkwpfenp4 = 0.0 ; } break ; case iua1sbu0j3 : if ( rtDW . njbocqae2t >= 60U
) { rtDW . co2n0fejda = orvtclpxx4 ; rtDW . njbocqae2t = 0U ; rtB .
mo2yss1m1k = 0.0 ; rtB . jjkwpfenp4 = 0.03 ; } break ; case g2xlvoqnxr : if (
rtDW . njbocqae2t >= 60U ) { rtDW . co2n0fejda = kw41sjmrpq ; rtDW .
njbocqae2t = 0U ; rtB . mtdmnunuoi = 1.0 ; rtB . mo2yss1m1k = 0.6 ; rtB .
jjkwpfenp4 = 0.0 ; } break ; case dqhmmrgkr0 : if ( rtDW . njbocqae2t >= 70U
) { rtDW . co2n0fejda = d0pes32jmr ; rtDW . njbocqae2t = 0U ; rtB .
mo2yss1m1k = 0.6 ; rtB . jjkwpfenp4 = 0.0 ; rtB . mtdmnunuoi = 1.0 ; } break
; case jwn2n42iuk : if ( rtDW . njbocqae2t >= 50U ) { rtDW . co2n0fejda =
h2qkac2pap ; rtDW . njbocqae2t = 0U ; rtB . mo2yss1m1k = 0.0 ; rtB .
jjkwpfenp4 = 0.03 ; } break ; case m0zspltx2s : hip4glo4zv ( ) ; break ; case
i5smfx3fdn : if ( rtDW . njbocqae2t >= 25U ) { rtDW . co2n0fejda = ovb3zhrf5j
; rtB . i5w0dsemue = rtB . dzwy32uw0n ; } break ; default : if ( rtDW .
njbocqae2t >= 10U ) { rtDW . co2n0fejda = gk5y14lpon ; rtDW . njbocqae2t = 0U
; rtB . jjkwpfenp4 = 0.03 ; rtB . mjogabnmjg = 5.0 ; } break ; } } counter =
suStrlen ( kxcjzg5yu4 ) ; b_copyfrom = obj1_Value -> size [ 0 ] * obj1_Value
-> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ; obj1_Value -> size [ 1 ] =
counter ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ; suStrToAscii ( ( uint8_T *
) & obj1_Value -> data [ 0 ] , counter , kxcjzg5yu4 ) ; equal = false ; if (
obj1_Value -> size [ 1 ] == 0 ) { equal = true ; } else if ( obj1_Value ->
size [ 1 ] == 0 ) { equal = true ; } if ( equal ) { b_copyfrom = obj1_Value
-> size [ 0 ] * obj1_Value -> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ;
obj1_Value -> size [ 1 ] = 1 ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ;
obj1_Value -> data [ 0 ] = ' ' ; } rtB . hskzxi0fod = l012ujkai0 ; inbf3ttenx
( & val , 2 ) ; switch ( ( int32_T ) rtB . mjogabnmjg ) { case 0 : b_copyfrom
= val -> size [ 0 ] * val -> size [ 1 ] ; val -> size [ 0 ] = 1 ; val -> size
[ 1 ] = obj1_Value -> size [ 1 ] + 1 ; fzjvjaawpn ( val , b_copyfrom ) ;
str_len = obj1_Value -> size [ 1 ] ; for ( b_in_idx = 0 ; b_in_idx < str_len
; b_in_idx ++ ) { val -> data [ b_in_idx ] = obj1_Value -> data [ b_in_idx ]
; } val -> data [ str_len ] = 'S' ; b_copyfrom = obj1_Value -> size [ 0 ] *
obj1_Value -> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ; obj1_Value -> size
[ 1 ] = val -> size [ 1 ] ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ; str_len
= val -> size [ 1 ] - 1 ; for ( b_in_idx = 0 ; b_in_idx <= str_len ; b_in_idx
++ ) { obj1_Value -> data [ b_in_idx ] = val -> data [ b_in_idx ] ; } rtB .
hskzxi0fod = l012ujkai0 + 1.0 ; break ; case 1 : b_copyfrom = val -> size [ 0
] * val -> size [ 1 ] ; val -> size [ 0 ] = 1 ; val -> size [ 1 ] =
obj1_Value -> size [ 1 ] + 1 ; fzjvjaawpn ( val , b_copyfrom ) ; str_len =
obj1_Value -> size [ 1 ] ; for ( b_in_idx = 0 ; b_in_idx < str_len ; b_in_idx
++ ) { val -> data [ b_in_idx ] = obj1_Value -> data [ b_in_idx ] ; } val ->
data [ str_len ] = 'L' ; b_copyfrom = obj1_Value -> size [ 0 ] * obj1_Value
-> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ; obj1_Value -> size [ 1 ] = val
-> size [ 1 ] ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ; str_len = val ->
size [ 1 ] - 1 ; for ( b_in_idx = 0 ; b_in_idx <= str_len ; b_in_idx ++ ) {
obj1_Value -> data [ b_in_idx ] = val -> data [ b_in_idx ] ; } rtB .
hskzxi0fod = l012ujkai0 + 1.0 ; break ; case 2 : b_copyfrom = val -> size [ 0
] * val -> size [ 1 ] ; val -> size [ 0 ] = 1 ; val -> size [ 1 ] =
obj1_Value -> size [ 1 ] + 1 ; fzjvjaawpn ( val , b_copyfrom ) ; str_len =
obj1_Value -> size [ 1 ] ; for ( b_in_idx = 0 ; b_in_idx < str_len ; b_in_idx
++ ) { val -> data [ b_in_idx ] = obj1_Value -> data [ b_in_idx ] ; } val ->
data [ str_len ] = 'R' ; b_copyfrom = obj1_Value -> size [ 0 ] * obj1_Value
-> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ; obj1_Value -> size [ 1 ] = val
-> size [ 1 ] ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ; str_len = val ->
size [ 1 ] - 1 ; for ( b_in_idx = 0 ; b_in_idx <= str_len ; b_in_idx ++ ) {
obj1_Value -> data [ b_in_idx ] = val -> data [ b_in_idx ] ; } rtB .
hskzxi0fod = l012ujkai0 + 1.0 ; break ; case 3 : b_copyfrom = val -> size [ 0
] * val -> size [ 1 ] ; val -> size [ 0 ] = 1 ; val -> size [ 1 ] =
obj1_Value -> size [ 1 ] + 1 ; fzjvjaawpn ( val , b_copyfrom ) ; str_len =
obj1_Value -> size [ 1 ] ; for ( b_in_idx = 0 ; b_in_idx < str_len ; b_in_idx
++ ) { val -> data [ b_in_idx ] = obj1_Value -> data [ b_in_idx ] ; } val ->
data [ str_len ] = 'B' ; b_copyfrom = obj1_Value -> size [ 0 ] * obj1_Value
-> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ; obj1_Value -> size [ 1 ] = val
-> size [ 1 ] ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ; str_len = val ->
size [ 1 ] - 1 ; for ( b_in_idx = 0 ; b_in_idx <= str_len ; b_in_idx ++ ) {
obj1_Value -> data [ b_in_idx ] = val -> data [ b_in_idx ] ; } rtB .
hskzxi0fod = l012ujkai0 + 1.0 ; break ; case 4 : b_copyfrom = val -> size [ 0
] * val -> size [ 1 ] ; val -> size [ 0 ] = 1 ; val -> size [ 1 ] =
obj1_Value -> size [ 1 ] + 1 ; fzjvjaawpn ( val , b_copyfrom ) ; str_len =
obj1_Value -> size [ 1 ] ; for ( b_in_idx = 0 ; b_in_idx < str_len ; b_in_idx
++ ) { val -> data [ b_in_idx ] = obj1_Value -> data [ b_in_idx ] ; } val ->
data [ str_len ] = 'E' ; b_copyfrom = obj1_Value -> size [ 0 ] * obj1_Value
-> size [ 1 ] ; obj1_Value -> size [ 0 ] = 1 ; obj1_Value -> size [ 1 ] = val
-> size [ 1 ] ; fzjvjaawpn ( obj1_Value , b_copyfrom ) ; str_len = val ->
size [ 1 ] - 1 ; for ( b_in_idx = 0 ; b_in_idx <= str_len ; b_in_idx ++ ) {
obj1_Value -> data [ b_in_idx ] = val -> data [ b_in_idx ] ; } rtB .
hskzxi0fod = l012ujkai0 + 1.0 ; break ; case 5 : break ; } y =
suAddStackString ( "" ) ; b_in_idx = obj1_Value -> size [ 1 ] ; suAsciiToStr
( y , ( uint8_T * ) & obj1_Value -> data [ 0 ] , b_in_idx ) ; suCopyString (
rtB . bkd0pdtznt , y ) ; suCopyString ( rtDW . ex0ljdtjz3 , rtB . bkd0pdtznt
) ; kt0wt4f5ht = suAddStackString ( NULL ) ; suCopyString ( kt0wt4f5ht , rtDW
. ex0ljdtjz3 ) ; counter = suStrlen ( kt0wt4f5ht ) ; b_copyfrom = yc -> size
[ 0 ] * yc -> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = counter
; fzjvjaawpn ( yc , b_copyfrom ) ; suStrToAscii ( ( uint8_T * ) & yc -> data
[ 0 ] , counter , kt0wt4f5ht ) ; b_copyfrom = val -> size [ 0 ] * val -> size
[ 1 ] ; val -> size [ 0 ] = 1 ; val -> size [ 1 ] = yc -> size [ 1 ] ;
fzjvjaawpn ( val , b_copyfrom ) ; str_len = yc -> size [ 1 ] - 1 ; for (
b_in_idx = 0 ; b_in_idx <= str_len ; b_in_idx ++ ) { val -> data [ b_in_idx ]
= yc -> data [ b_in_idx ] ; } counter = 0 ; s = 0 ; if ( rtB . ls0wcsfeyn ==
1.0 ) { inbf3ttenx ( & b_temp_Value , 2 ) ; inbf3ttenx ( & tmp_e , 2 ) ;
inbf3ttenx ( & tmp_i , 2 ) ; inbf3ttenx ( & tmp_m , 2 ) ; inbf3ttenx ( &
tmp_g , 2 ) ; while ( counter < 10 ) { str_len = val -> size [ 1 ] ; if ( val
-> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = 0 ; } else {
in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ; b_in_idx = 0 ; b_copyfrom = 1 ;
out_len = 0 ; while ( b_in_idx < str_len ) { b_in_idx ++ ; b_patt_idx = 1 ;
b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx <= 3 ) && ( b_tmp_in_idx <=
str_len ) && ( val -> data [ b_tmp_in_idx - 1 ] == k [ b_patt_idx - 1 ] ) ) {
b_tmp_in_idx ++ ; b_patt_idx ++ ; } if ( b_patt_idx > 3 ) { out_len ++ ;
b_copyfrom = b_tmp_in_idx ; } else if ( b_in_idx >= b_copyfrom ) { out_len ++
; } } b_copyfrom = yc -> size [ 0 ] * yc -> size [ 1 ] ; yc -> size [ 0 ] = 1
; yc -> size [ 1 ] = out_len ; fzjvjaawpn ( yc , b_copyfrom ) ; while (
in_idx < str_len ) { in_idx ++ ; b_in_idx = 1 ; b_copyfrom = in_idx ; while (
( b_in_idx <= 3 ) && ( b_copyfrom <= str_len ) && ( val -> data [ b_copyfrom
- 1 ] == k [ b_in_idx - 1 ] ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if (
b_in_idx > 3 ) { yc -> data [ out_idx ] = 'B' ; out_idx ++ ; copyfrom =
b_copyfrom ; } else if ( in_idx >= copyfrom ) { yc -> data [ out_idx ] = val
-> data [ in_idx - 1 ] ; out_idx ++ ; } } } b_y = suAddStackString ( "" ) ;
b_in_idx = yc -> size [ 1 ] ; suAsciiToStr ( b_y , ( uint8_T * ) & yc -> data
[ 0 ] , b_in_idx ) ; b_in_idx = suStrlen ( b_y ) ; b_copyfrom = d -> size [ 0
] * d -> size [ 1 ] ; d -> size [ 0 ] = 1 ; d -> size [ 1 ] = b_in_idx ;
fzjvjaawpn ( d , b_copyfrom ) ; suStrToAscii ( ( uint8_T * ) & d -> data [ 0
] , b_in_idx , b_y ) ; b_copyfrom = val -> size [ 0 ] * val -> size [ 1 ] ;
val -> size [ 0 ] = 1 ; val -> size [ 1 ] = d -> size [ 1 ] ; fzjvjaawpn (
val , b_copyfrom ) ; str_len = d -> size [ 1 ] - 1 ; for ( b_in_idx = 0 ;
b_in_idx <= str_len ; b_in_idx ++ ) { val -> data [ b_in_idx ] = d -> data [
b_in_idx ] ; } nghjzs0ayn ( val , tmp_e ) ; nghjzs0ayn1 ( tmp_e , tmp_i ) ;
nghjzs0ayn1g ( tmp_i , tmp_m ) ; nghjzs0ayn1gl ( tmp_m , tmp_g ) ;
nghjzs0ayn1glc ( tmp_g , b_temp_Value ) ; str_len = b_temp_Value -> size [ 1
] ; if ( b_temp_Value -> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ; yc ->
size [ 1 ] = 0 ; } else { in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ; b_in_idx
= 0 ; b_copyfrom = 1 ; out_len = 0 ; while ( b_in_idx < str_len ) { b_in_idx
++ ; b_patt_idx = 1 ; b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx <= 2 )
&& ( b_tmp_in_idx <= str_len ) && ( b_temp_Value -> data [ b_tmp_in_idx - 1 ]
== 'S' ) ) { b_tmp_in_idx ++ ; b_patt_idx ++ ; } if ( b_patt_idx > 2 ) {
out_len ++ ; b_copyfrom = b_tmp_in_idx ; } else if ( b_in_idx >= b_copyfrom )
{ out_len ++ ; } } b_copyfrom = yc -> size [ 0 ] * yc -> size [ 1 ] ; yc ->
size [ 0 ] = 1 ; yc -> size [ 1 ] = out_len ; fzjvjaawpn ( yc , b_copyfrom )
; while ( in_idx < str_len ) { in_idx ++ ; b_in_idx = 1 ; b_copyfrom = in_idx
; while ( ( b_in_idx <= 2 ) && ( b_copyfrom <= str_len ) && ( b_temp_Value ->
data [ b_copyfrom - 1 ] == 'S' ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if (
b_in_idx > 2 ) { yc -> data [ out_idx ] = 'S' ; out_idx ++ ; copyfrom =
b_copyfrom ; } else if ( in_idx >= copyfrom ) { yc -> data [ out_idx ] =
b_temp_Value -> data [ in_idx - 1 ] ; out_idx ++ ; } } } c_y =
suAddStackString ( "" ) ; b_in_idx = yc -> size [ 1 ] ; suAsciiToStr ( c_y ,
( uint8_T * ) & yc -> data [ 0 ] , b_in_idx ) ; b_in_idx = suStrlen ( c_y ) ;
b_copyfrom = g -> size [ 0 ] * g -> size [ 1 ] ; g -> size [ 0 ] = 1 ; g ->
size [ 1 ] = b_in_idx ; fzjvjaawpn ( g , b_copyfrom ) ; suStrToAscii ( (
uint8_T * ) & g -> data [ 0 ] , b_in_idx , c_y ) ; str_len = g -> size [ 1 ]
; if ( g -> size [ 1 ] == 0 ) { yc -> size [ 0 ] = 1 ; yc -> size [ 1 ] = 0 ;
} else { in_idx = 0 ; copyfrom = 1 ; out_idx = 0 ; b_in_idx = 0 ; b_copyfrom
= 1 ; out_len = 0 ; while ( b_in_idx < str_len ) { b_in_idx ++ ; b_patt_idx =
1 ; b_tmp_in_idx = b_in_idx ; while ( ( b_patt_idx <= 2 ) && ( b_tmp_in_idx
<= str_len ) && ( g -> data [ b_tmp_in_idx - 1 ] == 'B' ) ) { b_tmp_in_idx ++
; b_patt_idx ++ ; } if ( b_patt_idx > 2 ) { out_len ++ ; b_copyfrom =
b_tmp_in_idx ; } else if ( b_in_idx >= b_copyfrom ) { out_len ++ ; } }
b_copyfrom = yc -> size [ 0 ] * yc -> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc
-> size [ 1 ] = out_len ; fzjvjaawpn ( yc , b_copyfrom ) ; while ( in_idx <
str_len ) { in_idx ++ ; b_in_idx = 1 ; b_copyfrom = in_idx ; while ( (
b_in_idx <= 2 ) && ( b_copyfrom <= str_len ) && ( g -> data [ b_copyfrom - 1
] == 'B' ) ) { b_copyfrom ++ ; b_in_idx ++ ; } if ( b_in_idx > 2 ) { yc ->
data [ out_idx ] = 'B' ; out_idx ++ ; copyfrom = b_copyfrom ; } else if (
in_idx >= copyfrom ) { yc -> data [ out_idx ] = g -> data [ in_idx - 1 ] ;
out_idx ++ ; } } } d_y = suAddStackString ( "" ) ; b_in_idx = yc -> size [ 1
] ; suAsciiToStr ( d_y , ( uint8_T * ) & yc -> data [ 0 ] , b_in_idx ) ;
b_in_idx = suStrlen ( d_y ) ; b_copyfrom = i -> size [ 0 ] * i -> size [ 1 ]
; i -> size [ 0 ] = 1 ; i -> size [ 1 ] = b_in_idx ; fzjvjaawpn ( i ,
b_copyfrom ) ; suStrToAscii ( ( uint8_T * ) & i -> data [ 0 ] , b_in_idx ,
d_y ) ; b_copyfrom = val -> size [ 0 ] * val -> size [ 1 ] ; val -> size [ 0
] = 1 ; val -> size [ 1 ] = i -> size [ 1 ] ; fzjvjaawpn ( val , b_copyfrom )
; str_len = i -> size [ 1 ] - 1 ; for ( b_in_idx = 0 ; b_in_idx <= str_len ;
b_in_idx ++ ) { val -> data [ b_in_idx ] = i -> data [ b_in_idx ] ; } counter
++ ; } j42rpwppwg ( & tmp_g ) ; j42rpwppwg ( & tmp_m ) ; j42rpwppwg ( & tmp_i
) ; j42rpwppwg ( & tmp_e ) ; j42rpwppwg ( & b_temp_Value ) ; } b_copyfrom =
yc -> size [ 0 ] * yc -> size [ 1 ] ; yc -> size [ 0 ] = 1 ; yc -> size [ 1 ]
= val -> size [ 1 ] + 1 ; fzjvjaawpn ( yc , b_copyfrom ) ; str_len = val ->
size [ 1 ] ; for ( b_in_idx = 0 ; b_in_idx < str_len ; b_in_idx ++ ) { yc ->
data [ b_in_idx ] = val -> data [ b_in_idx ] ; } yc -> data [ val -> size [ 1
] ] = 'x' ; j42rpwppwg ( & val ) ; if ( counter == 10 ) { s = 1 ; } y_p =
suAddStackString ( "" ) ; b_in_idx = yc -> size [ 1 ] ; suAsciiToStr ( y_p ,
( uint8_T * ) & yc -> data [ 0 ] , b_in_idx ) ; rtB . chxyftuvao = s ;
suCopyString ( rtDW . ex0ljdtjz3 , y_p ) ; len = ( uint32_T ) suStrlen ( y_p
) ; rtB . gnp2aeui1f = rtP . Constant2_Value + ( real_T ) len ; l012ujkai0 =
rtDW . caiyox4oj2 [ 0 ] ; len = ( uint32_T ) suStrlen ( y_p ) ; if ( len >
31U ) { S = rtS ; diag = CreateDiagnosticAsVoidPtr (
"Simulink:StringBlocks:StringBufferOverflow" , 4 , 1 , 1 , 5 ,
"sim_Start/Optimization Subsystem/String to ASCII" , 1 , len , 1 , 31 ) ;
rt_ssReportDiagnosticAsWarning ( S , diag ) ; } suStrToAscii ( & tmp [ 0 ] ,
31 , y_p ) ; for ( b_in_idx = 0 ; b_in_idx < 31 ; b_in_idx ++ ) { rtB .
g2v4ytatik [ b_in_idx ] = tmp [ b_in_idx ] ; } if ( rtB . ls0wcsfeyn > rtP .
Switch2_Threshold ) { rtB . p01c3bphbw = rtP . Constant_Value ; } else { rtB
. p01c3bphbw = rtP . Constant1_Value ; } if ( l012ujkai0 > rtP .
Switch3_Threshold ) { suCopyString ( rtB . izgo3pnyfb , rtB . o0ftqgjmrw ) ;
} else { suCopyString ( rtB . izgo3pnyfb , NULL ) ; } { rtB . cf5djepmnt = (
rtP . Internal_C [ 0 ] ) * rtDW . gyqsentnna [ 0 ] + ( rtP . Internal_C [ 1 ]
) * rtDW . gyqsentnna [ 1 ] + ( rtP . Internal_C [ 2 ] ) * rtDW . gyqsentnna
[ 2 ] ; } if ( rtP . LeftMotor_sensorType > rtP . Switch_Threshold ) {
l012ujkai0 = rtP . Gain_Gain * rtDW . iafj1kgffe ; } else { l012ujkai0 = rtB
. cf5djepmnt ; } { rtB . hqdziciins = ( rtP . Internal_C_hiyczu5j42 [ 0 ] ) *
rtDW . psgym2jyk0 [ 0 ] + ( rtP . Internal_C_hiyczu5j42 [ 1 ] ) * rtDW .
psgym2jyk0 [ 1 ] + ( rtP . Internal_C_hiyczu5j42 [ 2 ] ) * rtDW . psgym2jyk0
[ 2 ] ; } if ( rtP . RightMotor_sensorType > rtP .
Switch_Threshold_bmwdvzwwf2 ) { deu1hqt5kj = rtP . Gain_Gain_o4hmcpr5ca *
rtDW . j2fbjqvjcp ; } else { deu1hqt5kj = rtB . hqdziciins ; } rtB .
e41kschlmd = ( deu1hqt5kj - l012ujkai0 ) * rtB . ppg1emiqtt ; l012ujkai0 = (
l012ujkai0 + deu1hqt5kj ) * rtB . orypiaqyjn ; rtB . jnbrvrbl4c = l012ujkai0
* muDoubleScalarCos ( aok40h4fye ) ; rtB . j4n5p1agp1 = l012ujkai0 *
muDoubleScalarSin ( aok40h4fye ) ; { SimStruct * rts = ssGetSFunction ( rtS ,
0 ) ; { static const char * blockSIDForSFcnLoader = "sim_Start:4:401" ;
sfcnLoader_setCurrentSFcnBlockSID ( blockSIDForSFcnLoader ) ; void ( *
sfcnMethodPtr ) ( SimStruct * , int ) = rts -> modelMethods . sFcn .
mdlOutputs . level2 ; sfcnLoader_separateComplexHandler_withTID ( rts ,
sfcnMethodPtr , ( 0 <= 1 ) && gbl_raccel_tid01eq ? 0 : 0 ) ; } } { real_T *
pDataValues = ( real_T * ) rtDW . k3xrmmg34t . DataPtr ; m1ultwsa1j =
pDataValues [ 0 ] ; } { uint8_T * pDataValues = ( uint8_T * ) rtDW .
pg5gatyniv . DataPtr ; real_T * pTimeValues = ( real_T * ) rtDW . pg5gatyniv
. TimePtr ; int_T currTimeIndex = rtDW . bn4k2qv04s . PrevIndex ; real_T t =
ssGetTaskTime ( rtS , 0 ) ; int numPoints , lastPoint ; FWksInfo *
fromwksInfo = ( FWksInfo * ) rtDW . pg5gatyniv . RSimInfoPtr ; numPoints =
fromwksInfo -> nDataPoints ; lastPoint = numPoints - 1 ; if ( t <=
pTimeValues [ 0 ] ) { currTimeIndex = 0 ; } else if ( t >= pTimeValues [
lastPoint ] ) { currTimeIndex = lastPoint - 1 ; } else { if ( t < pTimeValues
[ currTimeIndex ] ) { while ( t < pTimeValues [ currTimeIndex ] ) {
currTimeIndex -- ; } } else { while ( t >= pTimeValues [ currTimeIndex + 1 ]
) { currTimeIndex ++ ; } } } rtDW . bn4k2qv04s . PrevIndex = currTimeIndex ;
{ real_T t1 = pTimeValues [ currTimeIndex ] ; real_T t2 = pTimeValues [
currTimeIndex + 1 ] ; if ( t1 == t2 ) { if ( t < t1 ) { { int_T elIdx ; for (
elIdx = 0 ; elIdx < 31 ; ++ elIdx ) { ( & rtB . jialwgyso4 [ 0 ] ) [ elIdx ]
= pDataValues [ currTimeIndex ] ; pDataValues += numPoints ; } } } else { {
int_T elIdx ; for ( elIdx = 0 ; elIdx < 31 ; ++ elIdx ) { ( & rtB .
jialwgyso4 [ 0 ] ) [ elIdx ] = pDataValues [ currTimeIndex + 1 ] ;
pDataValues += numPoints ; } } } } else { real_T f1 = ( t2 - t ) / ( t2 - t1
) ; real_T f2 = 1.0 - f1 ; uint8_T d1 ; uint8_T d2 ; real_T tempOut ; { int_T
elIdx ; for ( elIdx = 0 ; elIdx < 31 ; ++ elIdx ) { d1 = pDataValues [
currTimeIndex ] ; d2 = pDataValues [ currTimeIndex + 1 ] ; tempOut =
rtInterpolate ( d1 , d2 , f1 , f2 ) ; if ( tempOut >= MAX_uint8_T ) { ( & rtB
. jialwgyso4 [ 0 ] ) [ elIdx ] = MAX_uint8_T ; } else if ( tempOut <= ( (
uint8_T ) ( 0U ) ) ) { ( & rtB . jialwgyso4 [ 0 ] ) [ elIdx ] = ( ( uint8_T )
( 0U ) ) ; } else { ( & rtB . jialwgyso4 [ 0 ] ) [ elIdx ] = ( uint8_T )
rtRound ( tempOut ) ; } pDataValues += numPoints ; } } } } } { real_T *
pDataValues = ( real_T * ) rtDW . jayrspqml1 . DataPtr ; iaqcyeykxh =
pDataValues [ 0 ] ; } counter = 0 ; if ( rtB . jialwgyso4 [ ( int32_T )
iaqcyeykxh - 1 ] > 0 ) { counter = 1 ; } rtB . mql3fd1ucd = ( ( m1ultwsa1j !=
0.0 ) && ( counter != 0 ) ) ; rtB . o1ijyw24e0 = rtDW . p2qqabsh1z ; if (
rtDW . bgn14fgjm3 < 255U ) { rtDW . bgn14fgjm3 ++ ; } rtDW . grgj1nbj2f =
hps0ylwak0 ; if ( rtDW . kfkq4jq4py == 0U ) { rtDW . kfkq4jq4py = 1U ; rtDW .
m1jml1zdzn = i0c2gg1zyu ; rtDW . a42pr4cd24 = 1.0 ; } else { switch ( rtDW .
m1jml1zdzn ) { case bt3at1vcd3 : rtB . pnwjxb31po = 0.0 ; if ( rtDW .
bgn14fgjm3 >= 150U ) { rtDW . m1jml1zdzn = g1nz51j2op ; rtDW . a42pr4cd24 ++
; rtB . jl4a230o5q = 0.03 ; rtB . pnwjxb31po = 0.0 ; rtB . oqf5mpl1fz = 0.0 ;
} break ; case ncvh5u3vyd : rtB . pnwjxb31po = 0.0 ; if ( rtDW . bgn14fgjm3
>= 150U ) { rtDW . m1jml1zdzn = g1nz51j2op ; rtDW . a42pr4cd24 ++ ; rtB .
jl4a230o5q = 0.03 ; rtB . pnwjxb31po = 0.0 ; rtB . oqf5mpl1fz = 0.0 ; } break
; case jzjomt55gg : rtB . pnwjxb31po = 0.0 ; if ( rtDW . bgn14fgjm3 >= 150U )
{ rtDW . m1jml1zdzn = g1nz51j2op ; rtDW . a42pr4cd24 ++ ; rtB . jl4a230o5q =
0.03 ; rtB . pnwjxb31po = 0.0 ; rtB . oqf5mpl1fz = 0.0 ; } break ; case
dywneg1ovl : if ( rtB . br5qezvsfu && rtB . faxpoeey0v ) { rtDW . m1jml1zdzn
= bt3at1vcd3 ; rtDW . bgn14fgjm3 = 0U ; rtB . jl4a230o5q = 0.03 ; rtB .
pnwjxb31po = 0.0 ; rtB . dxmhlj5cu2 = 0.0 ; } break ; case mf0n0qtzom : if (
rtB . br5qezvsfu && rtB . faxpoeey0v ) { rtDW . m1jml1zdzn = ncvh5u3vyd ;
rtDW . bgn14fgjm3 = 0U ; rtB . jl4a230o5q = 0.03 ; rtB . pnwjxb31po = 0.0 ;
rtB . dxmhlj5cu2 = 0.0 ; } break ; case crdnmesixf : if ( rtB . br5qezvsfu &&
rtB . faxpoeey0v ) { rtDW . m1jml1zdzn = jzjomt55gg ; rtDW . bgn14fgjm3 = 0U
; rtB . jl4a230o5q = 0.03 ; rtB . pnwjxb31po = 0.0 ; rtB . dxmhlj5cu2 = 0.0 ;
} break ; case jfua5qkldn : break ; case flmi0v3t0n : rtB . pnwjxb31po = 1.0
; if ( rtDW . bgn14fgjm3 >= 25U ) { rtDW . m1jml1zdzn = crdnmesixf ; rtB .
oqf5mpl1fz = rtB . o1ijyw24e0 ; } break ; case cqizbzg1ld : rtB . pnwjxb31po
= 1.0 ; if ( rtDW . bgn14fgjm3 >= 25U ) { rtDW . m1jml1zdzn = dywneg1ovl ;
rtB . oqf5mpl1fz = rtB . o1ijyw24e0 ; } break ; case jg3n5uukzi : rtB .
pnwjxb31po = 1.0 ; if ( rtDW . bgn14fgjm3 >= 25U ) { rtDW . m1jml1zdzn =
mf0n0qtzom ; rtB . oqf5mpl1fz = rtB . o1ijyw24e0 ; } break ; case nhjyaksagi
: if ( rtDW . bgn14fgjm3 >= 50U ) { rtDW . m1jml1zdzn = cqizbzg1ld ; rtDW .
bgn14fgjm3 = 0U ; rtB . pnwjxb31po = 1.0 ; rtB . dxmhlj5cu2 = 0.6 ; rtB .
jl4a230o5q = 0.0 ; } break ; case bxfbrmulsm : if ( rtDW . bgn14fgjm3 >= 50U
) { rtDW . m1jml1zdzn = jg3n5uukzi ; rtDW . bgn14fgjm3 = 0U ; rtB .
pnwjxb31po = 1.0 ; rtB . dxmhlj5cu2 = - 0.6 ; rtB . jl4a230o5q = 0.0 ; }
break ; case p5vg45rs3h : if ( rtDW . bgn14fgjm3 >= 50U ) { rtDW . m1jml1zdzn
= flmi0v3t0n ; rtDW . bgn14fgjm3 = 0U ; rtB . pnwjxb31po = 1.0 ; rtB .
jl4a230o5q = 0.03 ; } break ; case i0c2gg1zyu : if ( rtB . mql3fd1ucd ) {
rtDW . m1jml1zdzn = fnusxpnwqe ; rtDW . bgn14fgjm3 = 0U ; rtB . dxmhlj5cu2 =
0.6 ; } break ; case g1nz51j2op : rtB . pnwjxb31po = 0.0 ; if ( ( rtB .
o1ijyw24e0 > 0.0 ) && ( rtB . jialwgyso4 [ ( int32_T ) rtDW . a42pr4cd24 - 1
] == 76 ) ) { rtB . dxmhlj5cu2 = 0.0 ; rtDW . m1jml1zdzn = nhjyaksagi ; rtDW
. bgn14fgjm3 = 0U ; rtB . jl4a230o5q = 0.03 ; } else if ( ( rtB . o1ijyw24e0
> 0.0 ) && ( rtB . jialwgyso4 [ ( int32_T ) rtDW . a42pr4cd24 - 1 ] == 83 ) )
{ rtB . dxmhlj5cu2 = 0.0 ; rtDW . m1jml1zdzn = p5vg45rs3h ; rtDW . bgn14fgjm3
= 0U ; rtB . jl4a230o5q = 0.03 ; } else if ( ( rtB . o1ijyw24e0 > 0.0 ) && (
rtB . jialwgyso4 [ ( int32_T ) rtDW . a42pr4cd24 - 1 ] == 120 ) ) { rtDW .
m1jml1zdzn = jfua5qkldn ; rtB . dxmhlj5cu2 = 0.0 ; rtB . jl4a230o5q = 0.0 ; }
else if ( ( rtB . o1ijyw24e0 > 0.0 ) && ( rtB . jialwgyso4 [ ( int32_T ) rtDW
. a42pr4cd24 - 1 ] == 82 ) ) { rtB . dxmhlj5cu2 = 0.0 ; rtDW . m1jml1zdzn =
bxfbrmulsm ; rtDW . bgn14fgjm3 = 0U ; rtB . jl4a230o5q = 0.03 ; } break ;
case kpnsrpe45j : rtB . pnwjxb31po = 0.0 ; if ( rtDW . bgn14fgjm3 >= 10U ) {
rtDW . m1jml1zdzn = jg3n5uukzi ; rtDW . bgn14fgjm3 = 0U ; rtB . pnwjxb31po =
1.0 ; rtB . dxmhlj5cu2 = - 0.6 ; rtB . jl4a230o5q = 0.0 ; } break ; default :
if ( rtDW . bgn14fgjm3 >= 50U ) { rtDW . m1jml1zdzn = kpnsrpe45j ; rtDW .
bgn14fgjm3 = 0U ; rtB . pnwjxb31po = 0.0 ; rtB . dxmhlj5cu2 = 0.0 ; rtB .
jl4a230o5q = 0.03 ; } break ; } } rtB . ezqnv40hor = rtB . mtdmnunuoi + rtB .
pnwjxb31po ; eo3swzvasq ( 1 , rtS , rtB . h012lh2sdm , rtB . kawa0ssxug , rtB
. hng4zofqfq , & rtP . Constant_Value_nizol5xyrr , & rtP .
Constant1_Value_larkvagb33 , & h5onfuqhtm , rtDW . btjpsv3ywi , & rtP .
btjpsv3ywi ) ; if ( h5onfuqhtm > rtP . Switch_Threshold_owqwz4ijh5 ) { rtB .
krhfkykqos = ! ( rtP . RightLineSensor_envVal != 0.0 ) ; } else { rtB .
krhfkykqos = ! ( rtP . RightLineSensor_lineVal != 0.0 ) ; } eo3swzvasq ( 1 ,
rtS , rtB . h012lh2sdm , rtB . kawa0ssxug , rtB . hng4zofqfq , & rtP .
Constant_Value_hlhm5ajdso , & rtP . Constant1_Value_plr2cy0rrk , & kcxjzwo4sq
, rtDW . eo3swzvasqb , & rtP . eo3swzvasqb ) ; if ( kcxjzwo4sq > rtP .
Switch_Threshold_ijwjdx5hx3 ) { rtB . amaivwoi3x = ! ( rtP .
LeftLineSensor_envVal != 0.0 ) ; } else { rtB . amaivwoi3x = ! ( rtP .
LeftLineSensor_lineVal != 0.0 ) ; } if ( rtDW . j04nfygwcf < 31U ) { rtDW .
j04nfygwcf ++ ; } rtDW . gbtehizmoh = hps0ylwak0 ; if ( rtDW . nyhsr4t3fp ==
0U ) { rtDW . nyhsr4t3fp = 1U ; rtDW . mqobma15yv = o3u1pgvs0u ; rtB .
hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 0.0 ; } else { switch ( rtDW .
mqobma15yv ) { case cqttlgnk1c : if ( rtDW . j04nfygwcf >= 20U ) { rtDW .
mqobma15yv = mbnkdlgcja ; } break ; case nvtrslvbe5 : if ( rtDW . j04nfygwcf
>= 1U ) { rtDW . mqobma15yv = payc5e5ryc ; rtB . hldfmfuah3 = 0.0 ; } break ;
case n34wplmkl4 : rtDW . mqobma15yv = o3u1pgvs0u ; rtB . hldfmfuah3 = 0.0 ;
rtB . ii32dgziov = 0.0 ; break ; case d0pes32jmr : if ( rtDW . j04nfygwcf >=
1U ) { rtDW . mqobma15yv = o3u1pgvs0u ; rtB . hldfmfuah3 = 0.0 ; rtB .
ii32dgziov = 0.0 ; } break ; case b054agindn : if ( rtB . br5qezvsfu && rtB .
faxpoeey0v && rtB . krhfkykqos && rtB . amaivwoi3x ) { rtDW . mqobma15yv =
j1pnblzda5 ; rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 2.0 ; } else if (
rtDW . j04nfygwcf >= 20U ) { rtDW . mqobma15yv = o3u1pgvs0u ; rtB .
hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 0.0 ; } break ; case mbnkdlgcja : if (
( ! rtB . br5qezvsfu ) && ( ! rtB . faxpoeey0v ) && ( ! rtB . krhfkykqos ) &&
( ! rtB . amaivwoi3x ) ) { rtDW . mqobma15yv = fp2devqxsy ; rtDW . j04nfygwcf
= 0U ; rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 6.0 ; } else if ( ( rtB .
br5qezvsfu || rtB . faxpoeey0v ) && ( ! rtB . krhfkykqos ) && ( ! rtB .
amaivwoi3x ) ) { rtDW . mqobma15yv = nvtrslvbe5 ; rtDW . j04nfygwcf = 0U ;
rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 5.0 ; } else if ( rtB .
br5qezvsfu && rtB . faxpoeey0v && rtB . krhfkykqos && rtB . amaivwoi3x ) {
rtDW . mqobma15yv = b054agindn ; rtDW . j04nfygwcf = 0U ; } break ; case
j1pnblzda5 : rtDW . mqobma15yv = o3u1pgvs0u ; rtB . hldfmfuah3 = 0.0 ; rtB .
ii32dgziov = 0.0 ; break ; case ddgtoumbar : if ( rtDW . j04nfygwcf >= 1U ) {
rtDW . mqobma15yv = n34wplmkl4 ; rtB . hldfmfuah3 = 0.0 ; } break ; case
ikr1dxkfwn : if ( ( ! rtB . br5qezvsfu ) && ( ! rtB . faxpoeey0v ) ) { rtDW .
mqobma15yv = ddgtoumbar ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 = 0.0 ;
rtB . ii32dgziov = 8.0 ; } else if ( rtDW . j04nfygwcf >= 20U ) { rtDW .
mqobma15yv = oq4m20xxx1 ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 = 0.0 ;
rtB . ii32dgziov = 4.0 ; } else if ( rtB . krhfkykqos && rtB . amaivwoi3x ) {
rtDW . mqobma15yv = cqttlgnk1c ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 =
0.0 ; } break ; case oq4m20xxx1 : if ( rtDW . j04nfygwcf >= 1U ) { rtDW .
mqobma15yv = n34wplmkl4 ; rtB . hldfmfuah3 = 0.0 ; } break ; case ewdqahmcux
: if ( rtB . br5qezvsfu == rtB . faxpoeey0v ) { rtB . la1brdabwh = 0.0 ; rtDW
. mqobma15yv = o3u1pgvs0u ; rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 0.0 ;
} break ; case ihinm10c3b : if ( rtB . br5qezvsfu == rtB . faxpoeey0v ) { rtB
. la1brdabwh = 0.0 ; rtDW . mqobma15yv = o3u1pgvs0u ; rtB . hldfmfuah3 = 0.0
; rtB . ii32dgziov = 0.0 ; } break ; case nymocdvpmn : if ( rtDW . j04nfygwcf
>= 1U ) { rtDW . mqobma15yv = n34wplmkl4 ; rtB . hldfmfuah3 = 0.0 ; } break ;
case m3hjzoz043 : if ( ( ! rtB . br5qezvsfu ) && ( ! rtB . faxpoeey0v ) ) {
rtDW . mqobma15yv = nymocdvpmn ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 =
0.0 ; rtB . ii32dgziov = 7.0 ; } else if ( rtDW . j04nfygwcf >= 20U ) { rtDW
. mqobma15yv = fphfoyh1fa ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 = 0.0 ;
rtB . ii32dgziov = 3.0 ; } else if ( rtB . krhfkykqos && rtB . amaivwoi3x ) {
rtDW . mqobma15yv = cqttlgnk1c ; rtDW . j04nfygwcf = 0U ; rtB . hldfmfuah3 =
0.0 ; } break ; case fphfoyh1fa : if ( rtDW . j04nfygwcf >= 1U ) { rtDW .
mqobma15yv = n34wplmkl4 ; rtB . hldfmfuah3 = 0.0 ; } break ; case o3u1pgvs0u
: fc04r2nndv ( ) ; break ; case payc5e5ryc : rtDW . mqobma15yv = o3u1pgvs0u ;
rtB . hldfmfuah3 = 0.0 ; rtB . ii32dgziov = 0.0 ; break ; default : if ( rtDW
. j04nfygwcf >= 1U ) { rtDW . mqobma15yv = payc5e5ryc ; rtB . hldfmfuah3 =
0.0 ; } break ; } } rtB . dvpimpdrs3 = rtB . la1brdabwh + rtB . mo2yss1m1k ;
rtB . dkze5lu4ma = rtB . hldfmfuah3 + rtB . jjkwpfenp4 ; rtB . n4zowml5qq =
rtB . hldfmfuah3 + rtB . jl4a230o5q ; rtB . ltxnmikrnx = rtB . la1brdabwh +
rtB . dxmhlj5cu2 ; rtB . nm1wroc1sb = rtB . ii32dgziov - rtB . i5w0dsemue ;
rtB . ktb4r2yzkb = rtB . ii32dgziov - rtB . oqf5mpl1fz ; { real_T *
pDataValues = ( real_T * ) rtDW . opv4zomgtb . DataPtr ; k3ehhwacx5 =
pDataValues [ 0 ] ; } aok40h4fye = 1.0 / rtP . Towlwr_wheelR ; if (
k3ehhwacx5 > rtP . Switch1_Threshold ) { l012ujkai0 = rtB . n4zowml5qq ; }
else { l012ujkai0 = rtB . dkze5lu4ma ; } if ( k3ehhwacx5 > rtP .
Switch_Threshold_iry4v5n5zm ) { deu1hqt5kj = rtB . ltxnmikrnx ; } else {
deu1hqt5kj = rtB . dvpimpdrs3 ; } rtB . n3rebawkox = rtP . InputPWM [
plook_u32d_binckan ( ( rtP . changeparameters_Gain [ 0 ] * l012ujkai0 + rtP .
changeparameters_Gain [ 2 ] * deu1hqt5kj ) * aok40h4fye , rtP . WheelSpeed ,
172U ) ] ; rtB . eryzgsl04j = rtP . InputPWM [ plook_u32d_binckan ( ( rtP .
changeparameters_Gain [ 1 ] * l012ujkai0 + rtP . changeparameters_Gain [ 3 ]
* deu1hqt5kj ) * aok40h4fye , rtP . WheelSpeed , 172U ) ] ;
suSetStringStackSize ( tmp_p ) ; j42rpwppwg ( & i ) ; j42rpwppwg ( & g ) ;
j42rpwppwg ( & d ) ; j42rpwppwg ( & yc ) ; j42rpwppwg ( & obj1_Value ) ;
UNUSED_PARAMETER ( tid ) ; } void MdlOutputsTID2 ( int_T tid ) { suCopyString
( rtB . o0ftqgjmrw , rtP . StringConstant_String ) ; rtB . ppg1emiqtt = rtP .
wheelR / rtP . axleLength ; rtB . orypiaqyjn = rtP . Gain1_Gain * rtP .
wheelR ; UNUSED_PARAMETER ( tid ) ; } void MdlUpdate ( int_T tid ) { int32_T
idxDelay ; suCopyString ( rtDW . icjcuyoesf , rtB . bkd0pdtznt ) ; rtDW .
pd03hpujxp = rtB . hskzxi0fod ; rtDW . lruetkegpd = rtB . nm1wroc1sb ; rtDW .
jlsempkvov += rtP . DiscreteTimeIntegrator_gainval * rtB . jnbrvrbl4c ; rtDW
. flos1pd3x0 += rtP . DiscreteTimeIntegrator1_gainval * rtB . j4n5p1agp1 ;
rtDW . ileqgfdrx1 += rtP . DiscreteTimeIntegrator2_gainval * rtB . e41kschlmd
; for ( idxDelay = 0 ; idxDelay < 1499 ; idxDelay ++ ) { rtDW . caiyox4oj2 [
( uint32_T ) idxDelay ] = rtDW . caiyox4oj2 [ idxDelay + 1U ] ; } rtDW .
caiyox4oj2 [ 1499 ] = rtB . chxyftuvao ; rtDW . iafj1kgffe += rtP .
DiscreteTimeIntegrator_gainval_afikqgzpe0 * rtB . cf5djepmnt ; { real_T xnew
[ 3 ] ; xnew [ 0 ] = ( rtP . Internal_A [ 0 ] ) * rtDW . gyqsentnna [ 0 ] + (
rtP . Internal_A [ 1 ] ) * rtDW . gyqsentnna [ 1 ] + ( rtP . Internal_A [ 2 ]
) * rtDW . gyqsentnna [ 2 ] ; xnew [ 0 ] += ( rtP . Internal_B [ 0 ] ) * rtB
. n3rebawkox ; xnew [ 1 ] = ( rtP . Internal_A [ 3 ] ) * rtDW . gyqsentnna [
0 ] + ( rtP . Internal_A [ 4 ] ) * rtDW . gyqsentnna [ 1 ] + ( rtP .
Internal_A [ 5 ] ) * rtDW . gyqsentnna [ 2 ] ; xnew [ 1 ] += ( rtP .
Internal_B [ 1 ] ) * rtB . n3rebawkox ; xnew [ 2 ] = ( rtP . Internal_A [ 6 ]
) * rtDW . gyqsentnna [ 0 ] + ( rtP . Internal_A [ 7 ] ) * rtDW . gyqsentnna
[ 1 ] + ( rtP . Internal_A [ 8 ] ) * rtDW . gyqsentnna [ 2 ] ; xnew [ 2 ] +=
( rtP . Internal_B [ 2 ] ) * rtB . n3rebawkox ; ( void ) memcpy ( & rtDW .
gyqsentnna [ 0 ] , xnew , sizeof ( real_T ) * 3 ) ; } rtDW . j2fbjqvjcp +=
rtP . DiscreteTimeIntegrator_gainval_ptlrlhszmg * rtB . hqdziciins ; { real_T
xnew [ 3 ] ; xnew [ 0 ] = ( rtP . Internal_A_avp5ztxxzl [ 0 ] ) * rtDW .
psgym2jyk0 [ 0 ] + ( rtP . Internal_A_avp5ztxxzl [ 1 ] ) * rtDW . psgym2jyk0
[ 1 ] + ( rtP . Internal_A_avp5ztxxzl [ 2 ] ) * rtDW . psgym2jyk0 [ 2 ] ;
xnew [ 0 ] += ( rtP . Internal_B_ebkdikdi50 [ 0 ] ) * rtB . eryzgsl04j ; xnew
[ 1 ] = ( rtP . Internal_A_avp5ztxxzl [ 3 ] ) * rtDW . psgym2jyk0 [ 0 ] + (
rtP . Internal_A_avp5ztxxzl [ 4 ] ) * rtDW . psgym2jyk0 [ 1 ] + ( rtP .
Internal_A_avp5ztxxzl [ 5 ] ) * rtDW . psgym2jyk0 [ 2 ] ; xnew [ 1 ] += ( rtP
. Internal_B_ebkdikdi50 [ 1 ] ) * rtB . eryzgsl04j ; xnew [ 2 ] = ( rtP .
Internal_A_avp5ztxxzl [ 6 ] ) * rtDW . psgym2jyk0 [ 0 ] + ( rtP .
Internal_A_avp5ztxxzl [ 7 ] ) * rtDW . psgym2jyk0 [ 1 ] + ( rtP .
Internal_A_avp5ztxxzl [ 8 ] ) * rtDW . psgym2jyk0 [ 2 ] ; xnew [ 2 ] += ( rtP
. Internal_B_ebkdikdi50 [ 2 ] ) * rtB . eryzgsl04j ; ( void ) memcpy ( & rtDW
. psgym2jyk0 [ 0 ] , xnew , sizeof ( real_T ) * 3 ) ; } rtDW . p2qqabsh1z =
rtB . ktb4r2yzkb ; UNUSED_PARAMETER ( tid ) ; } void MdlUpdateTID2 ( int_T
tid ) { UNUSED_PARAMETER ( tid ) ; } void MdlTerminate ( void ) { dfbtnmeqyz
( 1 , rtS , rtDW . eo3swzvasqb ) ; dfbtnmeqyz ( 1 , rtS , rtDW . cor5gsbd40 )
; dfbtnmeqyz ( 1 , rtS , rtDW . btjpsv3ywi ) ; dfbtnmeqyz ( 1 , rtS , rtDW .
jyzsgxunlp ) ; { SimStruct * rts = ssGetSFunction ( rtS , 0 ) ; { static
const char * blockSIDForSFcnLoader = "sim_Start:4:401" ;
sfcnLoader_setCurrentSFcnBlockSID ( blockSIDForSFcnLoader ) ; void ( *
sfcnMethodPtr ) ( SimStruct * ) = rts -> modelMethods . sFcn . mdlTerminate ;
sfcnLoader_separateComplexHandler ( rts , sfcnMethodPtr ) ; } } rt_FREE (
rtDW . k3xrmmg34t . RSimInfoPtr ) ; rt_FREE ( rtDW . pg5gatyniv . RSimInfoPtr
) ; rt_FREE ( rtDW . jayrspqml1 . RSimInfoPtr ) ; rt_FREE ( rtDW . opv4zomgtb
. RSimInfoPtr ) ; } static void mr_sim_Start_cacheDataAsMxArray ( mxArray *
destArray , mwIndex i , int j , const void * srcData , size_t numBytes ) ;
static void mr_sim_Start_cacheDataAsMxArray ( mxArray * destArray , mwIndex i
, int j , const void * srcData , size_t numBytes ) { mxArray * newArray =
mxCreateUninitNumericMatrix ( ( size_t ) 1 , numBytes , mxUINT8_CLASS ,
mxREAL ) ; memcpy ( ( uint8_T * ) mxGetData ( newArray ) , ( const uint8_T *
) srcData , numBytes ) ; mxSetFieldByNumber ( destArray , i , j , newArray )
; } static void mr_sim_Start_restoreDataFromMxArray ( void * destData , const
mxArray * srcArray , mwIndex i , int j , size_t numBytes ) ; static void
mr_sim_Start_restoreDataFromMxArray ( void * destData , const mxArray *
srcArray , mwIndex i , int j , size_t numBytes ) { memcpy ( ( uint8_T * )
destData , ( const uint8_T * ) mxGetData ( mxGetFieldByNumber ( srcArray , i
, j ) ) , numBytes ) ; } static void mr_sim_Start_cacheBitFieldToMxArray (
mxArray * destArray , mwIndex i , int j , uint_T bitVal ) ; static void
mr_sim_Start_cacheBitFieldToMxArray ( mxArray * destArray , mwIndex i , int j
, uint_T bitVal ) { mxSetFieldByNumber ( destArray , i , j ,
mxCreateDoubleScalar ( ( double ) bitVal ) ) ; } static uint_T
mr_sim_Start_extractBitFieldFromMxArray ( const mxArray * srcArray , mwIndex
i , int j , uint_T numBits ) ; static uint_T
mr_sim_Start_extractBitFieldFromMxArray ( const mxArray * srcArray , mwIndex
i , int j , uint_T numBits ) { const uint_T varVal = ( uint_T ) mxGetScalar (
mxGetFieldByNumber ( srcArray , i , j ) ) ; return varVal & ( ( 1u << numBits
) - 1u ) ; } static void mr_sim_Start_cacheDataToMxArrayWithOffset ( mxArray
* destArray , mwIndex i , int j , mwIndex offset , const void * srcData ,
size_t numBytes ) ; static void mr_sim_Start_cacheDataToMxArrayWithOffset (
mxArray * destArray , mwIndex i , int j , mwIndex offset , const void *
srcData , size_t numBytes ) { uint8_T * varData = ( uint8_T * ) mxGetData (
mxGetFieldByNumber ( destArray , i , j ) ) ; memcpy ( ( uint8_T * ) & varData
[ offset * numBytes ] , ( const uint8_T * ) srcData , numBytes ) ; } static
void mr_sim_Start_restoreDataFromMxArrayWithOffset ( void * destData , const
mxArray * srcArray , mwIndex i , int j , mwIndex offset , size_t numBytes ) ;
static void mr_sim_Start_restoreDataFromMxArrayWithOffset ( void * destData ,
const mxArray * srcArray , mwIndex i , int j , mwIndex offset , size_t
numBytes ) { const uint8_T * varData = ( const uint8_T * ) mxGetData (
mxGetFieldByNumber ( srcArray , i , j ) ) ; memcpy ( ( uint8_T * ) destData ,
( const uint8_T * ) & varData [ offset * numBytes ] , numBytes ) ; } static
void mr_sim_Start_cacheBitFieldToCellArrayWithOffset ( mxArray * destArray ,
mwIndex i , int j , mwIndex offset , uint_T fieldVal ) ; static void
mr_sim_Start_cacheBitFieldToCellArrayWithOffset ( mxArray * destArray ,
mwIndex i , int j , mwIndex offset , uint_T fieldVal ) { mxSetCell (
mxGetFieldByNumber ( destArray , i , j ) , offset , mxCreateDoubleScalar ( (
double ) fieldVal ) ) ; } static uint_T
mr_sim_Start_extractBitFieldFromCellArrayWithOffset ( const mxArray *
srcArray , mwIndex i , int j , mwIndex offset , uint_T numBits ) ; static
uint_T mr_sim_Start_extractBitFieldFromCellArrayWithOffset ( const mxArray *
srcArray , mwIndex i , int j , mwIndex offset , uint_T numBits ) { const
uint_T fieldVal = ( uint_T ) mxGetScalar ( mxGetCell ( mxGetFieldByNumber (
srcArray , i , j ) , offset ) ) ; return fieldVal & ( ( 1u << numBits ) - 1u
) ; } mxArray * mr_sim_Start_GetDWork ( ) { static const char *
ssDWFieldNames [ 3 ] = { "rtB" , "rtDW" , "NULL_PrevZCX" , } ; mxArray * ssDW
= mxCreateStructMatrix ( 1 , 1 , 3 , ssDWFieldNames ) ;
mr_sim_Start_cacheDataAsMxArray ( ssDW , 0 , 0 , ( const void * ) & ( rtB ) ,
sizeof ( rtB ) ) ; { static const char * rtdwDataFieldNames [ 35 ] = {
"rtDW.pd03hpujxp" , "rtDW.lruetkegpd" , "rtDW.jlsempkvov" , "rtDW.flos1pd3x0"
, "rtDW.ileqgfdrx1" , "rtDW.caiyox4oj2" , "rtDW.iafj1kgffe" ,
"rtDW.gyqsentnna" , "rtDW.j2fbjqvjcp" , "rtDW.psgym2jyk0" , "rtDW.p2qqabsh1z"
, "rtDW.icjcuyoesf" , "rtDW.a42pr4cd24" , "rtDW.ex0ljdtjz3" ,
"rtDW.mwwhbb5fno" , "rtDW.grgj1nbj2f" , "rtDW.fen0hw5zdb" , "rtDW.gbtehizmoh"
, "rtDW.m1jml1zdzn" , "rtDW.co2n0fejda" , "rtDW.mqobma15yv" ,
"rtDW.fsmjxejob3" , "rtDW.bn4k2qv04s" , "rtDW.j244bobwtj" , "rtDW.aatqqysjgr"
, "rtDW.kfkq4jq4py" , "rtDW.bgn14fgjm3" , "rtDW.c1g1b2pcb5" ,
"rtDW.njbocqae2t" , "rtDW.nyhsr4t3fp" , "rtDW.j04nfygwcf" ,
"rtDW.jyzsgxunlp[0].dd10qqbw3x.b43cz3on2r" ,
"rtDW.btjpsv3ywi[0].dd10qqbw3x.b43cz3on2r" ,
"rtDW.cor5gsbd40[0].dd10qqbw3x.b43cz3on2r" ,
"rtDW.eo3swzvasqb[0].dd10qqbw3x.b43cz3on2r" , } ; mxArray * rtdwData =
mxCreateStructMatrix ( 1 , 1 , 35 , rtdwDataFieldNames ) ; int k0 ;
mxSetFieldByNumber ( rtdwData , 0 , 31 , mxCreateUninitNumericMatrix ( 1 , 1
* sizeof ( rtDW . jyzsgxunlp [ 0 ] . dd10qqbw3x . b43cz3on2r ) ,
mxUINT8_CLASS , mxREAL ) ) ; mxSetFieldByNumber ( rtdwData , 0 , 32 ,
mxCreateUninitNumericMatrix ( 1 , 1 * sizeof ( rtDW . btjpsv3ywi [ 0 ] .
dd10qqbw3x . b43cz3on2r ) , mxUINT8_CLASS , mxREAL ) ) ; mxSetFieldByNumber (
rtdwData , 0 , 33 , mxCreateUninitNumericMatrix ( 1 , 1 * sizeof ( rtDW .
cor5gsbd40 [ 0 ] . dd10qqbw3x . b43cz3on2r ) , mxUINT8_CLASS , mxREAL ) ) ;
mxSetFieldByNumber ( rtdwData , 0 , 34 , mxCreateUninitNumericMatrix ( 1 , 1
* sizeof ( rtDW . eo3swzvasqb [ 0 ] . dd10qqbw3x . b43cz3on2r ) ,
mxUINT8_CLASS , mxREAL ) ) ; mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 ,
0 , ( const void * ) & ( rtDW . pd03hpujxp ) , sizeof ( rtDW . pd03hpujxp ) )
; mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 1 , ( const void * ) & (
rtDW . lruetkegpd ) , sizeof ( rtDW . lruetkegpd ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 2 , ( const void * ) & (
rtDW . jlsempkvov ) , sizeof ( rtDW . jlsempkvov ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 3 , ( const void * ) & (
rtDW . flos1pd3x0 ) , sizeof ( rtDW . flos1pd3x0 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 4 , ( const void * ) & (
rtDW . ileqgfdrx1 ) , sizeof ( rtDW . ileqgfdrx1 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 5 , ( const void * ) & (
rtDW . caiyox4oj2 ) , sizeof ( rtDW . caiyox4oj2 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 6 , ( const void * ) & (
rtDW . iafj1kgffe ) , sizeof ( rtDW . iafj1kgffe ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 7 , ( const void * ) & (
rtDW . gyqsentnna ) , sizeof ( rtDW . gyqsentnna ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 8 , ( const void * ) & (
rtDW . j2fbjqvjcp ) , sizeof ( rtDW . j2fbjqvjcp ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 9 , ( const void * ) & (
rtDW . psgym2jyk0 ) , sizeof ( rtDW . psgym2jyk0 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 10 , ( const void * ) & (
rtDW . p2qqabsh1z ) , sizeof ( rtDW . p2qqabsh1z ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 11 , ( const void * ) & (
rtDW . icjcuyoesf ) , sizeof ( rtDW . icjcuyoesf ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 12 , ( const void * ) & (
rtDW . a42pr4cd24 ) , sizeof ( rtDW . a42pr4cd24 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 13 , ( const void * ) & (
rtDW . ex0ljdtjz3 ) , sizeof ( rtDW . ex0ljdtjz3 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 14 , ( const void * ) & (
rtDW . mwwhbb5fno ) , sizeof ( rtDW . mwwhbb5fno ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 15 , ( const void * ) & (
rtDW . grgj1nbj2f ) , sizeof ( rtDW . grgj1nbj2f ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 16 , ( const void * ) & (
rtDW . fen0hw5zdb ) , sizeof ( rtDW . fen0hw5zdb ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 17 , ( const void * ) & (
rtDW . gbtehizmoh ) , sizeof ( rtDW . gbtehizmoh ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 18 , ( const void * ) & (
rtDW . m1jml1zdzn ) , sizeof ( rtDW . m1jml1zdzn ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 19 , ( const void * ) & (
rtDW . co2n0fejda ) , sizeof ( rtDW . co2n0fejda ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 20 , ( const void * ) & (
rtDW . mqobma15yv ) , sizeof ( rtDW . mqobma15yv ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 21 , ( const void * ) & (
rtDW . fsmjxejob3 ) , sizeof ( rtDW . fsmjxejob3 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 22 , ( const void * ) & (
rtDW . bn4k2qv04s ) , sizeof ( rtDW . bn4k2qv04s ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 23 , ( const void * ) & (
rtDW . j244bobwtj ) , sizeof ( rtDW . j244bobwtj ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 24 , ( const void * ) & (
rtDW . aatqqysjgr ) , sizeof ( rtDW . aatqqysjgr ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 25 , ( const void * ) & (
rtDW . kfkq4jq4py ) , sizeof ( rtDW . kfkq4jq4py ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 26 , ( const void * ) & (
rtDW . bgn14fgjm3 ) , sizeof ( rtDW . bgn14fgjm3 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 27 , ( const void * ) & (
rtDW . c1g1b2pcb5 ) , sizeof ( rtDW . c1g1b2pcb5 ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 28 , ( const void * ) & (
rtDW . njbocqae2t ) , sizeof ( rtDW . njbocqae2t ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 29 , ( const void * ) & (
rtDW . nyhsr4t3fp ) , sizeof ( rtDW . nyhsr4t3fp ) ) ;
mr_sim_Start_cacheDataAsMxArray ( rtdwData , 0 , 30 , ( const void * ) & (
rtDW . j04nfygwcf ) , sizeof ( rtDW . j04nfygwcf ) ) ; for ( k0 = 0 ; k0 < 1
; ++ k0 ) { const mwIndex offset0 = k0 ;
mr_sim_Start_cacheDataToMxArrayWithOffset ( rtdwData , 0 , 31 , offset0 , & (
rtDW . jyzsgxunlp [ k0 ] . dd10qqbw3x . b43cz3on2r ) , sizeof ( rtDW .
jyzsgxunlp [ 0 ] . dd10qqbw3x . b43cz3on2r ) ) ; } for ( k0 = 0 ; k0 < 1 ; ++
k0 ) { const mwIndex offset0 = k0 ; mr_sim_Start_cacheDataToMxArrayWithOffset
( rtdwData , 0 , 32 , offset0 , & ( rtDW . btjpsv3ywi [ k0 ] . dd10qqbw3x .
b43cz3on2r ) , sizeof ( rtDW . btjpsv3ywi [ 0 ] . dd10qqbw3x . b43cz3on2r ) )
; } for ( k0 = 0 ; k0 < 1 ; ++ k0 ) { const mwIndex offset0 = k0 ;
mr_sim_Start_cacheDataToMxArrayWithOffset ( rtdwData , 0 , 33 , offset0 , & (
rtDW . cor5gsbd40 [ k0 ] . dd10qqbw3x . b43cz3on2r ) , sizeof ( rtDW .
cor5gsbd40 [ 0 ] . dd10qqbw3x . b43cz3on2r ) ) ; } for ( k0 = 0 ; k0 < 1 ; ++
k0 ) { const mwIndex offset0 = k0 ; mr_sim_Start_cacheDataToMxArrayWithOffset
( rtdwData , 0 , 34 , offset0 , & ( rtDW . eo3swzvasqb [ k0 ] . dd10qqbw3x .
b43cz3on2r ) , sizeof ( rtDW . eo3swzvasqb [ 0 ] . dd10qqbw3x . b43cz3on2r )
) ; } mxSetFieldByNumber ( ssDW , 0 , 1 , rtdwData ) ; } return ssDW ; } void
mr_sim_Start_SetDWork ( const mxArray * ssDW ) { ( void ) ssDW ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtB ) , ssDW , 0 , 0 ,
sizeof ( rtB ) ) ; { const mxArray * rtdwData = mxGetFieldByNumber ( ssDW , 0
, 1 ) ; int k0 ; mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW .
pd03hpujxp ) , rtdwData , 0 , 0 , sizeof ( rtDW . pd03hpujxp ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . lruetkegpd ) ,
rtdwData , 0 , 1 , sizeof ( rtDW . lruetkegpd ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . jlsempkvov ) ,
rtdwData , 0 , 2 , sizeof ( rtDW . jlsempkvov ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . flos1pd3x0 ) ,
rtdwData , 0 , 3 , sizeof ( rtDW . flos1pd3x0 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . ileqgfdrx1 ) ,
rtdwData , 0 , 4 , sizeof ( rtDW . ileqgfdrx1 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . caiyox4oj2 ) ,
rtdwData , 0 , 5 , sizeof ( rtDW . caiyox4oj2 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . iafj1kgffe ) ,
rtdwData , 0 , 6 , sizeof ( rtDW . iafj1kgffe ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . gyqsentnna ) ,
rtdwData , 0 , 7 , sizeof ( rtDW . gyqsentnna ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . j2fbjqvjcp ) ,
rtdwData , 0 , 8 , sizeof ( rtDW . j2fbjqvjcp ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . psgym2jyk0 ) ,
rtdwData , 0 , 9 , sizeof ( rtDW . psgym2jyk0 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . p2qqabsh1z ) ,
rtdwData , 0 , 10 , sizeof ( rtDW . p2qqabsh1z ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . icjcuyoesf ) ,
rtdwData , 0 , 11 , sizeof ( rtDW . icjcuyoesf ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . a42pr4cd24 ) ,
rtdwData , 0 , 12 , sizeof ( rtDW . a42pr4cd24 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . ex0ljdtjz3 ) ,
rtdwData , 0 , 13 , sizeof ( rtDW . ex0ljdtjz3 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . mwwhbb5fno ) ,
rtdwData , 0 , 14 , sizeof ( rtDW . mwwhbb5fno ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . grgj1nbj2f ) ,
rtdwData , 0 , 15 , sizeof ( rtDW . grgj1nbj2f ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . fen0hw5zdb ) ,
rtdwData , 0 , 16 , sizeof ( rtDW . fen0hw5zdb ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . gbtehizmoh ) ,
rtdwData , 0 , 17 , sizeof ( rtDW . gbtehizmoh ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . m1jml1zdzn ) ,
rtdwData , 0 , 18 , sizeof ( rtDW . m1jml1zdzn ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . co2n0fejda ) ,
rtdwData , 0 , 19 , sizeof ( rtDW . co2n0fejda ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . mqobma15yv ) ,
rtdwData , 0 , 20 , sizeof ( rtDW . mqobma15yv ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . fsmjxejob3 ) ,
rtdwData , 0 , 21 , sizeof ( rtDW . fsmjxejob3 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . bn4k2qv04s ) ,
rtdwData , 0 , 22 , sizeof ( rtDW . bn4k2qv04s ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . j244bobwtj ) ,
rtdwData , 0 , 23 , sizeof ( rtDW . j244bobwtj ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . aatqqysjgr ) ,
rtdwData , 0 , 24 , sizeof ( rtDW . aatqqysjgr ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . kfkq4jq4py ) ,
rtdwData , 0 , 25 , sizeof ( rtDW . kfkq4jq4py ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . bgn14fgjm3 ) ,
rtdwData , 0 , 26 , sizeof ( rtDW . bgn14fgjm3 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . c1g1b2pcb5 ) ,
rtdwData , 0 , 27 , sizeof ( rtDW . c1g1b2pcb5 ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . njbocqae2t ) ,
rtdwData , 0 , 28 , sizeof ( rtDW . njbocqae2t ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . nyhsr4t3fp ) ,
rtdwData , 0 , 29 , sizeof ( rtDW . nyhsr4t3fp ) ) ;
mr_sim_Start_restoreDataFromMxArray ( ( void * ) & ( rtDW . j04nfygwcf ) ,
rtdwData , 0 , 30 , sizeof ( rtDW . j04nfygwcf ) ) ; for ( k0 = 0 ; k0 < 1 ;
++ k0 ) { const mwIndex offset0 = k0 ;
mr_sim_Start_restoreDataFromMxArrayWithOffset ( & ( rtDW . jyzsgxunlp [ k0 ]
. dd10qqbw3x . b43cz3on2r ) , rtdwData , 0 , 31 , offset0 , sizeof ( rtDW .
jyzsgxunlp [ 0 ] . dd10qqbw3x . b43cz3on2r ) ) ; } for ( k0 = 0 ; k0 < 1 ; ++
k0 ) { const mwIndex offset0 = k0 ;
mr_sim_Start_restoreDataFromMxArrayWithOffset ( & ( rtDW . btjpsv3ywi [ k0 ]
. dd10qqbw3x . b43cz3on2r ) , rtdwData , 0 , 32 , offset0 , sizeof ( rtDW .
btjpsv3ywi [ 0 ] . dd10qqbw3x . b43cz3on2r ) ) ; } for ( k0 = 0 ; k0 < 1 ; ++
k0 ) { const mwIndex offset0 = k0 ;
mr_sim_Start_restoreDataFromMxArrayWithOffset ( & ( rtDW . cor5gsbd40 [ k0 ]
. dd10qqbw3x . b43cz3on2r ) , rtdwData , 0 , 33 , offset0 , sizeof ( rtDW .
cor5gsbd40 [ 0 ] . dd10qqbw3x . b43cz3on2r ) ) ; } for ( k0 = 0 ; k0 < 1 ; ++
k0 ) { const mwIndex offset0 = k0 ;
mr_sim_Start_restoreDataFromMxArrayWithOffset ( & ( rtDW . eo3swzvasqb [ k0 ]
. dd10qqbw3x . b43cz3on2r ) , rtdwData , 0 , 34 , offset0 , sizeof ( rtDW .
eo3swzvasqb [ 0 ] . dd10qqbw3x . b43cz3on2r ) ) ; } } } mxArray *
mr_sim_Start_GetSimStateDisallowedBlocks ( ) { mxArray * data =
mxCreateCellMatrix ( 1 , 3 ) ; mwIndex subs [ 2 ] , offset ; { static const
char * blockType [ 1 ] = { "" , } ; static const char * blockPath [ 1 ] = {
"[]" , } ; static const int reason [ 1 ] = { 3 , } ; for ( subs [ 0 ] = 0 ;
subs [ 0 ] < 1 ; ++ ( subs [ 0 ] ) ) { subs [ 1 ] = 0 ; offset =
mxCalcSingleSubscript ( data , 2 , subs ) ; mxSetCell ( data , offset ,
mxCreateString ( blockType [ subs [ 0 ] ] ) ) ; subs [ 1 ] = 1 ; offset =
mxCalcSingleSubscript ( data , 2 , subs ) ; mxSetCell ( data , offset ,
mxCreateString ( blockPath [ subs [ 0 ] ] ) ) ; subs [ 1 ] = 2 ; offset =
mxCalcSingleSubscript ( data , 2 , subs ) ; mxSetCell ( data , offset ,
mxCreateDoubleScalar ( ( double ) reason [ subs [ 0 ] ] ) ) ; } } return data
; } void MdlInitializeSizes ( void ) { ssSetNumContStates ( rtS , 0 ) ;
ssSetNumY ( rtS , 0 ) ; ssSetNumU ( rtS , 0 ) ; ssSetDirectFeedThrough ( rtS
, 0 ) ; ssSetNumSampleTimes ( rtS , 2 ) ; ssSetNumBlocks ( rtS , 137 ) ;
ssSetNumBlockIO ( rtS , 49 ) ; ssSetNumBlockParams ( rtS , 255992157 ) ; }
void MdlInitializeSampleTimes ( void ) { ssSetSampleTime ( rtS , 0 , 0.0 ) ;
ssSetSampleTime ( rtS , 1 , 0.01 ) ; ssSetOffsetTime ( rtS , 0 , 0.0 ) ;
ssSetOffsetTime ( rtS , 1 , 0.0 ) ; } void raccel_set_checksum ( ) {
ssSetChecksumVal ( rtS , 0 , 271311271U ) ; ssSetChecksumVal ( rtS , 1 ,
287428677U ) ; ssSetChecksumVal ( rtS , 2 , 139058845U ) ; ssSetChecksumVal (
rtS , 3 , 2583372187U ) ; }
#if defined(_MSC_VER)
#pragma optimize( "", off )
#endif
SimStruct * raccel_register_model ( ssExecutionInfo * executionInfo ) {
static struct _ssMdlInfo mdlInfo ; ( void ) memset ( ( char * ) rtS , 0 ,
sizeof ( SimStruct ) ) ; ( void ) memset ( ( char * ) & mdlInfo , 0 , sizeof
( struct _ssMdlInfo ) ) ; ssSetMdlInfoPtr ( rtS , & mdlInfo ) ;
ssSetExecutionInfo ( rtS , executionInfo ) ; { static time_T mdlPeriod [
NSAMPLE_TIMES ] ; static time_T mdlOffset [ NSAMPLE_TIMES ] ; static time_T
mdlTaskTimes [ NSAMPLE_TIMES ] ; static int_T mdlTsMap [ NSAMPLE_TIMES ] ;
static int_T mdlSampleHits [ NSAMPLE_TIMES ] ; static boolean_T
mdlTNextWasAdjustedPtr [ NSAMPLE_TIMES ] ; static int_T mdlPerTaskSampleHits
[ NSAMPLE_TIMES * NSAMPLE_TIMES ] ; static time_T mdlTimeOfNextSampleHit [
NSAMPLE_TIMES ] ; { int_T i ; for ( i = 0 ; i < NSAMPLE_TIMES ; i ++ ) {
mdlPeriod [ i ] = 0.0 ; mdlOffset [ i ] = 0.0 ; mdlTaskTimes [ i ] = 0.0 ;
mdlTsMap [ i ] = i ; mdlSampleHits [ i ] = 1 ; } } ssSetSampleTimePtr ( rtS ,
& mdlPeriod [ 0 ] ) ; ssSetOffsetTimePtr ( rtS , & mdlOffset [ 0 ] ) ;
ssSetSampleTimeTaskIDPtr ( rtS , & mdlTsMap [ 0 ] ) ; ssSetTPtr ( rtS , &
mdlTaskTimes [ 0 ] ) ; ssSetSampleHitPtr ( rtS , & mdlSampleHits [ 0 ] ) ;
ssSetTNextWasAdjustedPtr ( rtS , & mdlTNextWasAdjustedPtr [ 0 ] ) ;
ssSetPerTaskSampleHitsPtr ( rtS , & mdlPerTaskSampleHits [ 0 ] ) ;
ssSetTimeOfNextSampleHitPtr ( rtS , & mdlTimeOfNextSampleHit [ 0 ] ) ; }
ssSetSolverMode ( rtS , SOLVER_MODE_SINGLETASKING ) ; { ssSetBlockIO ( rtS ,
( ( void * ) & rtB ) ) ; ( void ) memset ( ( ( void * ) & rtB ) , 0 , sizeof
( B ) ) ; suInitializeString ( & rtB . izgo3pnyfb , "" ) ;
slsaCacheDWorkPointerForSimTargetOP ( rtS , & rtB . izgo3pnyfb ) ;
suInitializeString ( & rtB . o0ftqgjmrw , "" ) ;
slsaCacheDWorkPointerForSimTargetOP ( rtS , & rtB . o0ftqgjmrw ) ;
suInitializeString ( & rtB . bkd0pdtznt , "" ) ;
slsaCacheDWorkPointerForSimTargetOP ( rtS , & rtB . bkd0pdtznt ) ; } { void *
dwork = ( void * ) & rtDW ; ssSetRootDWork ( rtS , dwork ) ; ( void ) memset
( dwork , 0 , sizeof ( DW ) ) ; suInitializeString ( & rtDW . icjcuyoesf , ""
) ; slsaCacheDWorkPointerForSimTargetOP ( rtS , & rtDW . icjcuyoesf ) ;
suInitializeString ( & rtDW . ex0ljdtjz3 , "" ) ;
slsaCacheDWorkPointerForSimTargetOP ( rtS , & rtDW . ex0ljdtjz3 ) ; } {
static DataTypeTransInfo dtInfo ; ( void ) memset ( ( char_T * ) & dtInfo , 0
, sizeof ( dtInfo ) ) ; ssSetModelMappingInfo ( rtS , & dtInfo ) ; dtInfo .
numDataTypes = 16 ; dtInfo . dataTypeSizes = & rtDataTypeSizes [ 0 ] ; dtInfo
. dataTypeNames = & rtDataTypeNames [ 0 ] ; dtInfo . BTransTable = &
rtBTransTable ; dtInfo . PTransTable = & rtPTransTable ; dtInfo .
dataTypeInfoTable = rtDataTypeInfoTable ; } sim_Start_InitializeDataMapInfo (
) ; ssSetIsRapidAcceleratorActive ( rtS , true ) ; ssSetRootSS ( rtS , rtS )
; ssSetVersion ( rtS , SIMSTRUCT_VERSION_LEVEL2 ) ; ssSetModelName ( rtS ,
"sim_Start" ) ; ssSetPath ( rtS , "sim_Start" ) ; ssSetTStart ( rtS , 0.0 ) ;
ssSetTFinal ( rtS , 500.0 ) ; ssSetStepSize ( rtS , 0.01 ) ;
ssSetFixedStepSize ( rtS , 0.01 ) ; { static RTWLogInfo rt_DataLoggingInfo ;
rt_DataLoggingInfo . loggingInterval = ( NULL ) ; ssSetRTWLogInfo ( rtS , &
rt_DataLoggingInfo ) ; } { { static int_T rt_LoggedStateWidths [ ] = { 1 , 1
, 1 , 1 , 1 , 1500 , 1 , 3 , 1 , 3 , 1 , } ; static int_T
rt_LoggedStateNumDimensions [ ] = { 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1
, } ; static int_T rt_LoggedStateDimensions [ ] = { 1 , 1 , 1 , 1 , 1 , 1500
, 1 , 3 , 1 , 3 , 1 , } ; static boolean_T rt_LoggedStateIsVarDims [ ] = { 0
, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , } ; static BuiltInDTypeId
rt_LoggedStateDataTypeIds [ ] = { SS_DOUBLE , SS_DOUBLE , SS_DOUBLE ,
SS_DOUBLE , SS_DOUBLE , SS_DOUBLE , SS_DOUBLE , SS_DOUBLE , SS_DOUBLE ,
SS_DOUBLE , SS_DOUBLE , } ; static int_T rt_LoggedStateComplexSignals [ ] = {
0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , } ; static RTWPreprocessingFcnPtr
rt_LoggingStatePreprocessingFcnPtrs [ ] = { ( NULL ) , ( NULL ) , ( NULL ) ,
( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) , ( NULL ) ,
( NULL ) , } ; static const char_T * rt_LoggedStateLabels [ ] = { "DSTATE" ,
"DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" ,
"DSTATE" , "DSTATE" , "DSTATE" , } ; static const char_T *
rt_LoggedStateBlockNames [ ] = { "sim_Start/Write to memory/Unit Delay1" ,
"sim_Start/Unit Delay1" ,
"sim_Start/Robot Simulator/Discrete-Time\nIntegrator" ,
"sim_Start/Robot Simulator/Discrete-Time\nIntegrator1" ,
"sim_Start/Robot Simulator/Discrete-Time\nIntegrator2" ,
"sim_Start/Optimization Subsystem/Delay" ,
"sim_Start/Wheel Control subsystem/LeftMotor/Discrete-Time\nIntegrator" ,
"sim_Start/Wheel Control subsystem/LeftMotor/Motor/Internal" ,
"sim_Start/Wheel Control subsystem/RightMotor/Discrete-Time\nIntegrator" ,
"sim_Start/Wheel Control subsystem/RightMotor/Motor/Internal" ,
"sim_Start/Unit Delay2" , } ; static const char_T * rt_LoggedStateNames [ ] =
{ "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE"
, "DSTATE" , "DSTATE" , "DSTATE" , "DSTATE" , } ; static boolean_T
rt_LoggedStateCrossMdlRef [ ] = { 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
} ; static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert [ ] = { { 0 ,
SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE ,
SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0
, 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 ,
0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 ,
SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE ,
SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0
, 0 , 1.0 , 0 , 0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 ,
0.0 } , { 0 , SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , { 0 ,
SS_DOUBLE , SS_DOUBLE , 0 , 0 , 0 , 1.0 , 0 , 0.0 } , } ; static int_T
rt_LoggedStateIdxList [ ] = { 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 , }
; static RTWLogSignalInfo rt_LoggedStateSignalInfo = { 11 ,
rt_LoggedStateWidths , rt_LoggedStateNumDimensions , rt_LoggedStateDimensions
, rt_LoggedStateIsVarDims , ( NULL ) , ( NULL ) , rt_LoggedStateDataTypeIds ,
rt_LoggedStateComplexSignals , ( NULL ) , rt_LoggingStatePreprocessingFcnPtrs
, { rt_LoggedStateLabels } , ( NULL ) , ( NULL ) , ( NULL ) , {
rt_LoggedStateBlockNames } , { rt_LoggedStateNames } ,
rt_LoggedStateCrossMdlRef , rt_RTWLogDataTypeConvert , rt_LoggedStateIdxList
} ; static void * rt_LoggedStateSignalPtrs [ 11 ] ; rtliSetLogXSignalPtrs (
ssGetRTWLogInfo ( rtS ) , ( LogSignalPtrsType ) rt_LoggedStateSignalPtrs ) ;
rtliSetLogXSignalInfo ( ssGetRTWLogInfo ( rtS ) , & rt_LoggedStateSignalInfo
) ; rt_LoggedStateSignalPtrs [ 0 ] = ( void * ) & rtDW . pd03hpujxp ;
rt_LoggedStateSignalPtrs [ 1 ] = ( void * ) & rtDW . lruetkegpd ;
rt_LoggedStateSignalPtrs [ 2 ] = ( void * ) & rtDW . jlsempkvov ;
rt_LoggedStateSignalPtrs [ 3 ] = ( void * ) & rtDW . flos1pd3x0 ;
rt_LoggedStateSignalPtrs [ 4 ] = ( void * ) & rtDW . ileqgfdrx1 ;
rt_LoggedStateSignalPtrs [ 5 ] = ( void * ) rtDW . caiyox4oj2 ;
rt_LoggedStateSignalPtrs [ 6 ] = ( void * ) & rtDW . iafj1kgffe ;
rt_LoggedStateSignalPtrs [ 7 ] = ( void * ) rtDW . gyqsentnna ;
rt_LoggedStateSignalPtrs [ 8 ] = ( void * ) & rtDW . j2fbjqvjcp ;
rt_LoggedStateSignalPtrs [ 9 ] = ( void * ) rtDW . psgym2jyk0 ;
rt_LoggedStateSignalPtrs [ 10 ] = ( void * ) & rtDW . p2qqabsh1z ; }
rtliSetLogT ( ssGetRTWLogInfo ( rtS ) , "tout" ) ; rtliSetLogX (
ssGetRTWLogInfo ( rtS ) , "" ) ; rtliSetLogXFinal ( ssGetRTWLogInfo ( rtS ) ,
"" ) ; rtliSetLogVarNameModifier ( ssGetRTWLogInfo ( rtS ) , "none" ) ;
rtliSetLogFormat ( ssGetRTWLogInfo ( rtS ) , 4 ) ; rtliSetLogMaxRows (
ssGetRTWLogInfo ( rtS ) , 0 ) ; rtliSetLogDecimation ( ssGetRTWLogInfo ( rtS
) , 1 ) ; rtliSetLogY ( ssGetRTWLogInfo ( rtS ) , "" ) ;
rtliSetLogYSignalInfo ( ssGetRTWLogInfo ( rtS ) , ( NULL ) ) ;
rtliSetLogYSignalPtrs ( ssGetRTWLogInfo ( rtS ) , ( NULL ) ) ; } { static
ssSolverInfo slvrInfo ; ssSetSolverInfo ( rtS , & slvrInfo ) ;
ssSetSolverName ( rtS , "FixedStepDiscrete" ) ; ssSetVariableStepSolver ( rtS
, 0 ) ; ssSetSolverConsistencyChecking ( rtS , 0 ) ;
ssSetSolverAdaptiveZcDetection ( rtS , 0 ) ; ssSetSolverRobustResetMethod (
rtS , 0 ) ; ssSetSolverStateProjection ( rtS , 0 ) ;
ssSetSolverMassMatrixType ( rtS , ( ssMatrixType ) 0 ) ;
ssSetSolverMassMatrixNzMax ( rtS , 0 ) ; ssSetModelOutputs ( rtS , MdlOutputs
) ; ssSetModelLogData ( rtS , rt_UpdateTXYLogVars ) ;
ssSetModelLogDataIfInInterval ( rtS , rt_UpdateTXXFYLogVars ) ;
ssSetModelUpdate ( rtS , MdlUpdate ) ; ssSetTNextTid ( rtS , INT_MIN ) ;
ssSetTNext ( rtS , rtMinusInf ) ; ssSetSolverNeedsReset ( rtS ) ;
ssSetNumNonsampledZCs ( rtS , 0 ) ; } ssSetChecksumVal ( rtS , 0 , 271311271U
) ; ssSetChecksumVal ( rtS , 1 , 287428677U ) ; ssSetChecksumVal ( rtS , 2 ,
139058845U ) ; ssSetChecksumVal ( rtS , 3 , 2583372187U ) ; { static const
sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE ; static RTWExtModeInfo
rt_ExtModeInfo ; static const sysRanDType * systemRan [ 26 ] ;
gblRTWExtModeInfo = & rt_ExtModeInfo ; ssSetRTWExtModeInfo ( rtS , &
rt_ExtModeInfo ) ; rteiSetSubSystemActiveVectorAddresses ( & rt_ExtModeInfo ,
systemRan ) ; systemRan [ 0 ] = & rtAlwaysEnabled ; systemRan [ 1 ] = &
rtAlwaysEnabled ; systemRan [ 2 ] = & rtAlwaysEnabled ; systemRan [ 3 ] = &
rtAlwaysEnabled ; systemRan [ 4 ] = & rtAlwaysEnabled ; systemRan [ 5 ] = &
rtAlwaysEnabled ; systemRan [ 6 ] = & rtAlwaysEnabled ; systemRan [ 7 ] = &
rtAlwaysEnabled ; systemRan [ 8 ] = & rtAlwaysEnabled ; systemRan [ 9 ] = &
rtAlwaysEnabled ; systemRan [ 10 ] = & rtAlwaysEnabled ; systemRan [ 11 ] = &
rtAlwaysEnabled ; systemRan [ 12 ] = & rtAlwaysEnabled ; systemRan [ 13 ] = &
rtAlwaysEnabled ; systemRan [ 14 ] = & rtAlwaysEnabled ; systemRan [ 15 ] = &
rtAlwaysEnabled ; systemRan [ 16 ] = & rtAlwaysEnabled ; systemRan [ 17 ] = &
rtAlwaysEnabled ; systemRan [ 18 ] = & rtAlwaysEnabled ; systemRan [ 19 ] = &
rtAlwaysEnabled ; systemRan [ 20 ] = & rtAlwaysEnabled ; systemRan [ 21 ] = &
rtAlwaysEnabled ; systemRan [ 22 ] = & rtAlwaysEnabled ; systemRan [ 23 ] = &
rtAlwaysEnabled ; systemRan [ 24 ] = & rtAlwaysEnabled ; systemRan [ 25 ] = &
rtAlwaysEnabled ; rteiSetModelMappingInfoPtr ( ssGetRTWExtModeInfo ( rtS ) ,
& ssGetModelMappingInfo ( rtS ) ) ; rteiSetChecksumsPtr ( ssGetRTWExtModeInfo
( rtS ) , ssGetChecksums ( rtS ) ) ; rteiSetTPtr ( ssGetRTWExtModeInfo ( rtS
) , ssGetTPtr ( rtS ) ) ; } slsaDisallowedBlocksForSimTargetOP ( rtS ,
mr_sim_Start_GetSimStateDisallowedBlocks ) ; slsaGetWorkFcnForSimTargetOP (
rtS , mr_sim_Start_GetDWork ) ; slsaSetWorkFcnForSimTargetOP ( rtS ,
mr_sim_Start_SetDWork ) ; rt_RapidReadMatFileAndUpdateParams ( rtS ) ; if (
ssGetErrorStatus ( rtS ) ) { return rtS ; } ssSetNumSFunctions ( rtS , 1 ) ;
{ static SimStruct childSFunctions [ 1 ] ; static SimStruct *
childSFunctionPtrs [ 1 ] ; ( void ) memset ( ( void * ) & childSFunctions [ 0
] , 0 , sizeof ( childSFunctions ) ) ; ssSetSFunctions ( rtS , &
childSFunctionPtrs [ 0 ] ) ; ssSetSFunction ( rtS , 0 , & childSFunctions [ 0
] ) ; { SimStruct * rts = ssGetSFunction ( rtS , 0 ) ; static time_T
sfcnPeriod [ 1 ] ; static time_T sfcnOffset [ 1 ] ; static int_T sfcnTsMap [
1 ] ; ( void ) memset ( ( void * ) sfcnPeriod , 0 , sizeof ( time_T ) * 1 ) ;
( void ) memset ( ( void * ) sfcnOffset , 0 , sizeof ( time_T ) * 1 ) ;
ssSetSampleTimePtr ( rts , & sfcnPeriod [ 0 ] ) ; ssSetOffsetTimePtr ( rts ,
& sfcnOffset [ 0 ] ) ; ssSetSampleTimeTaskIDPtr ( rts , sfcnTsMap ) ; {
static struct _ssBlkInfo2 _blkInfo2 ; struct _ssBlkInfo2 * blkInfo2 = &
_blkInfo2 ; ssSetBlkInfo2Ptr ( rts , blkInfo2 ) ; } { static struct
_ssPortInfo2 _portInfo2 ; struct _ssPortInfo2 * portInfo2 = & _portInfo2 ;
_ssSetBlkInfo2PortInfo2Ptr ( rts , portInfo2 ) ; } ssSetMdlInfoPtr ( rts ,
ssGetMdlInfoPtr ( rtS ) ) ; { static struct _ssSFcnModelMethods2 methods2 ;
ssSetModelMethods2 ( rts , & methods2 ) ; } { static struct
_ssSFcnModelMethods3 methods3 ; ssSetModelMethods3 ( rts , & methods3 ) ; } {
static struct _ssSFcnModelMethods4 methods4 ; ssSetModelMethods4 ( rts , &
methods4 ) ; } { static struct _ssStatesInfo2 statesInfo2 ; static
ssPeriodicStatesInfo periodicStatesInfo ; static ssJacobianPerturbationBounds
jacPerturbationBounds ; ssSetStatesInfo2 ( rts , & statesInfo2 ) ;
ssSetPeriodicStatesInfo ( rts , & periodicStatesInfo ) ;
ssSetJacobianPerturbationBounds ( rts , & jacPerturbationBounds ) ; }
ssSetModelName ( rts , "sfun_time" ) ; ssSetPath ( rts ,
"sim_Start/Robot Simulator/Soft Real Time" ) ; if ( ssGetRTModel ( rtS ) == (
NULL ) ) { ssSetParentSS ( rts , rtS ) ; ssSetRootSS ( rts , ssGetRootSS (
rtS ) ) ; } else { ssSetRTModel ( rts , ssGetRTModel ( rtS ) ) ;
ssSetParentSS ( rts , ( NULL ) ) ; ssSetRootSS ( rts , rts ) ; } ssSetVersion
( rts , SIMSTRUCT_VERSION_LEVEL2 ) ; { static mxArray * sfcnParams [ 1 ] ;
ssSetSFcnParamsCount ( rts , 1 ) ; ssSetSFcnParamsPtr ( rts , & sfcnParams [
0 ] ) ; ssSetSFcnParam ( rts , 0 , ( mxArray * ) rtP . SoftRealTime_P1_Size )
; } ssSetRWork ( rts , ( real_T * ) & rtDW . mwwhbb5fno ) ; { static struct
_ssDWorkRecord dWorkRecord [ 1 ] ; static struct _ssDWorkAuxRecord
dWorkAuxRecord [ 1 ] ; ssSetSFcnDWork ( rts , dWorkRecord ) ;
ssSetSFcnDWorkAux ( rts , dWorkAuxRecord ) ; _ssSetNumDWork ( rts , 1 ) ;
ssSetDWorkWidth ( rts , 0 , 1 ) ; ssSetDWorkDataType ( rts , 0 , SS_DOUBLE )
; ssSetDWorkComplexSignal ( rts , 0 , 0 ) ; ssSetDWork ( rts , 0 , & rtDW .
mwwhbb5fno ) ; } { raccelLoadSFcnMexFile ( "sfun_time" , "sim_Start:4:401" ,
rts , 0 ) ; if ( ssGetErrorStatus ( rtS ) ) { return rtS ; } }
sfcnInitializeSampleTimes ( rts ) ; ssSetSampleTime ( rts , 0 , 0.0 ) ;
ssSetOffsetTime ( rts , 0 , 0.0 ) ; sfcnTsMap [ 0 ] = 0 ;
ssSetNumNonsampledZCs ( rts , 0 ) ; } } return rtS ; }
#if defined(_MSC_VER)
#pragma optimize( "", on )
#endif
const int_T gblParameterTuningTid = 2 ; void MdlOutputsParameterSampleTime (
int_T tid ) { MdlOutputsTID2 ( tid ) ; }
